# TASK MANAGER

Данный проект был написан в ходе обучения языку Java (Java SE + Spring).

## DEVELOPER INFO

Name: Andrey Pyrinov

E-mail: pirinoff.a@gmail.com

## SOFTWARE

OS: Windows 10 Pro 21H2 (19044.1586)

JDK: 17.0.2

## HARDWARE

CPU: i5

RAM: 16Gb

SSD: 256Gb

## BUILD

```bash
mvn clean install
```

## RUN

```bash
java -jar ./task-manager.jar
```