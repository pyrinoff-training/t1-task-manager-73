#UNIX end of line only!
docker stop testclient
docker rm $(docker ps -a | awk '/testclient/ {print $1}')
docker run \
  --name testclient \
  --network=docker_default \
  -e 'server.host=tm-balancer' \
  -e 'server.port=80' \
  -it \
  registry.gitlab.com/t1-task-manager/t1-task-manager-73/task-manager-client:latest
