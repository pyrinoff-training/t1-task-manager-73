--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

-- Started on 2022-10-21 14:26:32

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 209 (class 1259 OID 16396)
-- Name: projects; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projects (
    id character varying(100) NOT NULL,
    created timestamp(0) with time zone,
    name character varying(150),
    description character varying(250),
    status character varying(20),
    user_id character varying(100)
);


ALTER TABLE public.projects OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16410)
-- Name: sessions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sessions (
    id character varying(100) NOT NULL,
    date timestamp with time zone,
    user_id character varying(100),
    role character varying(15)
);


ALTER TABLE public.sessions OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 16403)
-- Name: tasks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tasks (
    id character varying(100) NOT NULL,
    created timestamp(0) with time zone,
    name character varying(150),
    description character varying(250),
    status character varying(20),
    user_id character varying(100),
    project_id character varying(100)
);


ALTER TABLE public.tasks OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 16415)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id character varying(100) NOT NULL,
    created timestamp(0) with time zone,
    first_name character varying(100),
    last_name character varying(100),
    middle_name character varying(100),
    login character varying(50) NOT NULL,
    password character varying(500) NOT NULL,
    email character varying(100),
    locked boolean,
    role character varying(15)
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 3176 (class 2606 OID 16402)
-- Name: projects projects_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (id);


--
-- TOC entry 3180 (class 2606 OID 16414)
-- Name: sessions sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (id);


--
-- TOC entry 3178 (class 2606 OID 16409)
-- Name: tasks tm_tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT tm_tasks_pkey PRIMARY KEY (id);


--
-- TOC entry 3182 (class 2606 OID 16421)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


-- Completed on 2022-10-21 14:26:32

--
-- PostgreSQL database dump complete
--

