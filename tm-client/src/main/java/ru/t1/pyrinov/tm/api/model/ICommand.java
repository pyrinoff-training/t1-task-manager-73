package ru.t1.pyrinov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.event.ConsoleEvent;

public interface ICommand {

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

    @NotNull
    String getName();

    void execute(@NotNull final ConsoleEvent consoleEvent);

}
