package ru.t1.pyrinov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.model.dto.ProjectDTO;

public interface IProjectRepository extends IUserOwnedRepository<ProjectDTO> {

    @Nullable
    ProjectDTO create(String userId, String name, String description);

    @Nullable
    ProjectDTO create(String userId, String name);

}
