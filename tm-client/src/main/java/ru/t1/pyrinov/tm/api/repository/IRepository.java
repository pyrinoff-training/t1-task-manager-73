package ru.t1.pyrinov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.model.dto.AbstractDTOModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractDTOModel> {

    @Nullable
    M add(M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(Comparator<M> comparator);

    @Nullable
    M findOneById(String id);

    int getSize();

    boolean existsById(String id);

    void clear();

    @Nullable
    M remove(M model);

    @Nullable
    M removeById(String id);

}
