package ru.t1.pyrinov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.model.dto.TaskDTO;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<TaskDTO> {

    @Nullable
    TaskDTO create(String userId, String name, String description);

    @Nullable
    TaskDTO create(String userId, String name);

    @NotNull
    List<TaskDTO> findAllByProjectId(String userId, String projectId);

}
