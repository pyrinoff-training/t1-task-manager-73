package ru.t1.pyrinov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.model.dto.AbstractUserOwnedDTOModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedDTOModel> extends IRepository<M> {

    @Nullable
    M add(String userId, M model);

    @NotNull
    List<M> findAll(String userId);

    @NotNull
    List<M> findAll(String userId, Comparator<M> comparator);

    @Nullable
    M findOneById(String userId, String id);

    int getSize(String userId);

    boolean existsById(String userId, String id);

    void clear(String userId);

    @Nullable
    M remove(String userId, M model);

    @Nullable
    M removeById(String userId, String id);

    void removeAllByUserId(String userId);

}
