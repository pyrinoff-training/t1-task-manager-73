package ru.t1.pyrinov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.model.dto.UserDTO;

public interface IUserRepository extends IRepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(String login);

    @Nullable
    UserDTO findByEmail(String email);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

}
