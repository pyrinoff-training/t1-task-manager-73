package ru.t1.pyrinov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.pyrinov.tm.api.provider.IAdminDataProvider;
import ru.t1.pyrinov.tm.api.provider.IConnectionProvider;

public interface IPropertyService extends IConnectionProvider, IAdminDataProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

}
