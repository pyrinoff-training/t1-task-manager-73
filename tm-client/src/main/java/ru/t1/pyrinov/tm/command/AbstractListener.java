package ru.t1.pyrinov.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import ru.t1.pyrinov.tm.event.ConsoleEvent;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.api.model.ICommand;
import ru.t1.pyrinov.tm.api.service.ITokenService;
import ru.t1.pyrinov.tm.enumerated.Role;

@Component
public abstract class AbstractListener implements ICommand {

    @Autowired
    @Getter
    @Setter
    protected @NotNull ITokenService tokenService;

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

    @Nullable
    public abstract Role[] getRoles();

    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
