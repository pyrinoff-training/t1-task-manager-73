package ru.t1.pyrinov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import ru.t1.pyrinov.tm.event.ConsoleEvent;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.pyrinov.tm.command.AbstractListener;
import ru.t1.pyrinov.tm.enumerated.Role;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.model.dto.ProjectDTO;

import java.util.List;

@Component
public abstract class AbstractProjectListener extends AbstractListener {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Autowired
    protected @NotNull IProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(@Nullable final ProjectDTO project) {
        if (project == null) {
            System.out.println("Null project");
            return;
        }
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

    protected void renderProjects(@NotNull final List<ProjectDTO> projects) {
        int index = 1;
        for (@NotNull final ProjectDTO oneProject : projects) {
            System.out.println(index + ". " + oneProject);
            index++;
        }
    }

}
