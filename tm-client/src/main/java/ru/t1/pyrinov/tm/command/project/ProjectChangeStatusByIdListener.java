package ru.t1.pyrinov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import ru.t1.pyrinov.tm.event.ConsoleEvent;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.model.dto.request.ProjectChangeStatusByIdRequest;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class ProjectChangeStatusByIdListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-change-status-by-id";

    @NotNull
    public static final String DESCRIPTION = "Change project status by id.";

    @Override
    @EventListener(condition = "@projectChangeStatusByIdListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS: ");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @Nullable final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken());
        request.setProjectId(id);
        request.setStatus(status);
        projectEndpoint.changeProjectStatusById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
