package ru.t1.pyrinov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import ru.t1.pyrinov.tm.event.ConsoleEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.event.ConsoleEvent;
import ru.t1.pyrinov.tm.model.dto.request.ProjectCreateRequest;

@Component
public final class ProjectClearListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-clear";

    @NotNull
    public static final String DESCRIPTION = "Remove all projects.";

    @Override
    @EventListener(condition = "@projectClearListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[CLEAR PROJECTS]");
        @NotNull ProjectCreateRequest request = new ProjectCreateRequest(getToken());
        projectEndpoint.createProject(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
