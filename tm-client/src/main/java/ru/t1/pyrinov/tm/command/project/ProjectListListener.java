package ru.t1.pyrinov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import ru.t1.pyrinov.tm.event.ConsoleEvent;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.model.dto.request.ProjectListRequest;
import ru.t1.pyrinov.tm.model.dto.response.ProjectListResponse;
import ru.t1.pyrinov.tm.enumerated.SortOrder;
import ru.t1.pyrinov.tm.model.dto.ProjectDTO;
import ru.t1.pyrinov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class ProjectListListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-list";

    @NotNull
    public static final String DESCRIPTION = "Display all projects.";

    @Override
    @EventListener(condition = "@projectListListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(SortOrder.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final SortOrder sort = SortOrder.toSortOrder(sortType);
        @Nullable final ProjectListRequest request = new ProjectListRequest(getToken());
        request.setSort(sort);
        @Nullable final ProjectListResponse response = projectEndpoint.getProjectList(request);
        @Nullable final List<ProjectDTO> projects = response.getProjects();
        if (projects == null) return;
        renderProjects(projects);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
