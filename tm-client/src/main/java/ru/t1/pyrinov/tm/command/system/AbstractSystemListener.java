package ru.t1.pyrinov.tm.command.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import ru.t1.pyrinov.tm.event.ConsoleEvent;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.pyrinov.tm.api.service.IPropertyService;
import ru.t1.pyrinov.tm.command.AbstractListener;
import ru.t1.pyrinov.tm.enumerated.Role;

@Getter
@Setter
@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @Autowired
    protected @NotNull IPropertyService propertyService;

    @Autowired
    protected @NotNull ISystemEndpoint systemEndpoint;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
