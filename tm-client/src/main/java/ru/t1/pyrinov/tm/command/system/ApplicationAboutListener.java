package ru.t1.pyrinov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import ru.t1.pyrinov.tm.event.ConsoleEvent;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.api.service.IPropertyService;
import ru.t1.pyrinov.tm.model.dto.request.ServerAboutRequest;
import ru.t1.pyrinov.tm.model.dto.response.ServerAboutResponse;

@Component
public final class ApplicationAboutListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String DESCRIPTION = "Display developer info.";

    @NotNull
    public static final String ARGUMENT = "-a";

    @Override
    @EventListener(condition = "@applicationAboutListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final IPropertyService service = getPropertyService();
        System.out.println("[ABOUT]");
        System.out.println("[Client]");
        System.out.println("Name:  " + service.getAuthorName());
        System.out.println("E-mail: " + service.getAuthorEmail());
        System.out.println();
        System.out.println();
        System.out.println("[Server]");
        @Nullable final ServerAboutRequest request = new ServerAboutRequest();
        @Nullable final ServerAboutResponse response = systemEndpoint.getAbout(request);
        System.out.println("Name: " + response.getName());
        System.out.println("E-mail: " + response.getEmail());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
