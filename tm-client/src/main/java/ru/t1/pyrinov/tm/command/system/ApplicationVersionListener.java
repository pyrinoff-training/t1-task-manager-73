package ru.t1.pyrinov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import ru.t1.pyrinov.tm.event.ConsoleEvent;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.model.dto.request.ServerVersionRequest;
import ru.t1.pyrinov.tm.model.dto.response.ServerVersionResponse;

@Component
public final class ApplicationVersionListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "version";

    @NotNull
    public static final String DESCRIPTION = "Display application version.";

    @NotNull
    public static final String ARGUMENT = "-v";

    @Override
    @EventListener(condition = "@applicationVersionListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[VERSION]");
        System.out.print("Client: ");
        System.out.println(getPropertyService().getApplicationVersion());
        @Nullable final ServerVersionRequest request = new ServerVersionRequest();
        @Nullable final ServerVersionResponse response = systemEndpoint.getVersion(request);
        System.out.println("Server: " + response.getVersion());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
