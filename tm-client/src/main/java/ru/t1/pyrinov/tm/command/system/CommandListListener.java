package ru.t1.pyrinov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import ru.t1.pyrinov.tm.event.ConsoleEvent;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.api.model.ICommand;
import ru.t1.pyrinov.tm.command.AbstractListener;

import java.util.Collection;

@Component
public final class CommandListListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "commands";

    @NotNull
    public static final String DESCRIPTION = "Show commands list.";

    @NotNull
    public static final String ARGUMENT = "-cmd";

    @Autowired
    private AbstractListener[] listeners;

    @Override
    @EventListener(condition = "@commandListListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMMANDS]");
        for (@NotNull final ICommand oneListener : listeners) {
            @NotNull final String name = oneListener.getName();
            if (name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
