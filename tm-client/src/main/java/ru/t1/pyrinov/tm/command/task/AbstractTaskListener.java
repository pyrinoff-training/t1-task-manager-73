package ru.t1.pyrinov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import ru.t1.pyrinov.tm.event.ConsoleEvent;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.pyrinov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.pyrinov.tm.command.AbstractListener;
import ru.t1.pyrinov.tm.enumerated.Role;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.model.dto.TaskDTO;

import java.util.List;

@Component
public abstract class AbstractTaskListener extends AbstractListener {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Autowired
    @NotNull
    protected ITaskEndpoint taskEndpoint;

    @Autowired
    @NotNull
    protected IProjectEndpoint projectEndpoint;

    protected void renderTasks(@NotNull final List<TaskDTO> tasks) {
        int index = 1;
        for (@NotNull final TaskDTO oneTask : tasks) {
            System.out.println(index + ". " + oneTask);
            index++;
        }
    }

    protected void showTask(@NotNull final TaskDTO task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}
