package ru.t1.pyrinov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import ru.t1.pyrinov.tm.event.ConsoleEvent;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.model.dto.request.TaskCompleteByIdRequest;
import ru.t1.pyrinov.tm.util.TerminalUtil;

@Component
public final class TaskCompleteByIdListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-complete-by-id";

    @NotNull
    public static final String DESCRIPTION = "Complete task by id.";

    @Override
    @EventListener(condition = "@taskCompleteByIdListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(getToken());
        request.setId(id);
        taskEndpoint.completeTaskById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
