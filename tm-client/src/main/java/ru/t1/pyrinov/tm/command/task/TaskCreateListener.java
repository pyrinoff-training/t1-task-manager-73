package ru.t1.pyrinov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import ru.t1.pyrinov.tm.event.ConsoleEvent;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.model.dto.request.TaskCreateRequest;
import ru.t1.pyrinov.tm.util.TerminalUtil;

@Component
public final class TaskCreateListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-create";

    @NotNull
    public static final String DESCRIPTION = "Create new task.";

    @Override
    @EventListener(condition = "@taskCreateListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[CREATE TASK]");
        System.out.print("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();
        @Nullable final TaskCreateRequest request = new TaskCreateRequest(getToken());
        request.setName(name);
        request.setDescription(description);
        taskEndpoint.createTask(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
