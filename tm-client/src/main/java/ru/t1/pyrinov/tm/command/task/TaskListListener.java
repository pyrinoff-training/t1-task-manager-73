package ru.t1.pyrinov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import ru.t1.pyrinov.tm.event.ConsoleEvent;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.model.dto.request.TaskListRequest;
import ru.t1.pyrinov.tm.model.dto.response.TaskListResponse;
import ru.t1.pyrinov.tm.enumerated.SortOrder;
import ru.t1.pyrinov.tm.model.dto.TaskDTO;
import ru.t1.pyrinov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class TaskListListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-list";

    @NotNull
    public static final String DESCRIPTION = "Display all tasks.";

    @Override
    @EventListener(condition = "@taskListListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(SortOrder.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final SortOrder sort = SortOrder.toSortOrder(sortType);
        @Nullable final TaskListRequest request = new TaskListRequest(getToken());
        request.setSort(sort);
        @Nullable final TaskListResponse response = taskEndpoint.getTasks(request);
        @Nullable final List<TaskDTO> tasks = response.getTasks();
        if (tasks == null) return;
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
