package ru.t1.pyrinov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import ru.t1.pyrinov.tm.event.ConsoleEvent;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.pyrinov.tm.api.endpoint.IUserEndpoint;
import ru.t1.pyrinov.tm.command.AbstractListener;
import ru.t1.pyrinov.tm.exception.entity.UserNotFoundException;
import ru.t1.pyrinov.tm.model.dto.UserDTO;

@Component
public abstract class AbstractUserListener extends AbstractListener {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Autowired
    protected @NotNull IUserEndpoint userEndpoint;

    @Autowired
    protected @NotNull IAuthEndpoint authEndpoint;

    protected void showUser(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
