package ru.t1.pyrinov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import ru.t1.pyrinov.tm.event.ConsoleEvent;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.model.dto.request.UserChangePasswordRequest;
import ru.t1.pyrinov.tm.enumerated.Role;
import ru.t1.pyrinov.tm.util.TerminalUtil;

@Component
public final class UserChangePasswordListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "change-user-pwd";

    @NotNull
    public static final String DESCRIPTION = "Change user password.";

    @Override
    @EventListener(condition = "@userChangePasswordListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[CHANGE PASSWORD]");
        System.out.print("NEW PASSWORD: ");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken());
        request.setPassword(newPassword);
        userEndpoint.changeUserPassword(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
