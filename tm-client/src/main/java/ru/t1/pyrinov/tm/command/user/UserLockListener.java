package ru.t1.pyrinov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import ru.t1.pyrinov.tm.event.ConsoleEvent;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.model.dto.request.UserLockRequest;
import ru.t1.pyrinov.tm.enumerated.Role;
import ru.t1.pyrinov.tm.util.TerminalUtil;

@Component
public final class UserLockListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "user-lock";

    @NotNull
    public static final String DESCRIPTION = "Lock user by login.";

    @Override
    @EventListener(condition = "@userLockListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER LOCK]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserLockRequest request = new UserLockRequest(getToken());
        request.setLogin(login);
        userEndpoint.lockUser(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
