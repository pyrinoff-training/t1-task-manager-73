package ru.t1.pyrinov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import ru.t1.pyrinov.tm.event.ConsoleEvent;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.model.dto.request.UserUpdateProfileRequest;
import ru.t1.pyrinov.tm.model.dto.response.UserUpdateProfileResponse;
import ru.t1.pyrinov.tm.enumerated.Role;
import ru.t1.pyrinov.tm.model.dto.UserDTO;
import ru.t1.pyrinov.tm.util.TerminalUtil;

@Component
public final class UserUpdateProfileListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "user-update-profile";

    @NotNull
    public static final String DESCRIPTION = "Update profile of current user.";

    @Override
    @EventListener(condition = "@userUpdateProfileListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.print("FIRST NAME: ");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.print("LAST NAME: ");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.print("MIDDLE NAME: ");
        @NotNull final String middleName = TerminalUtil.nextLine();
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(getToken());
        request.setFirstName(firstName);
        request.setLastName(lastName);
        request.setMiddleName(middleName);
        @Nullable final UserUpdateProfileResponse response = userEndpoint.updateUserProfile(request);
        @NotNull final UserDTO user = response.getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
