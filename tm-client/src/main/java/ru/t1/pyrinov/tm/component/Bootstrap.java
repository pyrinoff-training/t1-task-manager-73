package ru.t1.pyrinov.tm.component;

import com.sun.xml.ws.fault.ServerSOAPFaultException;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.api.endpoint.*;
import ru.t1.pyrinov.tm.api.service.*;
import ru.t1.pyrinov.tm.command.AbstractListener;
import ru.t1.pyrinov.tm.event.ConsoleEvent;
import ru.t1.pyrinov.tm.exception.AbstractException;
import ru.t1.pyrinov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.pyrinov.tm.exception.system.CommandNotSupportedException;
import ru.t1.pyrinov.tm.util.SystemUtil;
import ru.t1.pyrinov.tm.util.TerminalUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class Bootstrap {

    @Autowired
    private @NotNull ILoggerService loggerService;

    @Autowired
    private @NotNull IPropertyService propertyService;

    @Getter
    @Autowired
    private @NotNull ISystemEndpoint systemEndpointClient;

    @Getter
    @Autowired
    private @NotNull IProjectEndpoint projectEndpointClient;

    @Getter
    @Autowired
    private @NotNull ITaskEndpoint taskEndpointClient;

    @Getter
    @Autowired
    private @NotNull IAuthEndpoint authEndpointClient;

    @Getter
    @Autowired
    private @NotNull IUserEndpoint userEndpointClient;

    @Getter
    @Autowired
    private @NotNull ITokenService tokenService;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private @NotNull AbstractListener[] abstractListeners;


    @SneakyThrows
    public void start(@Nullable final String[] args) {
        initPID();
        showWelcomeMessage();
        initShutdownHook();
        processArguments(args);
        processCommands();
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        @Nullable final String argument = args[0];
        if (argument == null) return;
        try {
            processArgument(argument);
        } catch (final AbstractException e) {
            loggerService.error(e);
            System.out.println("[FAIL]");
        }
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.print("ENTER COMMAND: ");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                //} catch (final AbstractException e) { //#TODO Abstract exception
            } catch (final ServerSOAPFaultException | CommandNotSupportedException e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    public void processCommand(@NotNull final String command) {
        publisher.publishEvent(new ConsoleEvent(command));
    }

    private void processArgument(@NotNull final String argument) {
        @Nullable final AbstractListener abstractListener = getListenerByArgument(argument);
        if (abstractListener == null) throw new ArgumentNotSupportedException(argument);
        final @NotNull String argumentCommandName = abstractListener.getName();
        processCommand(argumentCommandName);
    }

    private @Nullable AbstractListener getListenerByArgument(@NotNull final String argument) {
        for (@NotNull final AbstractListener listener : abstractListeners) {
            if (argument.equals(listener.getArgument())) return listener;
        }
        return null;
    }

    private void showWelcomeMessage() {
        System.out.println("WELCOME TO TASK MANAGER!");
    }

    private void initShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            loggerService.info("** TASK MANAGER IS SHUTTING DOWN! **");
        }));
    }

    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(filename), pid.getBytes());
        } catch (@NotNull final IOException e) {
            e.printStackTrace();
        }
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

}
