package ru.t1.pyrinov.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.pyrinov.tm.api.endpoint.*;
import ru.t1.pyrinov.tm.api.service.IPropertyService;


@Configuration
@ComponentScan("ru.t1.pyrinov.tm")
public class ApplicationConfiguration {

    @Autowired
    private @NotNull IPropertyService propertyService;

    @Bean
    public @NotNull IAuthEndpoint getAuthEndpoint() {
        return IAuthEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());
    }

    @Bean
    public @NotNull ISystemEndpoint getSystemEndpoint() {
        return ISystemEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());
    }

    @Bean
    public @NotNull IProjectEndpoint getProjectEndpoint() {
        return IProjectEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());
    }

    @Bean
    public @NotNull ITaskEndpoint getTaskEndpoint() {
        return ITaskEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());
    }

    @Bean
    public @NotNull IUserEndpoint getUserEndpoint() {
        return IUserEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());
    }

}
