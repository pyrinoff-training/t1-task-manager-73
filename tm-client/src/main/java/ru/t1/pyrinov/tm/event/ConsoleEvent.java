package ru.t1.pyrinov.tm.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@AllArgsConstructor
public class ConsoleEvent {

    private final @NotNull String name;

}
