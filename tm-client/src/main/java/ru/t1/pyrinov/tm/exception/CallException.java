package ru.t1.pyrinov.tm.exception;

import org.jetbrains.annotations.Nullable;

public class CallException extends AbstractException {

    public CallException(@Nullable final String message) {
        super("Call exception! " + message);
    }

}
