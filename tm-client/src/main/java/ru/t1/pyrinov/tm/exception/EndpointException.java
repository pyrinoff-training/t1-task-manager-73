package ru.t1.pyrinov.tm.exception;

import org.jetbrains.annotations.Nullable;

public class EndpointException extends AbstractException {

    public EndpointException(@Nullable final String message) {
        super("Endpoint exception! " + message);
    }

}
