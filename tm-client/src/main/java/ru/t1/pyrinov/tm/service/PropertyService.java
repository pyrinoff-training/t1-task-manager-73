package ru.t1.pyrinov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.pyrinov.tm.api.service.IPropertyService;

@Service
@Getter
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    @Value("#{environment['server.host']}")
    private String serverHost;

    @NotNull
    @Value("#{environment['server.port']}")
    private Integer serverPort;

    @NotNull
    @Value("#{environment['admin.login']}")
    private String adminLogin;

    @NotNull
    @Value("#{environment['admin.password']}")
    private String adminPassword;

    public static final @NotNull String APPLICATION_VERSION_KEY = "version";

    public static final @NotNull String AUTHOR_NAME_KEY = "author_name";

    public static final @NotNull String AUTHOR_EMAIL_KEY = "author_name";

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }


}
