package ru.t1.pyrinov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.pyrinov.tm.api.service.ITokenService;

@Getter
@Setter
@Service
@NoArgsConstructor
public class TokenService implements ITokenService {

    @Nullable
    private String token;

}
