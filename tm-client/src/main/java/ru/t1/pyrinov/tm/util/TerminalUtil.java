package ru.t1.pyrinov.tm.util;

import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextLineInt() {
        @NotNull final String value = nextLine();
        return Integer.parseInt(value);
    }

}
