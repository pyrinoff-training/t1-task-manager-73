package ru.t1.pyrinov.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public final class ProjectTestData {

    @NotNull
    public final static String USER_PROJECT1_NAME = "PROJECT ONE";

    @NotNull
    public final static String USER_PROJECT1_DESCRIPTION = "Description for project one";

    @NotNull
    public final static String USER_PROJECT2_NAME = "PROJECT TWO";

    @NotNull
    public final static String USER_PROJECT2_DESCRIPTION = "Description for project two";

    @NotNull
    public final static String USER_PROJECT3_NAME = "PROJECT THREE";

    @NotNull
    public final static String USER_PROJECT3_DESCRIPTION = "Description for project three";

}
