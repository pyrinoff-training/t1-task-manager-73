package ru.t1.pyrinov.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public final class TaskTestData {

    @NotNull
    public final static String USER_TASK1_NAME = "TASK ONE";

    @NotNull
    public final static String USER_TASK1_DESCRIPTION = "Description for task one";

    @NotNull
    public final static String USER_TASK2_NAME = "TASK TWO";

    @NotNull
    public final static String USER_TASK2_DESCRIPTION = "Description for task two";

    @NotNull
    public final static String USER_TASK3_NAME = "TASK THREE";

    @NotNull
    public final static String USER_TASK3_DESCRIPTION = "Description for task three";


}
