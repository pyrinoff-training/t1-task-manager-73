package ru.t1.pyrinov.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public final class UserTestData {

    @NotNull
    public final static String USER_TEST_LOGIN = "user";

    @NotNull
    public final static String USER_TEST_PASSWORD = "user";

    @NotNull
    public final static String USER_TEST_EMAIL = "user@user.ru";

    @NotNull
    public final static String USER_TEST_LAST_NAME = "Иванов";

    @NotNull
    public final static String USER_TEST_FIRST_NAME = "Иван";

    @NotNull
    public final static String USER_TEST_MIDDLE_NAME = "Иванович";

}
