package ru.t1.pyrinov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.t1.pyrinov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.pyrinov.tm.api.endpoint.IUserEndpoint;
import ru.t1.pyrinov.tm.api.service.IPropertyService;
import ru.t1.pyrinov.tm.model.dto.request.*;
import ru.t1.pyrinov.tm.model.dto.response.UserProfileResponse;
import ru.t1.pyrinov.tm.marker.IntegrationTests;
import ru.t1.pyrinov.tm.model.dto.UserDTO;
import ru.t1.pyrinov.tm.service.PropertyService;

import static ru.t1.pyrinov.tm.constant.UserTestData.*;

public final class AuthEndpointTest implements IntegrationTests {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpointClient = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IUserEndpoint userEndpointClient = IUserEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @BeforeAll
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(propertyService.getAdminLogin());
        loginRequest.setPassword(propertyService.getAdminPassword());
        adminToken = authEndpointClient.login(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        request.setPassword(USER_TEST_PASSWORD);
        request.setEmail(USER_TEST_EMAIL);
        userEndpointClient.registryUser(request);
    }

    @AfterAll
    public static void tearDown() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        userEndpointClient.removeUser(request);
    }

    private String getUserToken() {
        @NotNull final UserLoginRequest request = new UserLoginRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        request.setPassword(USER_TEST_PASSWORD);
        return authEndpointClient.login(request).getToken();
    }

    @Test
    public void setupTest() {
    }

    @Test
    public void loginUser() {
        @Nullable final String token = getUserToken();
        Assertions.assertNotNull(token);
        @NotNull final UserProfileRequest viewRequest = new UserProfileRequest(token);
        @NotNull final UserProfileResponse userProfileResponse = authEndpointClient.profile(viewRequest);
        Assertions.assertNotNull(userProfileResponse.getUser());
        Assertions.assertEquals(USER_TEST_LOGIN, userProfileResponse.getUser().getLogin());
    }

    @Test
    public void viewProfileUser() {
        @Nullable final String token = getUserToken();
        @NotNull final UserProfileRequest request = new UserProfileRequest(token);
        @Nullable final UserDTO user = authEndpointClient.profile(request).getUser();
        Assertions.assertNotNull(user);
        Assertions.assertEquals(USER_TEST_LOGIN, user.getLogin());
    }

    @Test
    public void logoutUser() {
        @Nullable final String token = getUserToken();
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(token);
        authEndpointClient.logout(request);
        @NotNull final UserProfileRequest viewRequest = new UserProfileRequest(token);
        Assertions.assertThrows(Exception.class, () -> authEndpointClient.profile(viewRequest));
    }

}
