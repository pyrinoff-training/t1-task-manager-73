package ru.t1.pyrinov.tm.endpoint;

import com.sun.xml.ws.fault.ServerSOAPFaultException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;
import ru.t1.pyrinov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.pyrinov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.pyrinov.tm.api.endpoint.IUserEndpoint;
import ru.t1.pyrinov.tm.api.service.IPropertyService;
import ru.t1.pyrinov.tm.model.dto.request.*;
import ru.t1.pyrinov.tm.model.dto.response.UserRegistryResponse;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.marker.IntegrationTests;
import ru.t1.pyrinov.tm.model.dto.ProjectDTO;
import ru.t1.pyrinov.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;

import static ru.t1.pyrinov.tm.constant.ProjectTestData.*;
import static ru.t1.pyrinov.tm.constant.UserTestData.*;

public final class ProjectEndpointTest implements IntegrationTests {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpointClient = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IUserEndpoint userEndpointClient = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private static final IProjectEndpoint projectEndpointClient = IProjectEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @Nullable
    private String projectId1;

    private int projectIndex1;

    @Nullable
    private String projectId2;

    private int projectIndex2;

    List<String> testProjectIds = new ArrayList<>();

    @BeforeAll
    public static void setUp() {
        //Log in as admin
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(propertyService.getAdminLogin());
        loginRequest.setPassword(propertyService.getAdminPassword());
        adminToken = authEndpointClient.login(loginRequest).getToken();
        //Create test user
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        request.setPassword(USER_TEST_PASSWORD);
        request.setEmail(USER_TEST_EMAIL);
        UserRegistryResponse userRegistryResponse = userEndpointClient.registryUser(request);
        Assertions.assertNotNull(userRegistryResponse.getUser());
        //Log in into user
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin(USER_TEST_LOGIN);
        userLoginRequest.setPassword(USER_TEST_PASSWORD);
        userToken = authEndpointClient.login(userLoginRequest).getToken();
        Assertions.assertNotNull(userToken);
    }

    @AfterAll
    public static void tearDown() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        userEndpointClient.removeUser(request);
    }

    //#TODO брать индекс динамически, либо очищать полностью
    @BeforeEach
    public void before() {
        projectId1 = createTestProject(USER_PROJECT1_NAME, USER_PROJECT1_DESCRIPTION);
        projectIndex1 = 0;
        projectId2 = createTestProject(USER_PROJECT2_NAME, USER_PROJECT2_DESCRIPTION);
        projectIndex2 = 1;
    }

    @AfterEach
    public void after() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(userToken);
        Assertions.assertNotNull(projectEndpointClient.clearProject(request));
    }

    @Test
    public void preparationTest() {
    }

    @NotNull
    private String createTestProject(final String name, final String description) {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName(name);
        projectCreateRequest.setDescription(description);
        ProjectDTO project = projectEndpointClient.createProject(projectCreateRequest).getProject();
        Assertions.assertNotNull(project);
        return project.getId();
    }

    @Nullable
    private ProjectDTO findProjectById(final String id) {
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(userToken);
        request.setProjectId(id);
        return projectEndpointClient.showProjectById(request).getProject();
    }

    @Test
    public void changeProjectStatusById() {
        @NotNull final Status status = Status.COMPLETED;
        @NotNull final ProjectChangeStatusByIdRequest projectCreateRequestNullToken = new ProjectChangeStatusByIdRequest(null);
        projectCreateRequestNullToken.setProjectId(projectId1);
        projectCreateRequestNullToken.setStatus(status);
        Assertions.assertThrows(Exception.class, () -> projectEndpointClient.changeProjectStatusById(projectCreateRequestNullToken));
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(userToken);
        request.setProjectId(projectId1);
        request.setStatus(status);
        Assertions.assertNotNull(projectEndpointClient.changeProjectStatusById(request));
        @Nullable final ProjectDTO project = findProjectById(projectId1);
        Assertions.assertNotNull(project);
        Assertions.assertEquals(status, project.getStatus());
    }

    //#TODO сделать явные эксепшены для тестов
    @Test
    public void clearProject() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(userToken);
        Assertions.assertNotNull(projectEndpointClient.clearProject(request));
        Assertions.assertThrows(ServerSOAPFaultException.class, () -> findProjectById(projectId1));
        Assertions.assertThrows(ServerSOAPFaultException.class, () -> findProjectById(projectId2));
    }

    @Test
    public void createProject() {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName(USER_PROJECT3_NAME);
        projectCreateRequest.setDescription(USER_PROJECT3_DESCRIPTION);
        @Nullable ProjectDTO project = projectEndpointClient.createProject(projectCreateRequest).getProject();
        Assertions.assertNotNull(project);
        Assertions.assertEquals(USER_PROJECT3_NAME, project.getName());
        Assertions.assertEquals(USER_PROJECT3_DESCRIPTION, project.getDescription());
    }

    @Test
    public void completeById() {
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(userToken);
        request.setProjectId(projectId1);
        Assertions.assertNotNull(projectEndpointClient.completeProjectById(request));
        @Nullable ProjectDTO project = findProjectById(projectId1);
        Assertions.assertNotNull(project);
        Assertions.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void getProjects() {
        @NotNull final ProjectListRequest request = new ProjectListRequest(userToken);
        @Nullable final List<ProjectDTO> projects = projectEndpointClient.getProjectList(request).getProjects();
        Assertions.assertNotNull(projects);
        Assertions.assertEquals(2, projects.size());
        for (ProjectDTO project : projects) {
            Assertions.assertNotNull(findProjectById(project.getId()));
        }
    }

    @Test
    public void removeById() {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(userToken);
        request.setProjectId(projectId2);
        Assertions.assertNotNull(projectEndpointClient.removeProjectById(request));
        Assertions.assertThrows(ServerSOAPFaultException.class, () -> findProjectById(projectId2));
    }

    @Test
    public void showById() {
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(userToken);
        request.setProjectId(projectId1);
        @Nullable final ProjectDTO project = projectEndpointClient.showProjectById(request).getProject();
        Assertions.assertNotNull(project);
        Assertions.assertEquals(projectId1, project.getId());
    }

    @Test
    public void startById() {
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(userToken);
        request.setProjectId(projectId1);
        Assertions.assertNotNull(projectEndpointClient.startProjectById(request));
        @Nullable ProjectDTO project = findProjectById(projectId1);
        Assertions.assertNotNull(project);
        Assertions.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void updateById() {
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(userToken);
        request.setProjectId(projectId1);
        request.setName(USER_PROJECT3_NAME);
        request.setDescription(USER_PROJECT3_DESCRIPTION);
        Assertions.assertNotNull(projectEndpointClient.updateProjectById(request));
        @Nullable ProjectDTO project = findProjectById(projectId1);
        Assertions.assertNotNull(project);
        Assertions.assertEquals(USER_PROJECT3_NAME, project.getName());
        Assertions.assertEquals(USER_PROJECT3_DESCRIPTION, project.getDescription());
    }

}
