package ru.t1.pyrinov.tm.endpoint;

import com.sun.xml.ws.fault.ServerSOAPFaultException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;
import ru.t1.pyrinov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.pyrinov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.pyrinov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.pyrinov.tm.api.endpoint.IUserEndpoint;
import ru.t1.pyrinov.tm.api.service.IPropertyService;
import ru.t1.pyrinov.tm.model.dto.request.*;
import ru.t1.pyrinov.tm.model.dto.response.UserRegistryResponse;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.marker.IntegrationTests;
import ru.t1.pyrinov.tm.model.dto.ProjectDTO;
import ru.t1.pyrinov.tm.model.dto.TaskDTO;
import ru.t1.pyrinov.tm.service.PropertyService;

import java.util.List;

import static ru.t1.pyrinov.tm.constant.ProjectTestData.USER_PROJECT1_DESCRIPTION;
import static ru.t1.pyrinov.tm.constant.ProjectTestData.USER_PROJECT1_NAME;
import static ru.t1.pyrinov.tm.constant.TaskTestData.*;
import static ru.t1.pyrinov.tm.constant.UserTestData.*;

public final class TaskEndpointTest implements IntegrationTests {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpointClient = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IUserEndpoint userEndpointClient = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private static final IProjectEndpoint projectEndpointClient = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private static final ITaskEndpoint taskEndpointClient = ITaskEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @Nullable
    private String projectId1;

    @Nullable
    private String taskId1;

    private int taskIndex1;

    @Nullable
    private String taskId2;

    private int taskIndex2;

    @BeforeAll
    public static void setUp() {
        //Log in as admin
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(propertyService.getAdminLogin());
        loginRequest.setPassword(propertyService.getAdminPassword());
        adminToken = authEndpointClient.login(loginRequest).getToken();
        //Create test user
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        request.setPassword(USER_TEST_PASSWORD);
        request.setEmail(USER_TEST_EMAIL);
        UserRegistryResponse userRegistryResponse = userEndpointClient.registryUser(request);
        Assertions.assertNotNull(userRegistryResponse.getUser());
        //Log in into user
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin(USER_TEST_LOGIN);
        userLoginRequest.setPassword(USER_TEST_PASSWORD);
        userToken = authEndpointClient.login(userLoginRequest).getToken();
        Assertions.assertNotNull(userToken);
    }

    @AfterAll
    public static void tearDown() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        userEndpointClient.removeUser(request);
    }

    @BeforeEach
    public void before() {
        taskId1 = createTestTask(USER_TASK1_NAME, USER_TASK1_DESCRIPTION);
        taskIndex1 = 0;
        taskId2 = createTestTask(USER_TASK2_NAME, USER_TASK2_DESCRIPTION);
        taskIndex2 = 1;
        projectId1 = createTestProject(USER_PROJECT1_NAME, USER_PROJECT1_DESCRIPTION);
    }

    @AfterEach
    public void after() {
        @NotNull final TaskClearRequest request = new TaskClearRequest(userToken);
        Assertions.assertNotNull(taskEndpointClient.clearTask(request));
    }

    @NotNull
    private String createTestTask(final String name, final String description) {
        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(userToken);
        taskCreateRequest.setName(name);
        taskCreateRequest.setDescription(description);
        TaskDTO task = taskEndpointClient.createTask(taskCreateRequest).getTask();
        Assertions.assertNotNull(task);
        return task.getId();
    }

    @NotNull
    private String createTestProject(final String name, final String description) {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName(name);
        projectCreateRequest.setDescription(description);
        ProjectDTO project = projectEndpointClient.createProject(projectCreateRequest).getProject();
        Assertions.assertNotNull(project);
        return project.getId();
    }

    @Nullable
    private TaskDTO findTestTaskById(final String id) {
        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(userToken);
        request.setId(id);
        return taskEndpointClient.getTaskById(request).getTask();
    }

    @Test
    public void changeStatusByIdTask() {
        @NotNull final Status status = Status.COMPLETED;
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(userToken);
        request.setId(taskId1);
        request.setStatus(status);
        Assertions.assertNotNull(taskEndpointClient.changeTaskStatusById(request));
        @Nullable final TaskDTO task = findTestTaskById(taskId1);
        Assertions.assertNotNull(task);
        Assertions.assertEquals(status, task.getStatus());
    }

    @Test
    public void clearTask() {
        @NotNull final TaskClearRequest request = new TaskClearRequest(userToken);
        Assertions.assertNotNull(taskEndpointClient.clearTask(request));
        Assertions.assertThrows(ServerSOAPFaultException.class, () -> findTestTaskById(taskId1));
        Assertions.assertThrows(ServerSOAPFaultException.class, () -> findTestTaskById(taskId2));
    }

    @Test
    public void createTask() {
        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(userToken);
        taskCreateRequest.setName(USER_TASK3_NAME);
        taskCreateRequest.setDescription(USER_TASK3_DESCRIPTION);
        @Nullable TaskDTO task = taskEndpointClient.createTask(taskCreateRequest).getTask();
        Assertions.assertNotNull(task);
        Assertions.assertEquals(USER_TASK3_NAME, task.getName());
        Assertions.assertEquals(USER_TASK3_DESCRIPTION, task.getDescription());
    }

    @Test
    public void completeByIdTask() {
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(userToken);
        request.setId(taskId1);
        Assertions.assertNotNull(taskEndpointClient.completeTaskById(request));
        @Nullable TaskDTO task = findTestTaskById(taskId1);
        Assertions.assertNotNull(task);
        Assertions.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void getTasks() {
        @NotNull final TaskListRequest request = new TaskListRequest(userToken);
        @Nullable final List<TaskDTO> tasks = taskEndpointClient.getTasks(request).getTasks();
        Assertions.assertNotNull(tasks);
        Assertions.assertEquals(2, tasks.size());
        for (TaskDTO task : tasks) {
            Assertions.assertNotNull(findTestTaskById(task.getId()));
        }
    }

    @Test
    public void removeByIdTask() {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(userToken);
        request.setId(taskId2);
        Assertions.assertNotNull(taskEndpointClient.removeTaskById(request));
        Assertions.assertThrows(ServerSOAPFaultException.class, () -> findTestTaskById(taskId2));
    }

    @Test
    public void showByIdTask() {
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(userToken);
        request.setTaskId(taskId1);
        @Nullable final TaskDTO task = taskEndpointClient.showTaskById(request).getTask();
        Assertions.assertNotNull(task);
        Assertions.assertEquals(taskId1, task.getId());
    }

    @Test
    public void startByIdTask() {
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(userToken);
        request.setId(taskId1);
        Assertions.assertNotNull(taskEndpointClient.startTaskById(request));
        @Nullable TaskDTO task = findTestTaskById(taskId1);
        Assertions.assertNotNull(task);
        Assertions.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void updateByIdTask() {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(userToken);
        request.setId(taskId1);
        request.setName(USER_TASK3_NAME);
        request.setDescription(USER_TASK3_DESCRIPTION);
        Assertions.assertNotNull(taskEndpointClient.updateTaskById(request));
        @Nullable TaskDTO task = findTestTaskById(taskId1);
        Assertions.assertNotNull(task);
        Assertions.assertEquals(USER_TASK3_NAME, task.getName());
        Assertions.assertEquals(USER_TASK3_DESCRIPTION, task.getDescription());
    }

    private void bindTaskProject(final String projectId, final String taskId) {
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(userToken);
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        taskEndpointClient.bindTaskToProject(request);
    }

    @Test
    public void bindToProjectTask() {
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(userToken);
        request.setProjectId(projectId1);
        request.setTaskId(taskId1);
        Assertions.assertNotNull(taskEndpointClient.bindTaskToProject(request));
        @NotNull final TaskListByProjectIdRequest taskListByProjectIdRequest = new TaskListByProjectIdRequest(userToken);
        taskListByProjectIdRequest.setProjectId(projectId1);
        @Nullable final List<TaskDTO> tasks = taskEndpointClient.getTasksByProjectId(taskListByProjectIdRequest).getTasks();
        Assertions.assertNotNull(tasks);
        Assertions.assertEquals(1, tasks.size());
        Assertions.assertEquals(projectId1, tasks.get(0).getProjectId());
        Assertions.assertEquals(taskId1, tasks.get(0).getId());
    }

    @Test
    public void showByProjectIdTask() {
        bindTaskProject(projectId1, taskId1);
        bindTaskProject(projectId1, taskId2);
        @NotNull final TaskListByProjectIdRequest taskListByProjectIdRequest = new TaskListByProjectIdRequest(userToken);
        taskListByProjectIdRequest.setProjectId(projectId1);
        @Nullable final List<TaskDTO> tasks = taskEndpointClient.getTasksByProjectId(taskListByProjectIdRequest).getTasks();
        Assertions.assertNotNull(tasks);
        Assertions.assertEquals(2, tasks.size());
        for (TaskDTO task : tasks) {
            Assertions.assertEquals(projectId1, task.getProjectId());
        }
    }

    @Test
    public void unbindFromProjectTask() {
        bindTaskProject(projectId1, taskId1);
        bindTaskProject(projectId1, taskId2);
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(userToken);
        request.setProjectId(projectId1);
        request.setTaskId(taskId1);
        Assertions.assertNotNull(taskEndpointClient.unbindTaskFromProject(request));
        @NotNull final TaskListByProjectIdRequest taskListByProjectIdRequest = new TaskListByProjectIdRequest(userToken);
        taskListByProjectIdRequest.setProjectId(projectId1);
        @Nullable final List<TaskDTO> tasks = taskEndpointClient.getTasksByProjectId(taskListByProjectIdRequest).getTasks();
        Assertions.assertNotNull(tasks);
        Assertions.assertEquals(1, tasks.size());
        Assertions.assertEquals(projectId1, tasks.get(0).getProjectId());
        Assertions.assertEquals(taskId2, tasks.get(0).getId());
    }

}
