package ru.t1.pyrinov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.t1.pyrinov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.pyrinov.tm.api.endpoint.IUserEndpoint;
import ru.t1.pyrinov.tm.api.service.IPropertyService;
import ru.t1.pyrinov.tm.marker.IntegrationTests;
import ru.t1.pyrinov.tm.model.dto.UserDTO;
import ru.t1.pyrinov.tm.model.dto.request.*;
import ru.t1.pyrinov.tm.service.PropertyService;

import java.util.Locale;

import static ru.t1.pyrinov.tm.constant.UserTestData.*;

public final class UserEndpointTest implements IntegrationTests {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpointClient = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IUserEndpoint userEndpointClient = IUserEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @BeforeAll
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(propertyService.getAdminLogin());
        loginRequest.setPassword(propertyService.getAdminPassword());
        adminToken = authEndpointClient.login(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        request.setPassword(USER_TEST_PASSWORD);
        request.setEmail(USER_TEST_EMAIL);
        userEndpointClient.registryUser(request);
    }

    @AfterAll
    public static void tearDown() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        userEndpointClient.removeUser(request);
    }

    @Nullable
    private String getUserToken(final String login, final String password) {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        return authEndpointClient.login(request).getToken();
    }

    @Test
    public void setupTest() {
    }

    @Test
    public void changePasswordUser() {
        @Nullable String token = getUserToken(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        Assertions.assertNotNull(token);
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(token);
        request.setPassword(USER_TEST_PASSWORD.toLowerCase(Locale.ROOT));
        Assertions.assertNotNull(userEndpointClient.changeUserPassword(request));
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(token);
        authEndpointClient.logout(logoutRequest);
        token = getUserToken(USER_TEST_LOGIN, USER_TEST_PASSWORD.toLowerCase(Locale.ROOT));
        Assertions.assertNotNull(token);
        @NotNull final UserChangePasswordRequest newPasswordRequest = new UserChangePasswordRequest(token);
        newPasswordRequest.setPassword(USER_TEST_PASSWORD);
        Assertions.assertNotNull(userEndpointClient.changeUserPassword(newPasswordRequest));
    }

    @Test
    public void lockUser() {
        @NotNull final UserLockRequest request = new UserLockRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        Assertions.assertNotNull(userEndpointClient.lockUser(request));
        Assertions.assertThrows(Exception.class, () -> getUserToken(USER_TEST_LOGIN, USER_TEST_PASSWORD));
        unlockUser();
    }

    @Test
    public void registryUser() {
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken);
        removeRequest.setLogin(USER_TEST_LOGIN);
        userEndpointClient.removeUser(removeRequest);
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        request.setPassword(USER_TEST_PASSWORD);
        request.setEmail(USER_TEST_EMAIL);
        userEndpointClient.registryUser(request);
        Assertions.assertNotNull(getUserToken(USER_TEST_LOGIN, USER_TEST_PASSWORD));
    }

    @Test
    public void removeUser() {
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken);
        removeRequest.setLogin(USER_TEST_LOGIN);
        Assertions.assertNotNull(userEndpointClient.removeUser(removeRequest));
        Assertions.assertThrows(Exception.class, () -> getUserToken(USER_TEST_LOGIN, USER_TEST_PASSWORD));
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        request.setPassword(USER_TEST_PASSWORD);
        request.setEmail(USER_TEST_EMAIL);
        userEndpointClient.registryUser(request);
    }

    @Test
    public void unlockUser() {
        @NotNull final UserUnlockRequest request = new UserUnlockRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        Assertions.assertNotNull(userEndpointClient.unlockUser(request));
        Assertions.assertNotNull(getUserToken(USER_TEST_LOGIN, USER_TEST_PASSWORD));
    }

    @Test
    public void updateProfileUser() {
        @Nullable final String token = getUserToken(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(token);
        request.setFirstName(USER_TEST_FIRST_NAME);
        request.setLastName(USER_TEST_LAST_NAME);
        request.setMiddleName(USER_TEST_MIDDLE_NAME);
        Assertions.assertNotNull(userEndpointClient.updateUserProfile(request));
        @NotNull final UserProfileRequest viewRequest = new UserProfileRequest(token);
        @Nullable final UserDTO user = authEndpointClient.profile(viewRequest).getUser();
        Assertions.assertNotNull(user);
        Assertions.assertEquals(USER_TEST_FIRST_NAME, user.getFirstName());
        Assertions.assertEquals(USER_TEST_LAST_NAME, user.getLastName());
        Assertions.assertEquals(USER_TEST_MIDDLE_NAME, user.getMiddleName());
    }

}
