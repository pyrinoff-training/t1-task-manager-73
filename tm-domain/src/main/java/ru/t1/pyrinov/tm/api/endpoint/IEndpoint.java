package ru.t1.pyrinov.tm.api.endpoint;

import jakarta.jws.WebMethod;
import jakarta.xml.ws.Service;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.pyrinov.tm.api.provider.IConnectionProvider;

import javax.xml.namespace.QName;
import java.net.URL;

public interface IEndpoint {

    @NotNull
    String HOST = "0.0.0.0";

    @NotNull
    Integer PORT = 8080;

    @NotNull
    String REQUEST = "request";

    @NotNull
    String SPACE = "http://endpoint.tm.pyrinov.t1.ru/";

    @SneakyThrows
    @WebMethod(exclude = true)
    static <T> T newInstance(
            @NotNull final String name, @NotNull final String space,
            @NotNull final String part, @NotNull final Class<T> clazz
    ) {
        @NotNull final String host = HOST;
        @NotNull final Integer port = PORT;
        return newInstance(host, port, name, space, part, clazz);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static <T> T newInstance(
            @NotNull final IConnectionProvider connectionProvider,
            @NotNull final String name, @NotNull final String space,
            @NotNull final String part, @NotNull final Class<T> clazz
    ) {
        @NotNull final String host = connectionProvider.getServerHost();
        @NotNull final Integer port = connectionProvider.getServerPort();
        return newInstance(host, port, name, space, part, clazz);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static <T> T newInstance(
            @NotNull final String host, @NotNull final Integer port,
            @NotNull final String name, @NotNull final String space,
            @NotNull final String part, @NotNull final Class<T> clazz
    ) {
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(space, part);
        return Service.create(url, qName).getPort(clazz);
    }

}
