package ru.t1.pyrinov.tm.api.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.pyrinov.tm.api.provider.IConnectionProvider;
import ru.t1.pyrinov.tm.model.dto.request.ServerAboutRequest;
import ru.t1.pyrinov.tm.model.dto.request.ServerVersionRequest;
import ru.t1.pyrinov.tm.model.dto.response.ServerAboutResponse;
import ru.t1.pyrinov.tm.model.dto.response.ServerVersionResponse;

@WebService
public interface ISystemEndpoint extends IEndpoint {

    @NotNull
    String NAME = "SystemEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, ISystemEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(@NotNull final String host, @NotNull final Integer port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, ISystemEndpoint.class);
    }

    @NotNull
    @WebMethod
    ServerAboutResponse getAbout(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ServerAboutRequest request);

    @NotNull
    @WebMethod
    ServerVersionResponse getVersion(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ServerVersionRequest request);

}
