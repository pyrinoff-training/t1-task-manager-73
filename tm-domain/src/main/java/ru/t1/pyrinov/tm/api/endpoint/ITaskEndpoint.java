package ru.t1.pyrinov.tm.api.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.pyrinov.tm.api.provider.IConnectionProvider;
import ru.t1.pyrinov.tm.model.dto.request.*;
import ru.t1.pyrinov.tm.model.dto.response.*;


@WebService(endpointInterface = "ru.t1.pyrinov.tm.api.endpoint.ITaskEndpoint")
public interface ITaskEndpoint extends IEndpoint {

    @NotNull
    String NAME = "TaskEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, ITaskEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance(@NotNull final String host, @NotNull final Integer port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, ITaskEndpoint.class);
    }

    @NotNull
    @WebMethod
    TaskBindToProjectResponse bindTaskToProject(@WebParam(name = REQUEST, partName = REQUEST)
                                                @NotNull TaskBindToProjectRequest request);

    @NotNull
    @WebMethod
    TaskChangeStatusByIdResponse changeTaskStatusById(@WebParam(name = REQUEST, partName = REQUEST)
                                                      @NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    @WebMethod
    TaskClearResponse clearTask(@WebParam(name = REQUEST, partName = REQUEST)
                                @NotNull TaskClearRequest request);

    @NotNull
    @WebMethod
    TaskCompleteByIdResponse completeTaskById(@WebParam(name = REQUEST, partName = REQUEST)
                                              @NotNull TaskCompleteByIdRequest request);

    @NotNull
    @WebMethod
    TaskCreateResponse createTask(@WebParam(name = REQUEST, partName = REQUEST)
                                  @NotNull TaskCreateRequest request);

    @NotNull
    @WebMethod
    TaskGetByIdResponse getTaskById(@WebParam(name = REQUEST, partName = REQUEST)
                                    @NotNull TaskGetByIdRequest request);

    @NotNull
    @WebMethod
    TaskListResponse getTasks(@WebParam(name = REQUEST, partName = REQUEST)
                              @NotNull TaskListRequest request);

    @NotNull
    @WebMethod
    TaskListByProjectIdResponse getTasksByProjectId(@WebParam(name = REQUEST, partName = REQUEST)
                                                    @NotNull TaskListByProjectIdRequest request);

    @NotNull
    @WebMethod
    TaskRemoveByIdResponse removeTaskById(@WebParam(name = REQUEST, partName = REQUEST)
                                          @NotNull TaskRemoveByIdRequest request);

    @NotNull
    @WebMethod
    TaskShowByIdResponse showTaskById(@WebParam(name = REQUEST, partName = REQUEST)
                                      @NotNull TaskShowByIdRequest request);

    @NotNull
    @WebMethod
    TaskStartByIdResponse startTaskById(@WebParam(name = REQUEST, partName = REQUEST)
                                        @NotNull TaskStartByIdRequest request);

    @NotNull
    @WebMethod
    TaskUnbindFromProjectResponse unbindTaskFromProject(@WebParam(name = REQUEST, partName = REQUEST)
                                                        @NotNull TaskUnbindFromProjectRequest request);

    @NotNull
    @WebMethod
    TaskUpdateByIdResponse updateTaskById(@WebParam(name = REQUEST, partName = REQUEST)
                                          @NotNull TaskUpdateByIdRequest request);

}
