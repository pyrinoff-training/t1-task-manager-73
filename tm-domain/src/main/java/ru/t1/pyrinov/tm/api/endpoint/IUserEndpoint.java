package ru.t1.pyrinov.tm.api.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.pyrinov.tm.api.provider.IConnectionProvider;
import ru.t1.pyrinov.tm.model.dto.request.*;
import ru.t1.pyrinov.tm.model.dto.response.*;

@WebService
public interface IUserEndpoint extends IEndpoint {

    @NotNull
    String NAME = "UserEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final String host, @NotNull final Integer port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserChangePasswordResponse changeUserPassword(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserChangePasswordRequest request
    );

    @NotNull
    @WebMethod
    UserLockResponse lockUser(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserLockRequest request
    );

    @NotNull
    @WebMethod
    UserRegistryResponse registryUser(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserRegistryRequest request
    );

    @NotNull
    @WebMethod
    UserRemoveResponse removeUser(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserRemoveRequest request
    );

    @NotNull
    @WebMethod
    UserUnlockResponse unlockUser(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserUnlockRequest request
    );

    @NotNull
    @WebMethod
    UserUpdateProfileResponse updateUserProfile(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserUpdateProfileRequest request
    );

}
