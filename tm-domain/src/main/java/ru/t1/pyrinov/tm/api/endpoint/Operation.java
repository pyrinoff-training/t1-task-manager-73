package ru.t1.pyrinov.tm.api.endpoint;

import ru.t1.pyrinov.tm.model.dto.request.AbstractRequest;
import ru.t1.pyrinov.tm.model.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}
