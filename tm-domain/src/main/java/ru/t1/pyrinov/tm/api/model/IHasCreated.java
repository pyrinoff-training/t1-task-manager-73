package ru.t1.pyrinov.tm.api.model;

import org.jetbrains.annotations.NotNull;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

public interface IHasCreated {

    @NotNull
    OffsetDateTime getCreated();

    void setCreated(OffsetDateTime created);

    @NotNull
    static OffsetDateTime getCurrent() {
        return OffsetDateTime.now(ZoneOffset.UTC).truncatedTo(ChronoUnit.SECONDS);
    }

    /*
    static boolean isEqualWithoutMs(@NotNull OffsetDateTime obj1, @NotNull OffsetDateTime obj2) {
        return IHasCreated.truncateMs(obj1).equals(IHasCreated.truncateMs(obj2));
    }

    static Timestamp toTimestampUTC(@NotNull OffsetDateTime date) {
        return Timestamp.valueOf(LocalDateTime.ofInstant(date.toInstant(), ZoneOffset.UTC));
    }*/

    static long toLongUTC(@NotNull OffsetDateTime date) {
        return Timestamp.valueOf(LocalDateTime.ofInstant(date.toInstant(), ZoneOffset.UTC)).getTime();
    }

    static OffsetDateTime truncateMs(@NotNull OffsetDateTime date) {
        return date.truncatedTo(ChronoUnit.SECONDS);
    }

}
