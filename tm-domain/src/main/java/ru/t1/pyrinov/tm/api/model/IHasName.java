package ru.t1.pyrinov.tm.api.model;

import org.jetbrains.annotations.NotNull;

public interface IHasName {

    @NotNull
    String getName();

    void setName(String name);

}
