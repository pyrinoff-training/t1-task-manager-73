package ru.t1.pyrinov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.pyrinov.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(Status name);

}
