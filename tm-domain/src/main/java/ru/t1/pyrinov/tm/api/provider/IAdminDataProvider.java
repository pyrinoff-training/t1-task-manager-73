package ru.t1.pyrinov.tm.api.provider;

import org.jetbrains.annotations.NotNull;

public interface IAdminDataProvider {

    @NotNull String getAdminLogin();

    @NotNull String getAdminPassword();

}
