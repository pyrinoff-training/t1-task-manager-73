package ru.t1.pyrinov.tm.api.provider;

import org.jetbrains.annotations.NotNull;

public interface IConnectionProvider {

    @NotNull Integer getServerPort();

    @NotNull String getServerHost();

}
