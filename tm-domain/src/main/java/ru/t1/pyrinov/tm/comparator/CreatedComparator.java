package ru.t1.pyrinov.tm.comparator;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.api.model.IHasCreated;

import java.util.Comparator;

public enum CreatedComparator implements Comparator<IHasCreated> {

    INSTANCE;

    @NotNull
    @Override
    public int compare(@Nullable final IHasCreated o1, @Nullable final IHasCreated o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getCreated() == null || o2.getCreated() == null) return 0;
        return o1.getCreated().compareTo(o2.getCreated());
    }

}
