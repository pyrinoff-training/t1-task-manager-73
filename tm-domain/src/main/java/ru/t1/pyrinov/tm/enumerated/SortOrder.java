package ru.t1.pyrinov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.comparator.CreatedComparator;
import ru.t1.pyrinov.tm.comparator.NameComparator;
import ru.t1.pyrinov.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum SortOrder {

    BY_NAME("Sort by name"),
    BY_STATUS("Sort by status"),
    BY_CREATED("Sort by created");

    @Getter
    private final @NotNull String displayName;

    @Nullable
    public static SortOrder toSortOrder(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final SortOrder oneSortOrder : values()) {
            if (oneSortOrder.name().equals(value)) return oneSortOrder;
        }
        return null;
    }

    SortOrder(@NotNull final String displayName) {
        this.displayName = displayName;
    }

}
