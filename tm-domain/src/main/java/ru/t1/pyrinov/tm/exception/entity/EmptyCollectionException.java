package ru.t1.pyrinov.tm.exception.entity;

public class EmptyCollectionException extends AbstractEntityNotFoundException {

    public EmptyCollectionException() {
        super("Error! Collection is null or empty!");
    }

}
