package ru.t1.pyrinov.tm.exception.entity;

public class ProjectNotFoundException extends EntityNotFoundException {

    public ProjectNotFoundException() {
        super("Error! Project not found!");
    }

}
