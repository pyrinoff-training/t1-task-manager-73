package ru.t1.pyrinov.tm.exception.entity;


public class SessionNotFoundException extends AbstractEntityNotFoundException {

    public SessionNotFoundException() {
        super("Error! Session not found!");
    }

}
