package ru.t1.pyrinov.tm.exception.entity;

public class TaskNotFoundException extends EntityNotFoundException {

    public TaskNotFoundException() {
        super("Error! Task not found!");
    }

}
