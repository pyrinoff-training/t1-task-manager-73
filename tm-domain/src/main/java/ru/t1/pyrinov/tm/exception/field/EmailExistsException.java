package ru.t1.pyrinov.tm.exception.field;

public class EmailExistsException extends AbstractFieldException {

    public EmailExistsException() {
        super("Error! Email already exists!");
    }

}
