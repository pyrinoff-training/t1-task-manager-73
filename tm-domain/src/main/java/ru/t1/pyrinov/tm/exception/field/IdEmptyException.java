package ru.t1.pyrinov.tm.exception.field;

public class IdEmptyException extends AbstractFieldException {

    public IdEmptyException() {
        super("Error! Id is empty!");
    }

}
