package ru.t1.pyrinov.tm.exception.field;

public class IndexIncorrectException extends AbstractFieldException {

    public IndexIncorrectException() {
        super("Error! Index is incorrect!");
    }

}
