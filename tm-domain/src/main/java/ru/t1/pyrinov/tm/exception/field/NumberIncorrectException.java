package ru.t1.pyrinov.tm.exception.field;

import org.jetbrains.annotations.Nullable;

public class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("Error! Number is incorrect!");
    }

    public NumberIncorrectException(@Nullable final String value) {
        super("Error! This value '" + value + "' is incorrect...");
    }

}
