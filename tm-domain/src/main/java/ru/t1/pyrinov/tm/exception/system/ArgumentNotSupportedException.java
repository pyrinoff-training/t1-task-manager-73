package ru.t1.pyrinov.tm.exception.system;

import org.jetbrains.annotations.Nullable;

public class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Argument is not supported!");
    }

    public ArgumentNotSupportedException(@Nullable final String argument) {
        super("Error! Argument '" + argument + "' is not supported!");
    }

}
