package ru.t1.pyrinov.tm.exception.system;

import org.jetbrains.annotations.Nullable;

public class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command is not supported!");
    }

    public CommandNotSupportedException(@Nullable final String argument) {
        super("Error! Command '" + argument + "' is not supported!");
    }

}
