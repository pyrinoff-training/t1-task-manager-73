package ru.t1.pyrinov.tm.exception.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Access denied!");
    }

    public AccessDeniedException(@Nullable final String message) {
        super("Access denied! " + message);
    }

}
