package ru.t1.pyrinov.tm.exception.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.exception.AbstractException;

public class AuthenticationException extends AbstractException {

    public AuthenticationException() {
        super("Authentication failed!");
    }

    public AuthenticationException(@Nullable final String message) {
        super("Authentification failed: " + message);
    }

}
