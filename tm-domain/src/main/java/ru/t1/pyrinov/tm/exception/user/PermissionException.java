package ru.t1.pyrinov.tm.exception.user;

import ru.t1.pyrinov.tm.exception.AbstractException;

public class PermissionException extends AbstractException {

    public PermissionException() {
        super("Not enough permissions for that action!");
    }

}
