package ru.t1.pyrinov.tm.marshalling;

import jakarta.xml.bind.annotation.adapters.XmlAdapter;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

public class OffsetDateTimeAdapter extends XmlAdapter<String, OffsetDateTime> {

    private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Override
    public String marshal(OffsetDateTime dateTime) {
        return dateTime.format(dateFormat);
    }

    @Override
    public OffsetDateTime unmarshal(String dateTime) {
        return OffsetDateTime.parse(dateTime, dateFormat);
    }

}
