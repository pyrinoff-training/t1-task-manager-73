package ru.t1.pyrinov.tm.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.api.model.IHasCreated;
import ru.t1.pyrinov.tm.api.model.IWBS;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.marshalling.OffsetDateTimeAdapter;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

@Getter
@Setter
@Entity
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "projects", schema = "public")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class ProjectDTO extends AbstractUserOwnedDTOModel implements IWBS {

    private static final long serialVersionUID = 1;

    @Column
    @NotNull
    private String name = "";

    @Column
    @NotNull
    private String description = "";

    @Column
    @Nullable
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Column
    @NotNull
    @XmlJavaTypeAdapter(OffsetDateTimeAdapter.class)
    private OffsetDateTime created = getCurrentDate();

    public ProjectDTO(final @NotNull String name, final @NotNull Status status) {
        this.name = name;
        this.status = status;
    }

    public ProjectDTO(final @NotNull String name, final @NotNull Status status, @NotNull final String userId) {
        this.name = name;
        this.status = status;
        setUserId(userId);
    }

    @NotNull
    @Override
    public String toString() {
        return name + " | " + description + " | " + status.getDisplayName() + " (ID: " + getId() + ", created: "
                + created.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSSSXXXXX'Z'"))
                + ")";
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created == null
                ? IHasCreated.getCurrent()
                : IHasCreated.truncateMs(created).withOffsetSameInstant(ZoneOffset.UTC);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectDTO project = (ProjectDTO) o;
        return name.equals(project.name) && description.equals(project.description) && status == project.status && created.equals(project.created);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, status, created);
    }

}
