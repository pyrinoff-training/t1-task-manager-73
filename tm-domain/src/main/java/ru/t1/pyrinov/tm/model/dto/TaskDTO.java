package ru.t1.pyrinov.tm.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.api.model.IHasCreated;
import ru.t1.pyrinov.tm.api.model.IWBS;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.marshalling.OffsetDateTimeAdapter;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Objects;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tasks", schema = "public")
@XmlAccessorType(XmlAccessType.FIELD)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties(ignoreUnknown = true)
public final class TaskDTO extends AbstractUserOwnedDTOModel implements IWBS {

    private static final long serialVersionUID = 1;

    @Column
    @NotNull
    private String name = "";

    @Column
    @NotNull
    private String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    @Column
    @NotNull
    @XmlJavaTypeAdapter(OffsetDateTimeAdapter.class)
    private OffsetDateTime created = getCurrentDate();

    public TaskDTO(final @NotNull String name, final @NotNull Status status) {
        this.name = name;
        this.status = status;
    }

    public TaskDTO(final @NotNull String name, final @NotNull Status status, @NotNull final String userId) {
        this.name = name;
        this.status = status;
        setUserId(userId);
    }

    @NotNull
    @Override
    public String toString() {
        return name + "| " + description + " - " + status.getDisplayName() + " (ID: " + getId() + ", bound to project id: " + projectId + ", created: " + created + ")";
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created == null
                ? IHasCreated.getCurrent()
                : IHasCreated.truncateMs(created).withOffsetSameInstant(ZoneOffset.UTC);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskDTO task = (TaskDTO) o;
        return name.equals(task.name) && description.equals(task.description) && status == task.status && Objects.equals(projectId, task.projectId) && created.equals(task.created);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, status, projectId, created);
    }

}
