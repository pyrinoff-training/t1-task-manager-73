package ru.t1.pyrinov.tm.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.api.model.IHasCreated;
import ru.t1.pyrinov.tm.enumerated.Role;
import ru.t1.pyrinov.tm.marshalling.OffsetDateTimeAdapter;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "users", schema = "public")
@Accessors(chain = true)
@XmlAccessorType(XmlAccessType.FIELD)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO extends AbstractDTOModel {

    private static final long serialVersionUID = 1;

    @Column
    @Nullable
    private String login;

    @Nullable
    @Column(name = "password")
    private String passwordHash;

    @Column
    @Nullable
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @Column
    @NotNull
    private Boolean locked = false;

    @Column
    @NotNull
    @XmlJavaTypeAdapter(OffsetDateTimeAdapter.class)
    private OffsetDateTime created = getCurrentDate(); //особенности WS

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", role=" + role +
                ", locked=" + locked +
                ", created=" + created +
                '}';
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created == null
                ? IHasCreated.getCurrent()
                : IHasCreated.truncateMs(created).withOffsetSameInstant(ZoneOffset.UTC);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        @NotNull final UserDTO user = (UserDTO) o;
        return Objects.equals(login, user.login) && Objects.equals(passwordHash, user.passwordHash) && Objects.equals(email, user.email) && Objects.equals(firstName, user.firstName) && Objects.equals(lastName, user.lastName) && Objects.equals(middleName, user.middleName) && role == user.role && locked.equals(user.locked) && created.equals(user.created);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, passwordHash, email, firstName, lastName, middleName, role, locked, created);
    }

}
