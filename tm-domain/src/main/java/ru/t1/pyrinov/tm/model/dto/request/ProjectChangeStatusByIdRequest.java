package ru.t1.pyrinov.tm.model.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public class ProjectChangeStatusByIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    @Nullable
    private Status status;

    public ProjectChangeStatusByIdRequest(@Nullable String token) {
        super(token);
    }

}
