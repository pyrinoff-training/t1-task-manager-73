package ru.t1.pyrinov.tm.model.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class TaskGetByIdRequest extends AbstractUserRequest {

    @Nullable
    private String Id;

    public TaskGetByIdRequest(@Nullable String token) {
        super(token);
    }

}
