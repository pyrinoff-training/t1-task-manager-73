package ru.t1.pyrinov.tm.model.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class UserLoginRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    public UserLoginRequest(@Nullable String token) {
        super(token);
    }

}
