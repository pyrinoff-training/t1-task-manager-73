package ru.t1.pyrinov.tm.model.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class UserProfileRequest extends AbstractUserRequest {

    public UserProfileRequest(@Nullable String token) {
        super(token);
    }

}
