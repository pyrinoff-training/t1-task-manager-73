package ru.t1.pyrinov.tm.model.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class UserViewProfileRequest extends AbstractUserRequest {

    public UserViewProfileRequest(@Nullable String token) {
        super(token);
    }

}
