package ru.t1.pyrinov.tm.model.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.model.dto.ProjectDTO;

@NoArgsConstructor
public class ProjectChangeStatusByIdResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByIdResponse(@Nullable ProjectDTO project) {
        super(project);
    }

}
