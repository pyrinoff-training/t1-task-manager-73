package ru.t1.pyrinov.tm.model.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ServerAboutResponse extends AbstractResponse {

    private String email;

    private String name;

}
