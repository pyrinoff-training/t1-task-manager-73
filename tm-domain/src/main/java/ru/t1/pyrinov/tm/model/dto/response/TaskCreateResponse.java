package ru.t1.pyrinov.tm.model.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.model.dto.TaskDTO;

@NoArgsConstructor
public class TaskCreateResponse extends AbstractTaskResponse {

    public TaskCreateResponse(@Nullable TaskDTO task) {
        super(task);
    }

}
