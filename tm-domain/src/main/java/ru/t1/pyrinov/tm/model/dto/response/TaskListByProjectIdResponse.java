package ru.t1.pyrinov.tm.model.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.model.dto.TaskDTO;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaskListByProjectIdResponse extends AbstractResponse {

    @Nullable
    private List<TaskDTO> tasks;

    public TaskListByProjectIdResponse(@Nullable final List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

}
