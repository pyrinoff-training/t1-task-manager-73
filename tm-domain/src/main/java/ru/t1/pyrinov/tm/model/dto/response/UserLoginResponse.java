package ru.t1.pyrinov.tm.model.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class UserLoginResponse extends AbstractResultResponse {

    @Getter
    @Setter
    @Nullable
    private String token;

    public UserLoginResponse(@Nullable Throwable throwable) {
        super(throwable);
    }

    public UserLoginResponse(@Nullable String token) {
        this.token = token;
    }

}
