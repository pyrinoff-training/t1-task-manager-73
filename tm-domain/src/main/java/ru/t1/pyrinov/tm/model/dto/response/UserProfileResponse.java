package ru.t1.pyrinov.tm.model.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.model.dto.UserDTO;

@NoArgsConstructor
public final class UserProfileResponse extends AbstractUserResponse {

    public UserProfileResponse(@Nullable final UserDTO user) {
        super(user);
    }

}
