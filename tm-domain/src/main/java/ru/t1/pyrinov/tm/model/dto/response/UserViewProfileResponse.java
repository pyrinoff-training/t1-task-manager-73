package ru.t1.pyrinov.tm.model.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.model.dto.UserDTO;

@NoArgsConstructor
public class UserViewProfileResponse extends AbstractUserResponse {

    public UserViewProfileResponse(@Nullable UserDTO user) {
        super(user);
    }

}
