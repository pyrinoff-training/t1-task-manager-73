package ru.t1.pyrinov.tm.model.graph;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
//@Entity
@MappedSuperclass
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractUserOwnedGraphModel extends AbstractGraphModel {

    @ManyToOne
    @JoinColumn(name = "user_id")
    private @NotNull UserGraph user;

}
