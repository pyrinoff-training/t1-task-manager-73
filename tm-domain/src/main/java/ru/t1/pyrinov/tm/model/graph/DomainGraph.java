package ru.t1.pyrinov.tm.model.graph;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import jakarta.xml.bind.annotation.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.pyrinov.tm.model.dto.ProjectDTO;
import ru.t1.pyrinov.tm.model.dto.TaskDTO;
import ru.t1.pyrinov.tm.model.dto.UserDTO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@XmlRootElement
@NoArgsConstructor
@XmlType(name = "domain")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonXmlRootElement(localName = "domain")
public class DomainGraph implements Serializable {

    private static final long serialVersionUID = 1;

    private @NotNull String id = UUID.randomUUID().toString();

    @NotNull
    private Date created = new Date();

    @JsonProperty("user")
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    @JacksonXmlElementWrapper(localName = "users")
    private @NotNull List<UserDTO> users = new ArrayList<>();

    @JsonProperty("project")
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    @JacksonXmlElementWrapper(localName = "projects")
    private @NotNull List<ProjectDTO> projects = new ArrayList<>();

    @JsonProperty("task")
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    @JacksonXmlElementWrapper(localName = "tasks")
    private @NotNull List<TaskDTO> tasks = new ArrayList<>();

}
