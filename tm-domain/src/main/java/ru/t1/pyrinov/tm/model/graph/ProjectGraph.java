package ru.t1.pyrinov.tm.model.graph;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.t1.pyrinov.tm.api.model.IHasCreated;
import ru.t1.pyrinov.tm.api.model.IWBS;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.marshalling.OffsetDateTimeAdapter;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Entity
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "projects", schema = "public")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class ProjectGraph extends AbstractUserOwnedGraphModel implements IWBS {

    private static final long serialVersionUID = 1;

    @Column
    private @NotNull String name = "";

    @Column
    private @NotNull String description = "";

    @Column
    @Enumerated(EnumType.STRING)
    private @NotNull Status status = Status.NOT_STARTED;

    @Column
    @NotNull
    @XmlJavaTypeAdapter(OffsetDateTimeAdapter.class)
    private OffsetDateTime created = getCurrentDate();

    @JsonIgnore
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private @NotNull List<TaskGraph> tasks = new ArrayList<>();

    public ProjectGraph(final @NotNull String name, final @NotNull Status status) {
        this.name = name;
        this.status = status;
    }

    public ProjectGraph(final @NotNull String name, final @NotNull Status status, @NotNull final UserGraph user) {
        this.name = name;
        this.status = status;
        setUser(user);
    }


    @Override
    public @NotNull String toString() {
        return name + " | " + description + " | " + status.getDisplayName() + " (ID: " + getId() + ", created: "
                + created.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSSSXXXXX'Z'"))
                + ")";
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created == null
                ? IHasCreated.getCurrent()
                : IHasCreated.truncateMs(created).withOffsetSameInstant(ZoneOffset.UTC);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectGraph project = (ProjectGraph) o;
        return name.equals(project.name) && description.equals(project.description) && status == project.status && created.equals(project.created);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, status, created);
    }

}
