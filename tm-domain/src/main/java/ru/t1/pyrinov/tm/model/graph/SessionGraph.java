package ru.t1.pyrinov.tm.model.graph;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.enumerated.Role;
import ru.t1.pyrinov.tm.marshalling.OffsetDateTimeAdapter;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.Objects;

@Getter
@Setter
@Entity
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "sessions", schema = "public")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class SessionGraph extends AbstractUserOwnedGraphModel {

    private static final long serialVersionUID = 1;

    @Column
    @XmlJavaTypeAdapter(OffsetDateTimeAdapter.class)
    private @NotNull OffsetDateTime date = getCurrentDate();

    @Column
    @Enumerated(EnumType.STRING)
    private @Nullable Role role = null;

    @Override
    public String toString() {
        return "Session{" +
                "date=" + date +
                ", role=" + role +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SessionGraph session = (SessionGraph) o;
        return date.equals(session.date) && role == session.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, role);
    }

}
