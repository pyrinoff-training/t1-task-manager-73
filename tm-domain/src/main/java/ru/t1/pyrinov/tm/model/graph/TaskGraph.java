package ru.t1.pyrinov.tm.model.graph;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.api.model.IHasCreated;
import ru.t1.pyrinov.tm.api.model.IWBS;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.marshalling.OffsetDateTimeAdapter;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Objects;

@Getter
@Setter
@Entity
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "tasks", schema = "public")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class TaskGraph extends AbstractUserOwnedGraphModel implements IWBS {

    private static final long serialVersionUID = 1;

    @Column
    private @NotNull String name = "";

    @Column
    private @NotNull String description = "";

    @Column
    @Enumerated(EnumType.STRING)
    private @NotNull Status status = Status.NOT_STARTED;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "project_id")
    private @Nullable ProjectGraph project;

    @Column
    @XmlJavaTypeAdapter(OffsetDateTimeAdapter.class)
    private @NotNull OffsetDateTime created = getCurrentDate();

    public TaskGraph(final @NotNull String name, final @NotNull Status status) {
        this.name = name;
        this.status = status;
    }

    public TaskGraph(final @NotNull String name, final @NotNull Status status, @NotNull final UserGraph user) {
        this.name = name;
        this.status = status;
        setUser(user);
    }

    @Override
    public @NotNull String toString() {
        return name + "| " + description + " - " + status.getDisplayName() + " (ID: " + getId() + ", bound to project id: " + project.getId() + ", created: " + created + ")";
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created == null
                ? IHasCreated.getCurrent()
                : IHasCreated.truncateMs(created).withOffsetSameInstant(ZoneOffset.UTC);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskGraph task = (TaskGraph) o;
        return name.equals(task.name) && description.equals(task.description) && status == task.status && Objects.equals(project.getId(), task.getProject().getId()) && created.equals(task.created);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, status, project.getId(), created);
    }

}
