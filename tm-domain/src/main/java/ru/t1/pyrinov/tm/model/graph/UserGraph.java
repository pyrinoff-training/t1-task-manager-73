package ru.t1.pyrinov.tm.model.graph;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.api.model.IHasCreated;
import ru.t1.pyrinov.tm.enumerated.Role;
import ru.t1.pyrinov.tm.marshalling.OffsetDateTimeAdapter;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Entity
@Accessors(chain = true)
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "users", schema = "public")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserGraph extends AbstractGraphModel {

    private static final long serialVersionUID = 1;

    @Column
    private @Nullable String login;

    @Column(name = "password")
    private @Nullable String passwordHash;

    @Column
    private @Nullable String email;

    @Column(name = "first_name")
    private @Nullable String firstName;

    @Column(name = "last_name")
    private @Nullable String lastName;

    @Column(name = "middle_name")
    private @Nullable String middleName;

    @Column
    @Enumerated(EnumType.STRING)
    private @NotNull Role role = Role.USUAL;

    @Column
    private @NotNull Boolean locked = false;

    @Column
    @XmlJavaTypeAdapter(OffsetDateTimeAdapter.class)
    private @NotNull OffsetDateTime created = getCurrentDate(); //особенности WS

    @OneToMany(mappedBy = "user", targetEntity = TaskGraph.class, cascade = CascadeType.ALL, orphanRemoval = true)
    private @NotNull List<TaskGraph> tasks = new ArrayList<>();

    @OneToMany(mappedBy = "user", targetEntity = ProjectGraph.class, cascade = CascadeType.ALL, orphanRemoval = true)
    private @NotNull List<ProjectGraph> projects = new ArrayList<>();

    @OneToMany(mappedBy = "user", targetEntity = SessionGraph.class, cascade = CascadeType.ALL, orphanRemoval = true)
    private @NotNull List<SessionGraph> sessions = new ArrayList<>();

    @Override
    public @NotNull String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", role=" + role +
                ", locked=" + locked +
                ", created=" + created +
                '}';
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created == null
                ? IHasCreated.getCurrent()
                : IHasCreated.truncateMs(created).withOffsetSameInstant(ZoneOffset.UTC);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        @NotNull final UserGraph user = (UserGraph) o;
        return Objects.equals(login, user.login) && Objects.equals(passwordHash, user.passwordHash) && Objects.equals(email, user.email) && Objects.equals(firstName, user.firstName) && Objects.equals(lastName, user.lastName) && Objects.equals(middleName, user.middleName) && role == user.role && locked.equals(user.locked) && created.equals(user.created);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, passwordHash, email, firstName, lastName, middleName, role, locked, created);
    }

}
