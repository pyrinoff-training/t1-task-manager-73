package ru.t1.pyrinov.tm;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.pyrinov.tm.component.Bootstrap;
import ru.t1.pyrinov.tm.configuration.ApplicationConfiguration;
import ru.t1.pyrinov.tm.listener.EntityListener;
import ru.t1.pyrinov.tm.service.LoggerService;

import javax.jms.*;

public final class Application {

    @SneakyThrows
    public static void main(@Nullable String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start();
    }

}