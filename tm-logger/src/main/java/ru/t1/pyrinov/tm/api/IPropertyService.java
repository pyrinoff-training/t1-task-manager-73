package ru.t1.pyrinov.tm.api;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull String getDatabaseHost();

    @NotNull Integer getDatabasePort();

    @NotNull String getDatabaseName();

}
