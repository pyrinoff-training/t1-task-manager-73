package ru.t1.pyrinov.tm.component;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.listener.EntityListener;
import ru.t1.pyrinov.tm.service.LoggerService;

import javax.jms.*;

@Component
public class Bootstrap {

    private static @NotNull final String QUEUE = "LOGGER";

    @Autowired
    private @NotNull ConnectionFactory connectionFactory;

    @Autowired
    private @NotNull EntityListener entityListener;

    @SneakyThrows
    public void start() {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(QUEUE);
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }

}
