package ru.t1.pyrinov.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Component
public final class EntityListener implements MessageListener {

    @Autowired
    private @NotNull LoggerService loggerService;

    @Override
    @SneakyThrows
    public void onMessage(@Nullable final Message message) {
        if (!(message instanceof @Nullable final TextMessage textMessage)) return;
        @Nullable final String text = textMessage.getText();
        loggerService.log(text);
    }

}
