package ru.t1.pyrinov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.LinkedHashMap;
import java.util.Map;

@Service
public final class LoggerService {

    @Autowired
    private @NotNull PropertyService propertyService;

    private @NotNull final ObjectMapper objectMapper = new ObjectMapper();

    private @NotNull MongoClient mongoClient;

    private @NotNull MongoDatabase mongoDatabase;

    @PostConstruct
    public void postConstruct() {
        mongoClient = new MongoClient(propertyService.getDatabaseHost(), propertyService.getDatabasePort());
        mongoDatabase = mongoClient.getDatabase(propertyService.getDatabaseName());
    }

    @SneakyThrows
    public void log(@Nullable final String text) {
        if (text == null) return;
        @NotNull final Map<String, Object> event = objectMapper.readValue(text, LinkedHashMap.class);
        @NotNull final String table = event.get("table").toString();
        if (mongoDatabase.getCollection(table) == null) mongoDatabase.createCollection(table);
        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(table);
        collection.insertOne(new Document(event));
    }

}
