package ru.t1.pyrinov.tm.service;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.pyrinov.tm.api.IPropertyService;

import java.util.Properties;
import java.util.regex.Pattern;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @Value("#{environment['database.host']}")
    private @NotNull String databaseHost;


    @Value("#{environment['database.port']}")
    private  @NotNull Integer databasePort;


    @Value("#{environment['database.name']}")
    private  @NotNull String databaseName;

}
