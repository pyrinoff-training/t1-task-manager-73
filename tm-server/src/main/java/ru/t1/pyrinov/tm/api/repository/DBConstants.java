package ru.t1.pyrinov.tm.api.repository;

import org.jetbrains.annotations.NotNull;

public interface DBConstants {

    @NotNull
    String TABLE_PROJECT = "projects";

    @NotNull
    String TABLE_TASK = "tasks";

    @NotNull
    String TABLE_USER = "users";

    @NotNull
    String TABLE_SESSION = "sessions";

    @NotNull
    String COLUMN_ID = "id";

    @NotNull
    String COLUMN_NAME = "name";

    @NotNull
    String COLUMN_CREATED = "created";

    @NotNull
    String COLUMN_DESCRIPTION = "description";

    @NotNull
    String COLUMN_USER_ID = "user_id";

    @NotNull
    String COLUMN_STATUS = "status";

    @NotNull
    String COLUMN_ROLE = "role";

    @NotNull
    String COLUMN_PROJECT_ID = "project_id";

    @NotNull
    String COLUMN_LOGIN = "login";

    @NotNull
    String COLUMN_PASSWORD = "password";

    @NotNull
    String COLUMN_EMAIL = "email";

    @NotNull
    String COLUMN_LOCKED = "locked";

    @NotNull
    String COLUMN_FIRST_NAME = "first_name";

    @NotNull
    String COLUMN_LAST_NAME = "last_name";

    @NotNull
    String COLUMN_MIDDLE_NAME = "middle_name";

    @NotNull
    String COLUMN_DATE = "date";

}
