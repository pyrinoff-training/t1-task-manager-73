package ru.t1.pyrinov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.model.dto.SessionDTO;
import ru.t1.pyrinov.tm.model.dto.UserDTO;

public interface IAuthService {

    @NotNull
    @SneakyThrows
    String login(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    SessionDTO validateToken(@Nullable String token);

    void invalidate(@Nullable final SessionDTO session);

}
