package ru.t1.pyrinov.tm.api.service;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    //@NotNull EntityManager getEntityManager();

    void close();

    @NotNull Liquibase getLiquibase();

    @NotNull EntityManagerFactory getEntityManagerFactory();

}
