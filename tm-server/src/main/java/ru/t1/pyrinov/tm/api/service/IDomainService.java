package ru.t1.pyrinov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.model.graph.DomainGraph;

public interface IDomainService {

    @NotNull DomainGraph getDomain();

    void setDomain(@Nullable DomainGraph domain);

    void loadDataBackup();

    void loadDataBase64();

    void loadDataBinary();

    void loadDataJsonFasterxml();

    void loadDataXmlFasterxml();

    void loadDataXmlJaxb();

    void loadDataYamlFasterxml();

    void saveDataBackup();

    void saveDataBase64();

    void saveDataBinary();

    void saveDataJsonFasterxml();

    void saveDataXmlFasterxml();

    void saveDataXmlJaxb();

    void saveDataYamlFasterxml();

}
