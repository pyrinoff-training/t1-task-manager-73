package ru.t1.pyrinov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.pyrinov.tm.api.service.dto.IProjectDTOService;
import ru.t1.pyrinov.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.pyrinov.tm.api.service.dto.ITaskDTOService;
import ru.t1.pyrinov.tm.api.service.dto.IUserDTOService;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectDTOService getProjectService();

    @NotNull
    IProjectTaskDTOService getProjectTaskService();

    @NotNull
    ITaskDTOService getTaskService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IUserDTOService getUserService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IAdminService getAdminService();

}
