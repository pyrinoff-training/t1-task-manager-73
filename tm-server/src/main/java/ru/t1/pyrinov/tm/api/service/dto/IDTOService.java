package ru.t1.pyrinov.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.enumerated.SortOrder;
import ru.t1.pyrinov.tm.model.dto.AbstractDTOModel;

import javax.swing.*;
import java.util.List;

public interface IDTOService<M extends AbstractDTOModel> {

    @Nullable
    List<M> findAll(@Nullable SortOrder sort) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    M findById(@Nullable String id) throws Exception;

    void remove(@Nullable M model) throws Exception;

    M add(@Nullable M model) throws Exception;

    boolean existsById(@Nullable final String id);

}
