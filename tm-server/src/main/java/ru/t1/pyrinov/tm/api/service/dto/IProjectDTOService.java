package ru.t1.pyrinov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.model.dto.ProjectDTO;

public interface IProjectDTOService extends IUserOwnedDTOService<ProjectDTO> {

    @NotNull
    ProjectDTO create(String userId, String name, String description);

    @Nullable
    ProjectDTO create(String userId, String name);

    @Nullable
    ProjectDTO updateById(String userId, String id, String name, String description);

    @Nullable
    ProjectDTO changeProjectStatusById(String userId, String id, Status status);

    ProjectDTO findById(@Nullable String userId, @Nullable String id);

}
