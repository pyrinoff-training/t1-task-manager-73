package ru.t1.pyrinov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.model.dto.SessionDTO;

public interface ISessionDTOService extends IUserOwnedDTOService<SessionDTO> {

    void removeByUserId(@Nullable final String userId);

    SessionDTO findById(@NotNull String userId);

}
