package ru.t1.pyrinov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.enumerated.SortOrder;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.model.dto.TaskDTO;

import java.util.List;

public interface ITaskDTOService extends IUserOwnedDTOService<TaskDTO> {

    @NotNull
    TaskDTO create(String userId, String name, String description);

    @Nullable
    TaskDTO create(String userId, String name);

    @Nullable
    TaskDTO updateById(String userId, String id, String name, String description);

    @Nullable
    TaskDTO changeTaskStatusById(String userId, String id, Status status);


    @Nullable
    List<TaskDTO> findAllByProjectId(String userId, String taskId);

    void updateProjectIdById(@Nullable final String userId, @Nullable final String id, @Nullable final String taskId);

    @NotNull List<TaskDTO> findAll(@Nullable String userId, SortOrder sort);

    TaskDTO findById(@Nullable String userId, @Nullable String id);

}
