package ru.t1.pyrinov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.enumerated.Role;
import ru.t1.pyrinov.tm.model.dto.UserDTO;

public interface IUserDTOService extends IDTOService<UserDTO> {

    @NotNull
    UserDTO create(@Nullable final String login, @Nullable final String password) throws Exception;

    @NotNull
    UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception;

    @NotNull
    UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception;

    @NotNull
    UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) throws Exception;

    @Nullable
    UserDTO findByLogin(@Nullable final String login) throws Exception;

    @Nullable
    UserDTO findByEmail(@Nullable final String email) throws Exception;

    Boolean isLoginExist(@Nullable final String login) throws Exception;

    Boolean isEmailExist(@Nullable final String email) throws Exception;

    void lockUserByLogin(@Nullable final String login) throws Exception;

    void removeByLogin(@Nullable final String login) throws Exception;

    void setPassword(@Nullable final String id, @Nullable final String password) throws Exception;

    void unlockUserByLogin(@Nullable final String login) throws Exception;

    void updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws Exception;

    void clear(@NotNull String userId);

}
