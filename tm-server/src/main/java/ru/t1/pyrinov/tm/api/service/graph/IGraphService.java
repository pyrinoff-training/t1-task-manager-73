package ru.t1.pyrinov.tm.api.service.graph;

import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.enumerated.SortOrder;
import ru.t1.pyrinov.tm.model.graph.AbstractGraphModel;

import java.util.List;

public interface IGraphService<M extends AbstractGraphModel> {

    @Nullable
    List<M> findAll(@Nullable SortOrder sort) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    M findById(@Nullable String id) throws Exception;

    void remove(@Nullable M model) throws Exception;

    M add(@Nullable M model) throws Exception;

    boolean existsById(@Nullable final String id);

}
