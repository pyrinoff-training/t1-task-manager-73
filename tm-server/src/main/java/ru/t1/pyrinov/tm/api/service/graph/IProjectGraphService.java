package ru.t1.pyrinov.tm.api.service.graph;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.model.graph.ProjectGraph;

public interface IProjectGraphService extends IUserOwnedGraphService<ProjectGraph> {

    @NotNull
    ProjectGraph create(String userId, String name, String description);

    @Nullable
    ProjectGraph create(String userId, String name);

    @Nullable
    ProjectGraph updateById(String userId, String id, String name, String description);

    @Nullable
    ProjectGraph changeProjectStatusById(String userId, String id, Status status);

    ProjectGraph findById(@Nullable String userId, @Nullable String id);

}
