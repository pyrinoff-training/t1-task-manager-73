package ru.t1.pyrinov.tm.api.service.graph;

public interface IProjectTaskGraphService {

    void bindTaskToProject(String userId, String projectId, String taskId);

    void unbindTaskFromProject(String userId, String projectId, String taskId);

    void removeProjectById(String userId, String projectId);

}
