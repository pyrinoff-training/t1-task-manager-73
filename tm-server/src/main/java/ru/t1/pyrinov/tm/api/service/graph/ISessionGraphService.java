package ru.t1.pyrinov.tm.api.service.graph;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.model.graph.SessionGraph;

public interface ISessionGraphService extends IUserOwnedGraphService<SessionGraph> {

    void removeByUserId(@Nullable final String userId);

    SessionGraph findById(@NotNull String userId);

}
