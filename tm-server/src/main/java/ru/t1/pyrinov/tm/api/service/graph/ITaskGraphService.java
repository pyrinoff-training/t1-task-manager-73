package ru.t1.pyrinov.tm.api.service.graph;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.enumerated.SortOrder;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.model.graph.TaskGraph;

import java.util.List;

public interface ITaskGraphService extends IUserOwnedGraphService<TaskGraph> {

    @NotNull
    TaskGraph create(String userId, String name, String description);

    @Nullable
    TaskGraph create(String userId, String name);

    @Nullable
    TaskGraph updateById(String userId, String id, String name, String description);

    @Nullable
    TaskGraph changeTaskStatusById(String userId, String id, Status status);


    @Nullable
    List<TaskGraph> findAllByProjectId(String userId, String taskId);

    void updateProjectIdById(@Nullable final String userId, @Nullable final String id, @Nullable final String taskId);

    @NotNull List<TaskGraph> findAll(@Nullable String userId, SortOrder sort);

    TaskGraph findById(@Nullable String userId, @Nullable String id);

}
