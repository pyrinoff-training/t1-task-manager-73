package ru.t1.pyrinov.tm.api.service.graph;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.enumerated.Role;
import ru.t1.pyrinov.tm.model.graph.UserGraph;

public interface IUserGraphService extends IGraphService<UserGraph> {

    @NotNull
    UserGraph create(@Nullable final String login, @Nullable final String password) throws Exception;

    @NotNull
    UserGraph create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception;

    @NotNull
    UserGraph create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception;

    @NotNull
    UserGraph create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) throws Exception;

    @Nullable
    UserGraph findByLogin(@Nullable final String login) throws Exception;

    @Nullable
    UserGraph findByEmail(@Nullable final String email) throws Exception;

    Boolean isLoginExist(@Nullable final String login) throws Exception;

    Boolean isEmailExist(@Nullable final String email) throws Exception;

    void lockUserByLogin(@Nullable final String login) throws Exception;

    void removeByLogin(@Nullable final String login) throws Exception;

    void setPassword(@Nullable final String id, @Nullable final String password) throws Exception;

    void unlockUserByLogin(@Nullable final String login) throws Exception;

    void updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws Exception;

    void clear(@NotNull String userId);

}
