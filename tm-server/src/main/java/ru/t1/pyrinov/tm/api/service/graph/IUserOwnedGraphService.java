package ru.t1.pyrinov.tm.api.service.graph;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.enumerated.SortOrder;
import ru.t1.pyrinov.tm.model.graph.AbstractUserOwnedGraphModel;

import java.util.List;

public interface IUserOwnedGraphService<M extends AbstractUserOwnedGraphModel> {

    @Nullable
    List<M> findAll(@Nullable String userId) throws Exception;

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable SortOrder sort) throws Exception;

    void removeById(@Nullable String userId, @Nullable String id) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String projectId);

    M add(@Nullable final String userId, @NotNull final M model);

    M findById(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

}
