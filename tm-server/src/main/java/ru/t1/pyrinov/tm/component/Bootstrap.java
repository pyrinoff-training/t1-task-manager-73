package ru.t1.pyrinov.tm.component;

import jakarta.xml.ws.Endpoint;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.api.endpoint.*;
import ru.t1.pyrinov.tm.api.service.*;
import ru.t1.pyrinov.tm.api.service.dto.*;
import ru.t1.pyrinov.tm.endpoint.AbstractEndpoint;
import ru.t1.pyrinov.tm.enumerated.Role;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.model.dto.ProjectDTO;
import ru.t1.pyrinov.tm.model.dto.TaskDTO;
import ru.t1.pyrinov.tm.model.dto.UserDTO;
import ru.t1.pyrinov.tm.util.SystemUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Component
public final class Bootstrap implements IServiceLocator {

    @Getter
    @Autowired
    private @NotNull IPropertyService propertyService;

    @Getter
    @Autowired
    private @NotNull IConnectionService connectionService;

    @Getter
    @Autowired
    private @NotNull IProjectDTOService projectService;

    @Getter
    @Autowired
    private @NotNull ITaskDTOService taskService;

    @Getter
    @Autowired
    private @NotNull IProjectTaskDTOService projectTaskService;

    @Getter
    @Autowired
    private @NotNull IUserDTOService userService;

    @Getter
    @Autowired
    private @NotNull ISessionDTOService sessionService;

    @Getter
    @Autowired
    private @NotNull IAuthService authService;

    @Getter
    @Autowired
    private @NotNull ILoggerService loggerService;

    @Getter
    @Autowired
    private @NotNull IAdminService adminService;

    @Getter
    @Autowired
    private @NotNull IAuthEndpoint authEndpoint;

    @Autowired
    private @NotNull ISystemEndpoint systemEndpoint;

    @Autowired
    private @NotNull IUserEndpoint userEndpoint;

    @Autowired
    private @NotNull IProjectEndpoint projectEndpoint;

    @Autowired
    private @NotNull ITaskEndpoint taskEndpoint;

    @Autowired
    private @NotNull IAdminEndpoint adminEndpoint;

    @Autowired
    private @NotNull AbstractEndpoint[] endpoints;

    @SneakyThrows
    public void start() {
        initPID();
        initDemoData();
        initShutdownHook();
        registryAllEndpoints(endpoints);
    }

    private void registryAllEndpoints(@NotNull final AbstractEndpoint[] endpoints) {
        for (@NotNull AbstractEndpoint endpoint : endpoints) {
            registry(endpoint);
        }
    }

    private void initDemoData() {
        try {
            if (!userService.isLoginExist("test")) {
                @Nullable final UserDTO userTest = userService.create("test", "test", "test@test.ru");
                projectService.add(userTest.getId(), new ProjectDTO("TEST PROJECT ONE", Status.IN_PROGRESS, userTest.getId()));
                projectService.add(userTest.getId(), new ProjectDTO("TEST PROJECT TWO", Status.NOT_STARTED, userTest.getId()));
                projectService.add(userTest.getId(), new ProjectDTO("TEST PROJECT THREE", Status.IN_PROGRESS, userTest.getId()));
                projectService.add(userTest.getId(), new ProjectDTO("TEST PROJECT FOUR", Status.COMPLETED, userTest.getId()));

                taskService.add(userTest.getId(), new TaskDTO("TEST TASK ONE", Status.IN_PROGRESS, userTest.getId()));
                taskService.add(userTest.getId(), new TaskDTO("TEST TASK TWO", Status.COMPLETED, userTest.getId()));
                taskService.add(userTest.getId(), new TaskDTO("TEST TASK THREE", Status.NOT_STARTED, userTest.getId()));
                taskService.add(userTest.getId(), new TaskDTO("TEST TASK FOUR", Status.IN_PROGRESS, userTest.getId()));
            }
            if (!userService.isLoginExist(propertyService.getAdminLogin())) {
                @Nullable final UserDTO admin = userService.create(propertyService.getAdminLogin(), propertyService.getAdminPassword(), "admin@admin", Role.ADMIN);
            }
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            System.out.println("[FAIL TO INITIALIZE TEST DATA!] Exc: " + e);
        }
    }

    private void initShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            loggerService.info("** TASK MANAGER IS SHUTTING DOWN! **");
        }));
    }

    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(filename), pid.getBytes());
        } catch (@NotNull final IOException e) {
            e.printStackTrace();
        }
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

}
