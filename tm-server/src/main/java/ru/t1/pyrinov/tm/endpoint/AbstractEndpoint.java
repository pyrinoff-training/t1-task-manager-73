package ru.t1.pyrinov.tm.endpoint;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.pyrinov.tm.api.service.IServiceLocator;
import ru.t1.pyrinov.tm.enumerated.Role;
import ru.t1.pyrinov.tm.exception.user.AccessDeniedException;
import ru.t1.pyrinov.tm.model.dto.SessionDTO;
import ru.t1.pyrinov.tm.model.dto.request.AbstractUserRequest;

@Getter
@Setter
public abstract class AbstractEndpoint {

    @Autowired
    protected @NotNull IServiceLocator serviceLocator;

    @NotNull
    @SneakyThrows
    protected SessionDTO check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        @NotNull final SessionDTO session = getServiceLocator().getAuthService().validateToken(token);
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return getServiceLocator().getAuthService().validateToken(token);
    }

}
