package ru.t1.pyrinov.tm.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.pyrinov.tm.api.endpoint.IAdminEndpoint;
import ru.t1.pyrinov.tm.api.service.IAdminService;
import ru.t1.pyrinov.tm.exception.endpoint.EndpointException;
import ru.t1.pyrinov.tm.model.dto.request.SchemeDropRequest;
import ru.t1.pyrinov.tm.model.dto.request.SchemeInitRequest;
import ru.t1.pyrinov.tm.model.dto.response.SchemeDropResponse;
import ru.t1.pyrinov.tm.model.dto.response.SchemeInitResponse;

@Controller
@WebService(endpointInterface = "ru.t1.pyrinov.tm.api.endpoint.IAdminEndpoint")
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    @Autowired
    private @NotNull IAdminService adminService;

    @WebMethod
    @Override
    @SneakyThrows
    public SchemeInitResponse initScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @Nullable final SchemeInitRequest request
    ) {
        try {
            adminService.initScheme(request.getInitToken());
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new SchemeInitResponse();
    }

    @WebMethod
    @Override
    @SneakyThrows
    public SchemeDropResponse dropScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @Nullable final SchemeDropRequest request
    ) {
        try {
            adminService.dropScheme(request.getInitToken());
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new SchemeDropResponse();
    }

}
