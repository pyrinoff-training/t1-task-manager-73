package ru.t1.pyrinov.tm.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.pyrinov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.pyrinov.tm.api.service.IAuthService;
import ru.t1.pyrinov.tm.model.dto.SessionDTO;
import ru.t1.pyrinov.tm.model.dto.UserDTO;
import ru.t1.pyrinov.tm.model.dto.request.UserLoginRequest;
import ru.t1.pyrinov.tm.model.dto.request.UserLogoutRequest;
import ru.t1.pyrinov.tm.model.dto.request.UserProfileRequest;
import ru.t1.pyrinov.tm.model.dto.response.UserLoginResponse;
import ru.t1.pyrinov.tm.model.dto.response.UserLogoutResponse;
import ru.t1.pyrinov.tm.model.dto.response.UserProfileResponse;

@Controller
@WebService(endpointInterface = "ru.t1.pyrinov.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    @Autowired
    private @NotNull IAuthService authService;

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        @Nullable final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        authService.invalidate(session);
        System.out.println("invalidating...");
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserProfileRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        UserDTO userDTO = getServiceLocator().getUserService().findById(userId);
        return new UserProfileResponse(userDTO);
    }

}
