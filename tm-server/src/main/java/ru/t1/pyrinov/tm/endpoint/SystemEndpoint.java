package ru.t1.pyrinov.tm.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.pyrinov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.pyrinov.tm.api.service.IPropertyService;
import ru.t1.pyrinov.tm.model.dto.request.ServerAboutRequest;
import ru.t1.pyrinov.tm.model.dto.request.ServerVersionRequest;
import ru.t1.pyrinov.tm.model.dto.response.ServerAboutResponse;
import ru.t1.pyrinov.tm.model.dto.response.ServerVersionResponse;

@Controller
@WebService(endpointInterface = "ru.t1.pyrinov.tm.api.endpoint.ISystemEndpoint")
public class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    @Autowired
    private @NotNull IPropertyService propertyService;

    @WebMethod
    @Override
    @NotNull
    public ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull final ServerAboutRequest request
    ) {
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @WebMethod
    @Override
    @NotNull
    public ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull ServerVersionRequest request
    ) {
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
