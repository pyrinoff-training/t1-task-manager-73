package ru.t1.pyrinov.tm.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.pyrinov.tm.api.endpoint.IUserEndpoint;
import ru.t1.pyrinov.tm.api.service.IAuthService;
import ru.t1.pyrinov.tm.api.service.dto.IUserDTOService;
import ru.t1.pyrinov.tm.enumerated.Role;
import ru.t1.pyrinov.tm.model.dto.SessionDTO;
import ru.t1.pyrinov.tm.model.dto.UserDTO;
import ru.t1.pyrinov.tm.model.dto.request.*;
import ru.t1.pyrinov.tm.model.dto.response.*;

@Controller
@WebService(endpointInterface = "ru.t1.pyrinov.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @Autowired
    private @NotNull IUserDTOService userDTOService;

    @SneakyThrows
    @WebMethod
    @Override
    @NotNull
    public UserChangePasswordResponse changeUserPassword(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserChangePasswordRequest request
    ) {
        check(request);
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String password = request.getPassword();
        userDTOService.setPassword(userId, password);
        return new UserChangePasswordResponse();
    }

    @SneakyThrows
    @WebMethod
    @Override
    @NotNull
    public UserLockResponse lockUser(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserLockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        userDTOService.lockUserByLogin(login);
        return new UserLockResponse();
    }

    @WebMethod
    @Override
    @NotNull
    public UserRegistryResponse registryUser(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserRegistryRequest request
    ) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @Nullable final UserDTO user = authService.registry(login, password, email);
        return new UserRegistryResponse(user);
    }

    @SneakyThrows
    @WebMethod
    @Override
    @NotNull
    public UserRemoveResponse removeUser(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserRemoveRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        userDTOService.removeByLogin(login);
        return new UserRemoveResponse();
    }

    @SneakyThrows
    @WebMethod
    @Override
    @NotNull
    public UserUnlockResponse unlockUser(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserUnlockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        userDTOService.unlockUserByLogin(login);
        return new UserUnlockResponse();
    }

    @SneakyThrows
    @WebMethod
    @Override
    @NotNull
    public UserUpdateProfileResponse updateUserProfile(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserUpdateProfileRequest request
    ) {
        check(request);
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        userDTOService.updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateProfileResponse();
    }

}
