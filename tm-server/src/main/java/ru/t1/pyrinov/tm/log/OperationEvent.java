package ru.t1.pyrinov.tm.log;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class OperationEvent {

    private @NotNull OperationType type;

    private @Nullable Object entity;

    private @Nullable String table;

    private long timestamp = System.currentTimeMillis();

    public OperationEvent(@NotNull final OperationType type, @Nullable final Object entity) {
        this.type = type;
        this.entity = entity;
    }

}
