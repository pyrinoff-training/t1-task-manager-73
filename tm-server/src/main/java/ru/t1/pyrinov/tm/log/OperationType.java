package ru.t1.pyrinov.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}
