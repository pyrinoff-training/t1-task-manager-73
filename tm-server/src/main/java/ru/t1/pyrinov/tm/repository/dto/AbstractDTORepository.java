package ru.t1.pyrinov.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.pyrinov.tm.model.dto.AbstractDTOModel;

public interface AbstractDTORepository<M extends AbstractDTOModel> extends JpaRepository<M, String> {


}
