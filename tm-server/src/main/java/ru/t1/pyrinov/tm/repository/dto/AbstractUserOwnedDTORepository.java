package ru.t1.pyrinov.tm.repository.dto;

import org.springframework.stereotype.Repository;
import ru.t1.pyrinov.tm.model.dto.AbstractUserOwnedDTOModel;


@Repository
public interface AbstractUserOwnedDTORepository<M extends AbstractUserOwnedDTOModel> extends AbstractDTORepository<M> {


}