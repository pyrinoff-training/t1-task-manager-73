package ru.t1.pyrinov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.pyrinov.tm.model.dto.TaskDTO;

import java.util.List;
import java.util.Optional;

@Repository
//@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public interface TaskDTORepository extends AbstractUserOwnedDTORepository<TaskDTO> {

    @Nullable List<TaskDTO> findByUserIdAndProjectId(
            @Param("userId")
            @NotNull final String userId,
            @Param("projectId")
            @NotNull final String projectId
    );

    @Query("select count(*)>0 FROM TaskDTO WHERE userId=:userId AND id=:id")
    boolean existsByUserIdAndId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    long countByUserId(@Param("userId") @NotNull final String userId);

    long deleteByUserId(@Param("userId") @NotNull final String userId);

    long deleteByUserIdAndId(@Param("userId") @NotNull final String userId, @Param("id") @NotNull final String id);

    @NotNull List<TaskDTO> findByUserId(@Param("userId") @NotNull final String userId);

    @NotNull List<TaskDTO> findByUserId(@Param("userId") @NotNull final String userId, @NotNull final Sort sort);

    @NotNull Optional<TaskDTO> findByUserIdAndId(@Param("userId") @NotNull final String userId, @Param("id") @NotNull final String id);

}
