package ru.t1.pyrinov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.pyrinov.tm.model.dto.UserDTO;

@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public interface UserDTORepository extends AbstractDTORepository<UserDTO> {

    @Nullable UserDTO findByLogin(@Param("login") @NotNull final String login);

    @Nullable UserDTO findByEmail(@Param("email") @NotNull final String email);

    @Query("select count(*)>0 FROM UserDTO WHERE login=:login")
    Boolean isLoginExists(@Param("login") @NotNull final String login);

    @Query("select count(*)>0 FROM UserDTO WHERE email=:email")
    Boolean isEmailExists(@Param("email") @NotNull final String email);

}
