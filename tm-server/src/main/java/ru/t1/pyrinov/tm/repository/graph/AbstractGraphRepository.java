package ru.t1.pyrinov.tm.repository.graph;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.pyrinov.tm.model.graph.AbstractGraphModel;

public interface AbstractGraphRepository<M extends AbstractGraphModel> extends JpaRepository<M, String> {

}
