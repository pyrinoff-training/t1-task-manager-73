package ru.t1.pyrinov.tm.repository.graph;

import ru.t1.pyrinov.tm.model.graph.AbstractUserOwnedGraphModel;

public interface AbstractUserOwnedGraphRepository<M extends AbstractUserOwnedGraphModel> extends AbstractGraphRepository<M> {


}
