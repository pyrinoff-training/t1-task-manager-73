package ru.t1.pyrinov.tm.repository.graph;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.pyrinov.tm.model.graph.SessionGraph;

import java.util.List;
import java.util.Optional;

@Repository
//@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public interface SessionGraphRepository extends AbstractUserOwnedGraphRepository<SessionGraph> {

    @Query("select count(*)>0 FROM SessionGraph WHERE userId=:userId and id=:id")
        //@Query("select case when count(m)> 0 then true else false end FROM SessionGraph m WHERE userId=:userId and id=:id")
    boolean existsByUserIdAndId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    long countByUserId(@Param("userId") @NotNull final String userId);

    long deleteByUserId(@Param("userId") @NotNull final String userId);

    long deleteByUserIdAndId(@Param("userId") @NotNull final String userId, @Param("id") @NotNull final String id);

    @NotNull List<SessionGraph> findByUserId(@Param("userId") @NotNull final String userId);

    @NotNull List<SessionGraph> findByUserId(@Param("userId") @NotNull final String userId, @NotNull final Sort sort);

    @NotNull Optional<SessionGraph> findByUserIdAndId(@Param("userId") @NotNull final String userId, @Param("id") @NotNull final String id);

}