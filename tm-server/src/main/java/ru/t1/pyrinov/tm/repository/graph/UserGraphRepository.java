package ru.t1.pyrinov.tm.repository.graph;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.pyrinov.tm.model.graph.UserGraph;

@Repository
//@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public interface UserGraphRepository extends AbstractGraphRepository<UserGraph> {

    @Nullable UserGraph findByLogin(@Param("login") @NotNull final String login);

    @Nullable UserGraph findByEmail(@Param("email") @NotNull final String email);

    @Query("select count(*)>0 FROM UserGraph WHERE login=:login")
    Boolean isLoginExists(@Param("login") @NotNull final String login);

    @Query("select count(*)>0 FROM UserGraph WHERE email=:email")
    Boolean isEmailExists(@Param("email") @NotNull final String email);

}
