package ru.t1.pyrinov.tm.service;

import liquibase.Liquibase;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.pyrinov.tm.api.service.IAdminService;
import ru.t1.pyrinov.tm.api.service.IConnectionService;
import ru.t1.pyrinov.tm.api.service.IPropertyService;
import ru.t1.pyrinov.tm.exception.user.AccessDeniedException;

@Service
public class AdminService implements IAdminService {

    @Autowired
    private @NotNull IConnectionService connectionService;

    @Autowired
    private @NotNull IPropertyService propertyService;

    @Override
    @SneakyThrows
    public void dropScheme(@Nullable String initToken) {
        @NotNull final String token = propertyService.getDatabaseAdminProtectionToken();
        if (initToken == null || !initToken.equals(token)) throw new AccessDeniedException();
        @NotNull final Liquibase liquibase = connectionService.getLiquibase();
        liquibase.dropAll();
    }

    @Override
    @SneakyThrows
    public void initScheme(@Nullable String initToken) {
        @NotNull final String token = propertyService.getDatabaseAdminProtectionToken();
        if (initToken == null || !initToken.equals(token)) throw new AccessDeniedException();
        @NotNull final Liquibase liquibase = connectionService.getLiquibase();
        liquibase.update("scheme");
    }

}
