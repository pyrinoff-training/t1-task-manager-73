package ru.t1.pyrinov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.pyrinov.tm.api.model.IHasCreated;
import ru.t1.pyrinov.tm.api.service.IAuthService;
import ru.t1.pyrinov.tm.api.service.IPropertyService;
import ru.t1.pyrinov.tm.api.service.dto.ISessionDTOService;
import ru.t1.pyrinov.tm.api.service.dto.IUserDTOService;
import ru.t1.pyrinov.tm.enumerated.Role;
import ru.t1.pyrinov.tm.exception.field.LoginEmptyException;
import ru.t1.pyrinov.tm.exception.field.PasswordEmptyException;
import ru.t1.pyrinov.tm.exception.user.AccessDeniedException;
import ru.t1.pyrinov.tm.exception.user.AuthenticationException;
import ru.t1.pyrinov.tm.model.dto.SessionDTO;
import ru.t1.pyrinov.tm.model.dto.UserDTO;
import ru.t1.pyrinov.tm.util.CryptUtil;
import ru.t1.pyrinov.tm.util.HashUtil;

import java.time.OffsetDateTime;

@Service
public class AuthService implements IAuthService {

    @Autowired
    private @NotNull IUserDTOService userService;

    @Autowired
    private @NotNull IPropertyService propertyService;

    @Autowired
    private @NotNull ISessionDTOService sessionService;

    @SneakyThrows
    private @NotNull SessionDTO createSession(@NotNull final UserDTO user) {
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return sessionService.add(user.getId(), session);
    }

    @SneakyThrows
    private @NotNull String getToken(@NotNull final UserDTO user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final SessionDTO session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @Override
    @SneakyThrows
    public @NotNull String login(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = userService.findByLogin(login);
        if (user == null) throw new AuthenticationException();
        if (user.getLocked()) throw new AuthenticationException();
        @Nullable final String hash = HashUtil.md5WithSalt(propertyService, password);
        if (hash == null) throw new AuthenticationException();
        if (!hash.equals(user.getPasswordHash())) throw new AuthenticationException();
        return getToken(user);
    }

    @SneakyThrows
    public @NotNull UserDTO registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email, Role.USUAL);
    }

    @Override
    @SneakyThrows
    public @NotNull SessionDTO validateToken(@Nullable final String token) {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull final String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        @NotNull final SessionDTO session = objectMapper.readValue(json, SessionDTO.class);
        @NotNull final OffsetDateTime currentDate = IHasCreated.getCurrent();
        @NotNull final OffsetDateTime sessionDate = session.getDate();
        final long delta = (IHasCreated.toLongUTC(currentDate) - IHasCreated.toLongUTC(sessionDate)) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        if (!sessionService.existsById(session.getUserId(), session.getId())) throw new AccessDeniedException();
        return session;

    }

    @Override
    @SneakyThrows
    public void invalidate(@Nullable final SessionDTO session) {
        if (session == null) return;
        sessionService.removeById(session.getUserId(), session.getId());
    }

}
