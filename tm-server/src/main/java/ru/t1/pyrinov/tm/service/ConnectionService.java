package ru.t1.pyrinov.tm.service;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.pyrinov.tm.api.service.IConnectionService;
import ru.t1.pyrinov.tm.api.service.IDatabaseDataProvider;

import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

@Service
public class ConnectionService implements IConnectionService {

    @Autowired
    private @NotNull IDatabaseDataProvider databaseDataProvider;

    @Autowired
    @Getter
    private @NotNull EntityManagerFactory entityManagerFactory;

    private static final ClassLoaderResourceAccessor ACCESSOR = new ClassLoaderResourceAccessor();

    private static Database DATABASE;

    @Override public void close() {
        entityManagerFactory.close();
    }

    private @NotNull Connection getConnection(Properties properties) throws SQLException {
        return DriverManager.getConnection(
                properties.getProperty("url"),
                properties.getProperty("username"),
                properties.getProperty("password")
        );
    }

    @Override
    @SneakyThrows
    public @NotNull Liquibase getLiquibase() {
        initLiquibase();
        return new Liquibase(databaseDataProvider.getLiquibaseChangelogFile(true), ACCESSOR, DATABASE);
    }

    private void initLiquibase() throws IOException, SQLException, DatabaseException {
        final Properties properties = new Properties();
        final InputStream inputStream = ClassLoader.getSystemResourceAsStream("liquibase.properties");
        properties.load(inputStream);
        if (!properties.contains("outputChangeLogFile"))
            properties.setProperty("outputChangeLogFile", databaseDataProvider.getLiquibaseChangelogFile(false));
        if (!properties.contains("url")) properties.setProperty("url", databaseDataProvider.getDatabaseUrl());
        if (!properties.contains("username"))
            properties.setProperty("username", databaseDataProvider.getDatabaseUsername());
        if (!properties.contains("password"))
            properties.setProperty("password", databaseDataProvider.getDatabasePassword());
        final Connection connection = getConnection(properties);
        final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        DATABASE = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
    }

}
