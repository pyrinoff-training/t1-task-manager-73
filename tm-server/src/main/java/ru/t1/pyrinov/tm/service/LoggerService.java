package ru.t1.pyrinov.tm.service;

import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.pyrinov.tm.api.service.IConnectionService;
import ru.t1.pyrinov.tm.api.service.ILoggerService;
import ru.t1.pyrinov.tm.listener.EntityListener;
import ru.t1.pyrinov.tm.listener.JmsLoggerProducer;

import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.*;

@Service
public class LoggerService implements ILoggerService {

    private @NotNull static final String CONFIG_FILE = "/logger.properties";

    private @NotNull static final String COMMANDS = "COMMANDS";
    private @NotNull static final String ERRORS = "ERRORS";
    private @NotNull static final String MESSAGES = "MESSAGES";

    private @NotNull static final String COMMANDS_FILE = "./commands.xml";
    private @NotNull static final String ERRORS_FILE = "./errors.xml";
    private @NotNull static final String MESSAGES_FILE = "./messages.xml";

    private @NotNull static final LogManager MANAGER = LogManager.getLogManager();
    private @NotNull static final ConsoleHandler CONSOLE_HANDLER = getConsoleHandler();

    private @NotNull static final Logger LOGGER_ROOT = Logger.getLogger("");
    private @NotNull static final Logger LOGGER_COMMAND = Logger.getLogger(COMMANDS);
    private @NotNull static final Logger LOGGER_ERROR = Logger.getLogger(ERRORS);
    private @NotNull static final Logger LOGGER_MESSAGES = Logger.getLogger(MESSAGES);

    @Autowired
    private @NotNull IConnectionService connectionService;

    private @NotNull final ConsoleHandler consoleHandler = getConsoleHandler();

    public @NotNull static Logger getLoggerCommand() {
        return Logger.getLogger(COMMANDS);
    }

    public @NotNull static Logger getLoggerError() {
        return LOGGER_ERROR;
    }

    public @NotNull static Logger getLoggerMessages() {
        return LOGGER_MESSAGES;
    }

    static {
        loadConfigFromFile();
        registry(LOGGER_COMMAND, COMMANDS_FILE, false);
        registry(LOGGER_ERROR, ERRORS_FILE, true);
        registry(LOGGER_MESSAGES, MESSAGES_FILE, true);
    }

    private static void loadConfigFromFile() {
        try {
            @NotNull final Class<?> clazz = LoggerService.class;
            @NotNull final InputStream inputStream = clazz.getResourceAsStream(CONFIG_FILE);
            MANAGER.readConfiguration(inputStream);
        } catch (final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    private static @NotNull ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private static void registry(@NotNull final Logger logger, @Nullable final String fileName, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(CONSOLE_HANDLER);
            logger.setUseParentHandlers(false);
            if (fileName != null && !fileName.isEmpty())
                logger.addHandler(new FileHandler(fileName));
        } catch (@NotNull final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @Override
    public void command(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_COMMAND.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGES.fine(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        if (e == null) return;
        LOGGER_ERROR.log(Level.SEVERE, e.getMessage(), e);
    }

    @Override
    public void info(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGES.info(message);
    }

    @Override
    public void initJmsLogger() {
        @NotNull final JmsLoggerProducer jmsLoggerProducer = new JmsLoggerProducer();
        @NotNull final EntityListener entityListener = new EntityListener(jmsLoggerProducer);
        @NotNull final EntityManagerFactory entityManagerFactory = connectionService.getEntityManagerFactory();
        @NotNull final SessionFactoryImpl sessionFactoryImpl = entityManagerFactory.unwrap(SessionFactoryImpl.class);
        @NotNull final EventListenerRegistry eventListenerRegistry = sessionFactoryImpl.getServiceRegistry().getService(EventListenerRegistry.class);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_INSERT).appendListener(entityListener);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(entityListener);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_DELETE).appendListener(entityListener);
    }

}
