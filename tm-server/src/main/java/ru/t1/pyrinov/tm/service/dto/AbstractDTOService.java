package ru.t1.pyrinov.tm.service.dto;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.pyrinov.tm.api.service.dto.IDTOService;
import ru.t1.pyrinov.tm.enumerated.SortOrder;
import ru.t1.pyrinov.tm.exception.field.IdEmptyException;
import ru.t1.pyrinov.tm.model.dto.AbstractDTOModel;
import ru.t1.pyrinov.tm.repository.dto.AbstractDTORepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public abstract class AbstractDTOService<M extends AbstractDTOModel> implements IDTOService<M> {

    @Getter
    @PersistenceContext
    protected @NotNull EntityManager entityManager;

    @Getter
    @Autowired
    protected @NotNull ApplicationContext context;

    protected abstract @NotNull AbstractDTORepository<M> getRepository();

    @NotNull
    @SneakyThrows
    @Transactional
    public M add(@NotNull final M model) {
        return getRepository().save(model);
    }

    @SneakyThrows
    @Transactional
    public void clear() {
        getRepository().deleteAll();
    }

    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return getRepository().existsById(id);
    }

    @SneakyThrows
    public @Nullable List<M> findAll() {
        return getRepository().findAll();
    }

    @Nullable
    @SneakyThrows
    public List<M> findAll(@Nullable final SortOrder sort) {
        if (sort == null) return findAll();
        return getRepository().findAll(getSort(sort));
    }

    @Nullable
    @SneakyThrows
    public M findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().findById(id).orElse(null);
    }

    public long count() {
        return getRepository().count();
    }

    @Transactional
    public void remove(@Nullable final M model) {
        getRepository().delete(model);
    }

    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        getRepository().deleteById(id);
    }

    @Transactional
    public void update(@Nullable final M model) {
        if (model == null) return;
        getRepository().save(model);
    }

    public static Sort getSort(@NotNull final SortOrder sortOrder) {
        if (sortOrder.equals(SortOrder.BY_NAME)) return Sort.by(Sort.Direction.ASC, "name");
        if (sortOrder.equals(SortOrder.BY_STATUS)) return Sort.by(Sort.Direction.ASC, "status");
        return Sort.by(Sort.Direction.ASC, "created");
    }

}
