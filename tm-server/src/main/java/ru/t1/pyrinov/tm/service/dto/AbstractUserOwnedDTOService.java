package ru.t1.pyrinov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.pyrinov.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.pyrinov.tm.exception.entity.EntityNullException;
import ru.t1.pyrinov.tm.exception.field.UserIdEmptyException;
import ru.t1.pyrinov.tm.exception.user.AccessDeniedException;
import ru.t1.pyrinov.tm.model.dto.AbstractUserOwnedDTOModel;
import ru.t1.pyrinov.tm.repository.dto.AbstractUserOwnedDTORepository;

import java.util.Objects;

public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedDTOModel>
        extends AbstractDTOService<M>
        implements IUserOwnedDTOService<M> {

    @NotNull
    protected abstract AbstractUserOwnedDTORepository<M> getRepository();

    @NotNull
    @SneakyThrows
    @Transactional
    public M add(@Nullable final String userId, @NotNull final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        //if(model.getUserId() == null) model.setUserId(userId);
        if (!Objects.equals(model.getUserId(), userId)) throw new AccessDeniedException();
        getRepository().save(model);
        return model;
    }

    @Transactional
    public void update(@Nullable final String userId, @Nullable final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNullException();
        if (!Objects.equals(model.getUserId(), userId)) throw new AccessDeniedException();
        getRepository().save(model);
    }

}
