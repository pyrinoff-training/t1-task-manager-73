package ru.t1.pyrinov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.pyrinov.tm.api.service.dto.IProjectDTOService;
import ru.t1.pyrinov.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.pyrinov.tm.api.service.dto.ITaskDTOService;
import ru.t1.pyrinov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.pyrinov.tm.exception.entity.TaskNotFoundException;
import ru.t1.pyrinov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.pyrinov.tm.exception.field.TaskIdEmptyException;
import ru.t1.pyrinov.tm.exception.field.UserIdEmptyException;
import ru.t1.pyrinov.tm.model.dto.TaskDTO;

import java.util.List;

@Service
public class ProjectTaskDTOService implements IProjectTaskDTOService {

    @Autowired
    private @NotNull IProjectDTOService projectService;

    @Autowired
    private @NotNull ITaskDTOService taskService;

    @Override
    @SneakyThrows
    public void bindTaskToProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final TaskDTO task = taskService.findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        taskService.updateProjectIdById(userId, taskId, projectId);
        task.setProjectId(projectId);
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final TaskDTO task = taskService.findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        taskService.updateProjectIdById(userId, taskId, null);
        task.setProjectId(null);
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, projectId);
        if (tasks == null) return;
        for (@NotNull final TaskDTO oneTask : tasks) taskService.removeById(userId, oneTask.getId());
        projectService.removeById(userId, projectId);
    }

}
