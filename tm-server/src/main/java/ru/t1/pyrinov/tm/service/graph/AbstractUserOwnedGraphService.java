package ru.t1.pyrinov.tm.service.graph;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.pyrinov.tm.api.service.graph.IUserOwnedGraphService;
import ru.t1.pyrinov.tm.exception.entity.EntityNullException;
import ru.t1.pyrinov.tm.exception.field.UserIdEmptyException;
import ru.t1.pyrinov.tm.exception.user.AccessDeniedException;
import ru.t1.pyrinov.tm.model.graph.AbstractUserOwnedGraphModel;
import ru.t1.pyrinov.tm.repository.graph.AbstractUserOwnedGraphRepository;

import java.util.Objects;

public abstract class AbstractUserOwnedGraphService<M extends AbstractUserOwnedGraphModel>
        extends AbstractGraphService<M>
        implements IUserOwnedGraphService<M> {

    @NotNull
    protected abstract AbstractUserOwnedGraphRepository<M> getRepository();

    @NotNull
    @SneakyThrows
    @Transactional
    public M add(@Nullable final String userId, @NotNull final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNullException();
        if (model.getUser() == null) throw new EntityNullException();
        if (!Objects.equals(model.getUser().getId(), userId)) throw new AccessDeniedException();
        getRepository().save(model);
        return model;
    }

    @Transactional
    public void update(@Nullable final String userId, @Nullable final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNullException();
        if (model.getUser() == null) throw new EntityNullException();
        if (!Objects.equals(model.getUser().getId(), userId)) throw new AccessDeniedException();
        getRepository().save(model);
    }

}
