package ru.t1.pyrinov.tm.service.graph;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.pyrinov.tm.api.service.graph.IProjectGraphService;
import ru.t1.pyrinov.tm.enumerated.SortOrder;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.exception.entity.EntityNullException;
import ru.t1.pyrinov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.pyrinov.tm.exception.entity.StatusNotFoundException;
import ru.t1.pyrinov.tm.exception.field.*;
import ru.t1.pyrinov.tm.model.graph.ProjectGraph;
import ru.t1.pyrinov.tm.repository.graph.ProjectGraphRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public final class ProjectGraphService
        extends AbstractUserOwnedGraphService<ProjectGraph>
        implements IProjectGraphService {

    @Getter
    @Autowired
    private @NotNull ProjectGraphRepository repository;

    @SneakyThrows
    @Transactional
    public ProjectGraph create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull ProjectGraph project = new ProjectGraph();
        project.setName(name);
        add(userId, project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectGraph create(@Nullable final String userId, @Nullable final String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @NotNull final ProjectGraph project = new ProjectGraph();
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Transactional
    public @NotNull Collection<ProjectGraph> add(@Nullable final String userId, @Nullable final Collection<ProjectGraph> models) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (models == null) throw new EntityNullException();
        @NotNull final List<ProjectGraph> result = new ArrayList<>();
        for (@NotNull final ProjectGraph model : models) {
            result.add(add(model));
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectGraph updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @Nullable final ProjectGraph project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description == null ? "" : description);
        update(project);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public ProjectGraph changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusNotFoundException();
        @Nullable final ProjectGraph project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(userId, project);
        return project;
    }

    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().existsByUserIdAndId(userId, id);
    }


    @SneakyThrows
    @Transactional
    public void removeByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        getRepository().deleteByUserId(userId);
    }

    @SneakyThrows
    public long countByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().countByUserId(userId);
    }

    @SneakyThrows
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final ProjectGraph model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNullException();
        getRepository().deleteByUserIdAndId(userId, model.getId());
    }

    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        getRepository().deleteByUserId(userId);
    }

    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        @Nullable ProjectGraph result = findById(userId, id);
        remove(userId, result);
    }

    @Nullable
    @SneakyThrows
    public List<ProjectGraph> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().findByUserId(userId);
    }

    @Nullable
    @SneakyThrows
    public List<ProjectGraph> findAll(@Nullable final String userId, @Nullable final SortOrder sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return getRepository().findByUserId(userId, getSort(sort));
    }

    @Nullable
    @SneakyThrows
    public ProjectGraph findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().findByUserIdAndId(userId, id).orElse(null);
    }

}
