package ru.t1.pyrinov.tm.service.graph;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.pyrinov.tm.api.service.graph.IProjectGraphService;
import ru.t1.pyrinov.tm.api.service.graph.IProjectTaskGraphService;
import ru.t1.pyrinov.tm.api.service.graph.ITaskGraphService;
import ru.t1.pyrinov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.pyrinov.tm.exception.entity.TaskNotFoundException;
import ru.t1.pyrinov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.pyrinov.tm.exception.field.TaskIdEmptyException;
import ru.t1.pyrinov.tm.exception.field.UserIdEmptyException;
import ru.t1.pyrinov.tm.model.graph.ProjectGraph;
import ru.t1.pyrinov.tm.model.graph.TaskGraph;

import java.util.List;

@Service
public class ProjectTaskGraphService implements IProjectTaskGraphService {

    @Autowired
    private @NotNull IProjectGraphService projectService;

    @Autowired
    private @NotNull ITaskGraphService taskService;

    @Override
    @SneakyThrows
    public void bindTaskToProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final TaskGraph task = taskService.findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        taskService.updateProjectIdById(userId, taskId, projectId);
        @Nullable final ProjectGraph projectGraph = projectService.findById(userId, projectId);
        if (projectGraph == null) throw new ProjectNotFoundException();
        task.setProject(projectGraph);
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final TaskGraph task = taskService.findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        taskService.updateProjectIdById(userId, taskId, null);
        task.setProject(null);
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final List<TaskGraph> tasks = taskService.findAllByProjectId(userId, projectId);
        if (tasks == null) return;
        for (@NotNull final TaskGraph oneTask : tasks) taskService.removeById(userId, oneTask.getId());
        projectService.removeById(userId, projectId);
    }

}
