package ru.t1.pyrinov.tm.service.graph;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.pyrinov.tm.api.service.graph.ISessionGraphService;
import ru.t1.pyrinov.tm.enumerated.SortOrder;
import ru.t1.pyrinov.tm.exception.entity.EntityNullException;
import ru.t1.pyrinov.tm.exception.field.IdEmptyException;
import ru.t1.pyrinov.tm.exception.field.UserIdEmptyException;
import ru.t1.pyrinov.tm.model.graph.SessionGraph;
import ru.t1.pyrinov.tm.repository.graph.SessionGraphRepository;

import java.util.List;

@Service
public class SessionGraphService
        extends AbstractUserOwnedGraphService<SessionGraph>
        implements ISessionGraphService {

    @Getter
    @Autowired
    private @NotNull SessionGraphRepository repository;

    @SneakyThrows
    @Transactional
    public void removeByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        getRepository().deleteByUserId(userId);
    }

    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return getRepository().existsByUserIdAndId(userId, id);
    }

    @SneakyThrows
    public long countByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().countByUserId(userId);
    }

    @SneakyThrows
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final SessionGraph model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNullException();
        getRepository().deleteByUserIdAndId(userId, model.getId());
    }

    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final String userId) {
        getRepository().deleteByUserId(userId);
    }

    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        @Nullable SessionGraph result = findById(userId, id);
        remove(userId, result);
    }

    @Nullable
    @SneakyThrows
    public List<SessionGraph> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().findByUserId(userId);
    }

    @Nullable
    @SneakyThrows
    public List<SessionGraph> findAll(@Nullable final String userId, @Nullable final SortOrder sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return getRepository().findByUserId(userId, getSort(sort));
    }

    @Nullable
    @SneakyThrows
    public SessionGraph findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().findByUserIdAndId(userId, id).orElse(null);
    }

}
