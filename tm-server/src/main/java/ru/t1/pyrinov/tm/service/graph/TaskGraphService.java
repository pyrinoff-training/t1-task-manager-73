package ru.t1.pyrinov.tm.service.graph;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.pyrinov.tm.api.service.graph.IProjectGraphService;
import ru.t1.pyrinov.tm.api.service.graph.ITaskGraphService;
import ru.t1.pyrinov.tm.enumerated.SortOrder;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.exception.entity.EntityNullException;
import ru.t1.pyrinov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.pyrinov.tm.exception.entity.StatusNotFoundException;
import ru.t1.pyrinov.tm.exception.entity.TaskNotFoundException;
import ru.t1.pyrinov.tm.exception.field.*;
import ru.t1.pyrinov.tm.model.graph.ProjectGraph;
import ru.t1.pyrinov.tm.model.graph.TaskGraph;
import ru.t1.pyrinov.tm.repository.graph.TaskGraphRepository;

import java.util.List;

@Service
public final class TaskGraphService
        extends AbstractUserOwnedGraphService<TaskGraph>
        implements ITaskGraphService {

    @Getter
    @Autowired
    private @NotNull TaskGraphRepository repository;

    @Autowired
    private @NotNull IProjectGraphService projectService;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskGraph create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @NotNull final TaskGraph task = new TaskGraph();
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskGraph create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final TaskGraph task = new TaskGraph();
        task.setName(name);
        return add(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskGraph updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @Nullable final TaskGraph task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskGraph changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final TaskGraph task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        if (status == null) throw new StatusNotFoundException();
        task.setStatus(status);
        update(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public List<TaskGraph> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        return repository.findByUserIdAndProjectId(userId, projectId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateProjectIdById(@Nullable final String userId, @Nullable final String id, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskGraph task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        @Nullable final ProjectGraph projectGraph = projectService.findById(userId, projectId);
        if (projectGraph == null) throw new ProjectNotFoundException();
        task.setProject(projectGraph);
        update(userId, task);
    }

    @SneakyThrows
    @Transactional
    public void removeByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        getRepository().deleteByUserId(userId);
    }

    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return getRepository().existsByUserIdAndId(userId, id);
    }

    @SneakyThrows
    public long countByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().countByUserId(userId);
    }

    @SneakyThrows
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final TaskGraph model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNullException();
        getRepository().deleteByUserIdAndId(userId, model.getId());
    }

    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final String userId) {
        getRepository().deleteByUserId(userId);
    }

    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        @Nullable TaskGraph result = findById(userId, id);
        remove(userId, result);
    }

    @Nullable
    @SneakyThrows
    public List<TaskGraph> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().findByUserId(userId);
    }

    @Nullable
    @SneakyThrows
    public List<TaskGraph> findAll(@Nullable final String userId, @Nullable final SortOrder sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return getRepository().findByUserId(userId, getSort(sort));
    }

    @Nullable
    @SneakyThrows
    public TaskGraph findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().findByUserIdAndId(userId, id).orElse(null);
    }

}
