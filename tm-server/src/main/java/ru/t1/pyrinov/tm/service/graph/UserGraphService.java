package ru.t1.pyrinov.tm.service.graph;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.pyrinov.tm.api.service.IPropertyService;
import ru.t1.pyrinov.tm.api.service.graph.IProjectGraphService;
import ru.t1.pyrinov.tm.api.service.graph.ITaskGraphService;
import ru.t1.pyrinov.tm.api.service.graph.IUserGraphService;
import ru.t1.pyrinov.tm.enumerated.Role;
import ru.t1.pyrinov.tm.exception.entity.UserNotFoundException;
import ru.t1.pyrinov.tm.exception.field.*;
import ru.t1.pyrinov.tm.model.graph.UserGraph;
import ru.t1.pyrinov.tm.repository.graph.UserGraphRepository;
import ru.t1.pyrinov.tm.util.HashUtil;

@Service
public class UserGraphService
        extends AbstractGraphService<UserGraph>
        implements IUserGraphService {

    @Autowired
    private @NotNull IProjectGraphService projectService;

    @Autowired
    private @NotNull ITaskGraphService taskService;

    @Autowired
    private @NotNull IPropertyService propertyService;

    @Getter
    @Autowired
    private @NotNull UserGraphRepository repository;

    public @NotNull UserGraph createUser(@NotNull String login, @NotNull String password, @Nullable String email, @Nullable Role role) {
        @NotNull final UserGraph user = new UserGraph();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.md5WithSalt(propertyService, password));
        user.setRole(role == null ? Role.USUAL : role);
        user.setEmail(email == null ? "" : email);
        return user;
    }

    @Transactional
    public @NotNull UserGraph create(@Nullable final String login, @Nullable final String password, @Nullable final String email, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (role == null) throw new RoleEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (isEmailExist(email)) throw new EmailExistsException();
        @NotNull final UserGraph user = createUser(login, password, email, role);
        return add(user);
    }

    @Override
    @Transactional
    public @NotNull UserGraph create(@Nullable String login, @Nullable String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        @NotNull final UserGraph user = createUser(login, password, null, Role.USUAL);
        return add(user);
    }

    @Override
    @Transactional
    public @NotNull UserGraph create(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (isEmailExist(email)) throw new EmailExistsException();
        @NotNull final UserGraph user = createUser(login, password, email, Role.USUAL);
        return add(user);
    }

    @Override
    @Transactional
    public @NotNull UserGraph create(@Nullable String login, @Nullable String password, @Nullable Role role) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        @NotNull final UserGraph user = createUser(login, password, null, role);
        return add(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserGraph findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserGraph findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByLogin(@Nullable final String login) {
        @Nullable final UserGraph user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        taskService.removeAll(userId);
        projectService.removeAll(userId);
        remove(user);
    }

    @SneakyThrows
    public void removeByEmail(@Nullable final String email) {
        @Nullable final UserGraph user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        taskService.removeAll(userId);
        projectService.removeAll(userId);
        remove(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserGraph user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.md5WithSalt(propertyService, password));
        update(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateUser(@Nullable final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserGraph user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        return findByEmail(email) != null;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        @Nullable final UserGraph user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        update(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        @Nullable final UserGraph user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        update(user);
    }

    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        getRepository().deleteById(userId);
    }


}
