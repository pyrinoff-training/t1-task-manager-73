package ru.t1.pyrinov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.api.service.ISaltProvider;

public interface HashUtil {

    @Nullable
    static String md5(@NotNull final String value) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(value.getBytes());
            @NotNull StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    static String md5WithSalt(@Nullable final String value, @NotNull final String secret, @NotNull final Integer iterations) {
        if (value == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < iterations; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    @Nullable
    static String md5WithSalt(@Nullable final ISaltProvider saltProvider, @Nullable final String value) {
        if (saltProvider == null) return null;
        @NotNull final String passwordSecret = saltProvider.getPasswordSecret();
        @NotNull final Integer passwordIteration = saltProvider.getPasswordIteration();
        return md5WithSalt(value, passwordSecret, passwordIteration);
    }

}
