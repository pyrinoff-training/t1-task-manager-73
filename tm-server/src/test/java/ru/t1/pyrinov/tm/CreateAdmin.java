package ru.t1.pyrinov.tm;


import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.pyrinov.tm.api.service.IPropertyService;
import ru.t1.pyrinov.tm.api.service.dto.IUserDTOService;
import ru.t1.pyrinov.tm.enumerated.Role;

public class CreateAdmin {

    @Autowired
    private static @NotNull IPropertyService PROPERTY_SERVICE;

    @Autowired
    private static @NotNull IUserDTOService USER_SERVICE;

    @SneakyThrows
    @Test
    public void createAdmin() {
        if (!USER_SERVICE.isLoginExist(PROPERTY_SERVICE.getAdminLogin())) {
            USER_SERVICE.create(PROPERTY_SERVICE.getAdminLogin(), PROPERTY_SERVICE.getAdminPassword(), Role.ADMIN);
        }
    }

}
