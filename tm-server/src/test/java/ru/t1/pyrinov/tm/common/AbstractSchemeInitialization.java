package ru.t1.pyrinov.tm.common;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.pyrinov.tm.api.service.IPropertyService;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class AbstractSchemeInitialization extends AbstractSpringTest {

    private static @NotNull final ClassLoaderResourceAccessor ACCESSOR = new ClassLoaderResourceAccessor();

    private static @Nullable Database DATABASE;

    @Autowired
    private @Nullable IPropertyService PROPERTY_SERVICE;

    private @NotNull Connection getConnection(@NotNull final Properties properties) throws SQLException {
        return DriverManager.getConnection(
                properties.getProperty("url"),
                properties.getProperty("username"),
                properties.getProperty("password")
        );
    }

    @BeforeAll
    public void schemaInitialization() throws IOException, SQLException, LiquibaseException {
        PROPERTY_SERVICE = context.getBean(IPropertyService.class);

        @NotNull final Properties properties = new Properties();
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream("liquibase.properties");
        properties.load(inputStream);
        if (!properties.contains("outputChangeLogFile"))
            properties.setProperty("outputChangeLogFile", PROPERTY_SERVICE.getLiquibaseChangelogFile(false));
        if (!properties.contains("url")) properties.setProperty("url", PROPERTY_SERVICE.getDatabaseUrl());
        if (!properties.contains("username"))
            properties.setProperty("username", PROPERTY_SERVICE.getDatabaseUsername());
        if (!properties.contains("password"))
            properties.setProperty("password", PROPERTY_SERVICE.getDatabasePassword());
        @NotNull final Connection connection = getConnection(properties);
        @NotNull final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        DATABASE = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);

        @NotNull final Liquibase liquibase = new Liquibase(PROPERTY_SERVICE.getLiquibaseChangelogFile(true), ACCESSOR, DATABASE);
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @AfterAll
    public void databaseShutdown() throws DatabaseException {
        assert DATABASE != null;
        DATABASE.close();
    }

}
