package ru.t1.pyrinov.tm.common;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.pyrinov.tm.configuration.ApplicationConfiguration;

@TestMethodOrder(MethodOrderer.MethodName.class)
public class AbstractSpringTest {

    //    @Autowired
    //    protected @NotNull ApplicationContext context;

    @NotNull
    protected final static AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ApplicationConfiguration.class);

    @BeforeAll
    public static void setUp() throws Exception {
        System.out.println("Spring Config initialized...");
    }

    @AfterAll
    public static void tearDown() throws Exception {
        System.out.println("Spring context closed...");
    }

}
