package ru.t1.pyrinov.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.t1.pyrinov.tm.comparator.NameComparator;
import ru.t1.pyrinov.tm.model.dto.ProjectDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@UtilityClass
public final class ProjectTestData {

    @NotNull
    public final static ProjectDTO USER_PROJECT1 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO USER_PROJECT2 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO USER_PROJECT3 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO ADMIN_PROJECT1 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO ADMIN_PROJECT2 = new ProjectDTO();

    @NotNull
    public final static String NON_EXISTING_PROJECT_ID = UUID.randomUUID().toString();

    @NotNull
    public final static List<ProjectDTO> USER_PROJECT_LIST = Arrays.asList(USER_PROJECT1, USER_PROJECT2, USER_PROJECT3);

    @NotNull
    public final static List<ProjectDTO> ADMIN_PROJECT_LIST = Arrays.asList(ADMIN_PROJECT1, ADMIN_PROJECT2);

    @NotNull
    public final static List<ProjectDTO> PROJECT_LIST = new ArrayList<>();

    @NotNull
    public final static List<ProjectDTO> SORTED_PROJECT_LIST = new ArrayList<>();

    static {
        USER_PROJECT_LIST.forEach(project -> project.setName("USER PROJECT " + Math.random() * 100));
        USER_PROJECT_LIST.forEach(project -> project.setDescription("DESCRIPTION"));

        ADMIN_PROJECT_LIST.forEach(project -> project.setUserId(UserTestData.USER_ADMIN.getId()));
        ADMIN_PROJECT_LIST.forEach(project -> project.setName("ADMIN PROJECT " + Math.random() * 100));
        ADMIN_PROJECT_LIST.forEach(project -> project.setDescription("DESCRIPTION"));

        PROJECT_LIST.addAll(USER_PROJECT_LIST);
        PROJECT_LIST.addAll(ADMIN_PROJECT_LIST);

        SORTED_PROJECT_LIST.addAll(PROJECT_LIST);
        SORTED_PROJECT_LIST.sort(NameComparator.INSTANCE);
    }

}
