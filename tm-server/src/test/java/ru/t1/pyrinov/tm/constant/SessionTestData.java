package ru.t1.pyrinov.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.model.dto.SessionDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@UtilityClass
public final class SessionTestData {

    @NotNull
    public final static SessionDTO USER_SESSION1 = new SessionDTO();

    @NotNull
    public final static SessionDTO USER_SESSION2 = new SessionDTO();

    @NotNull
    public final static SessionDTO USER_SESSION3 = new SessionDTO();

    @NotNull
    public final static SessionDTO ADMIN_SESSION1 = new SessionDTO();

    @Nullable
    public final static SessionDTO NULL_SESSION = null;

    @NotNull
    public final static String NON_EXISTING_SESSION_ID = UUID.randomUUID().toString();

    @NotNull
    public final static List<SessionDTO> USER_SESSION_LIST = Arrays.asList(USER_SESSION1, USER_SESSION2, USER_SESSION3);

    @NotNull
    public final static List<SessionDTO> ADMIN_SESSION_LIST = List.of(ADMIN_SESSION1);

    @NotNull
    public final static List<SessionDTO> SESSION_LIST = new ArrayList<>();

    static {
        SESSION_LIST.addAll(USER_SESSION_LIST);
        SESSION_LIST.addAll(ADMIN_SESSION_LIST);
    }

}
