package ru.t1.pyrinov.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.comparator.NameComparator;
import ru.t1.pyrinov.tm.model.dto.TaskDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@UtilityClass
public final class TaskTestData {

    @NotNull
    public final static TaskDTO USER_TASK1 = new TaskDTO();

    @NotNull
    public final static TaskDTO USER_TASK2 = new TaskDTO();

    @NotNull
    public final static TaskDTO USER_TASK3 = new TaskDTO();

    @NotNull
    public final static TaskDTO ADMIN_TASK1 = new TaskDTO();

    @Nullable
    public final static TaskDTO NULL_TASK = null;

    @NotNull
    public final static String NON_EXISTING_TASK_ID = UUID.randomUUID().toString();

    @NotNull
    public final static List<TaskDTO> USER_TASK_LIST = Arrays.asList(USER_TASK1, USER_TASK2, USER_TASK3);

    @NotNull
    public final static List<TaskDTO> ADMIN_TASK_LIST = List.of(ADMIN_TASK1);

    @NotNull
    public final static List<TaskDTO> TASK_LIST = new ArrayList<>();

    @NotNull
    public final static List<TaskDTO> SORTED_TASK_LIST = new ArrayList<>();

    static {
        USER_TASK_LIST.forEach(task -> task.setName("User Test Task " + task.getId()));
        USER_TASK_LIST.forEach(task -> task.setDescription("User Test Task " + task.getId() + " description"));
        USER_TASK_LIST.forEach(task -> task.setProjectId(ProjectTestData.USER_PROJECT1.getId()));
        ADMIN_TASK_LIST.forEach(task -> task.setUserId(UserTestData.USER_ADMIN.getId()));
        ADMIN_TASK_LIST.forEach(task -> task.setName("Admin Test Task " + task.getId()));
        ADMIN_TASK_LIST.forEach(task -> task.setDescription("Admin Test Task " + task.getId() + " description"));
        TASK_LIST.addAll(USER_TASK_LIST);
        TASK_LIST.addAll(ADMIN_TASK_LIST);
        SORTED_TASK_LIST.addAll(TASK_LIST);
        SORTED_TASK_LIST.sort(NameComparator.INSTANCE);
    }

}
