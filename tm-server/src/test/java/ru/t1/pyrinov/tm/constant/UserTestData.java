package ru.t1.pyrinov.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.pyrinov.tm.api.service.IPropertyService;
import ru.t1.pyrinov.tm.common.AbstractSpringTest;
import ru.t1.pyrinov.tm.model.dto.UserDTO;
import ru.t1.pyrinov.tm.util.HashUtil;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public final class UserTestData extends AbstractSpringTest {

    @NotNull
    public final static String USER_USER_LOGIN = "user";

    @NotNull
    public final static String USER_USER_PASSWORD = "user";

    @NotNull
    public final static String USER_USER_EMAIL = "user@user.ru";

    @NotNull
    public final static String USER_USER2_LOGIN = "user2";

    @NotNull
    public final static String USER2_TEST_PASSWORD = "user2";

    @NotNull
    public final static String USER2_TEST_EMAIL = "user2@user2.ru";

    @NotNull
    public final static String USER_ADMIN_LOGIN = "admin";

    @NotNull
    public final static String USER_ADMIN_PASSWORD = "admin";

    @NotNull
    public final static String USER_ADMIN_EMAIL = "admin@admin.ru";

    @NotNull
    public final static String NOT_CREATED_USER_LOGIN = "someuser";

    @NotNull
    public final static String NON_EXISTING_USER_ID = "123";

    @NotNull
    public final static UserDTO USER_USER = new UserDTO();

    @NotNull
    public final static UserDTO USER2_TEST = new UserDTO();

    @NotNull
    public final static UserDTO USER_ADMIN = new UserDTO();

    @Nullable
    public final static UserDTO NULL_USER = null;

    @NotNull
    public final static List<UserDTO> USER_LIST = Arrays.asList(USER_USER, USER2_TEST, USER_ADMIN);

    @NotNull
    public final static List<UserDTO> USER_LIST_ADDED = List.of(USER_ADMIN);

    @Autowired
    private static @NotNull IPropertyService PROPERTY_SERVICE;

    static {
        PROPERTY_SERVICE = context.getBean(IPropertyService.class);

        USER_USER.setLogin(USER_USER_LOGIN);
        USER_USER.setPasswordHash(HashUtil.md5WithSalt(PROPERTY_SERVICE, USER_USER_PASSWORD));
        USER_USER.setEmail(USER_USER_EMAIL);

        USER2_TEST.setLogin(USER_USER2_LOGIN);
        USER2_TEST.setPasswordHash(HashUtil.md5WithSalt(PROPERTY_SERVICE, USER2_TEST_PASSWORD));
        USER2_TEST.setEmail(USER2_TEST_EMAIL);

        USER_ADMIN.setLogin(USER_ADMIN_LOGIN);
        USER_ADMIN.setPasswordHash(HashUtil.md5WithSalt(PROPERTY_SERVICE, USER_ADMIN_PASSWORD));
        USER_ADMIN.setEmail(USER_ADMIN_EMAIL);
    }

}
