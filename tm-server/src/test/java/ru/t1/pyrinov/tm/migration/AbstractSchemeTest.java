package ru.t1.pyrinov.tm.migration;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import ru.t1.pyrinov.tm.api.service.IPropertyService;
import ru.t1.pyrinov.tm.common.AbstractSpringTest;
import ru.t1.pyrinov.tm.marker.MigrationTest;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public abstract class AbstractSchemeTest extends AbstractSpringTest implements MigrationTest {

    private static @NotNull final ClassLoaderResourceAccessor ACCESSOR = new ClassLoaderResourceAccessor();

    private static @Nullable Database DATABASE;

    private static @NotNull IPropertyService PROPERTY_SERVICE;

    @AfterAll
    public static void after() throws DatabaseException {
        DATABASE.close();
    }

    @BeforeAll
    public static void before() throws IOException, SQLException, DatabaseException {
        PROPERTY_SERVICE = context.getBean(IPropertyService.class);
        @NotNull final Properties properties = new Properties();
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream("liquibase.properties");
        properties.load(inputStream);
        if (!properties.contains("outputChangeLogFile"))
            properties.setProperty("outputChangeLogFile", PROPERTY_SERVICE.getLiquibaseChangelogFile(false));
        if (!properties.contains("url")) properties.setProperty("url", PROPERTY_SERVICE.getDatabaseUrl());
        if (!properties.contains("username"))
            properties.setProperty("username", PROPERTY_SERVICE.getDatabaseUsername());
        if (!properties.contains("password"))
            properties.setProperty("password", PROPERTY_SERVICE.getDatabasePassword());
        @NotNull final Connection connection = getConnection(properties);
        @NotNull final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        DATABASE = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
    }

    private static @NotNull Connection getConnection(@NotNull final Properties properties) throws SQLException {
        return DriverManager.getConnection(
                properties.getProperty("url"),
                properties.getProperty("username"),
                properties.getProperty("password")
        );
    }

    public static @NotNull Liquibase initLiquibase(@NotNull final String filename) {
        return new Liquibase(filename, ACCESSOR, DATABASE);
    }

    public static @NotNull Liquibase getLiquibase() {
        return initLiquibase(PROPERTY_SERVICE.getLiquibaseChangelogFile(true));
    }

}
