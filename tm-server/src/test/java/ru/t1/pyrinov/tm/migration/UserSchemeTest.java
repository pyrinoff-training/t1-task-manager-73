package ru.t1.pyrinov.tm.migration;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

public class UserSchemeTest extends AbstractSchemeTest {

    @Test
    public void Test() throws LiquibaseException {
        @NotNull final Liquibase liquibase = getLiquibase();
        liquibase.dropAll();
        liquibase.update("user");
    }

}
