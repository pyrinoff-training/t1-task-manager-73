package ru.t1.pyrinov.tm.service;


import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import ru.t1.pyrinov.tm.api.service.IPropertyService;
import ru.t1.pyrinov.tm.api.service.dto.IUserDTOService;
import ru.t1.pyrinov.tm.common.AbstractSpringTest;
import ru.t1.pyrinov.tm.enumerated.Role;

public class AdminCreationTest extends AbstractSpringTest {

    private @NotNull IUserDTOService USER_SERVICE;
    private @NotNull IPropertyService PROPERTY_SERVICE;

    @Test
    @Disabled
    @SneakyThrows
    public void createAdmin() {
        USER_SERVICE = context.getBean(IUserDTOService.class);
        PROPERTY_SERVICE = context.getBean(IPropertyService.class);
        if (!USER_SERVICE.isLoginExist(PROPERTY_SERVICE.getAdminLogin())) {
            USER_SERVICE.create(PROPERTY_SERVICE.getAdminLogin(), PROPERTY_SERVICE.getAdminPassword(), Role.ADMIN);
        }
    }

}
