package ru.t1.pyrinov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;
import ru.t1.pyrinov.tm.api.service.dto.IProjectDTOService;
import ru.t1.pyrinov.tm.api.service.dto.IUserDTOService;
import ru.t1.pyrinov.tm.common.AbstractSchemeInitialization;
import ru.t1.pyrinov.tm.enumerated.SortOrder;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.pyrinov.tm.exception.entity.StatusNotFoundException;
import ru.t1.pyrinov.tm.exception.field.*;
import ru.t1.pyrinov.tm.marker.UnitTest;
import ru.t1.pyrinov.tm.model.dto.ProjectDTO;
import ru.t1.pyrinov.tm.model.dto.UserDTO;

import java.util.List;

import static ru.t1.pyrinov.tm.constant.ProjectTestData.*;
import static ru.t1.pyrinov.tm.constant.UserTestData.USER_USER_LOGIN;
import static ru.t1.pyrinov.tm.constant.UserTestData.USER_USER_PASSWORD;

@TestMethodOrder(MethodOrderer.MethodName.class)
public final class ProjectServiceTest extends AbstractSchemeInitialization implements UnitTest {

    private static @NotNull IProjectDTOService PROJECT_DTO_SERVICE;

    private static @NotNull IUserDTOService USER_DTO_SERVICE;

    @NotNull
    private static String USER_ID = "";

    @BeforeAll
    public static void setUp() throws Exception {
        PROJECT_DTO_SERVICE = context.getBean(IProjectDTOService.class);
        USER_DTO_SERVICE = context.getBean(IUserDTOService.class);

        @NotNull final UserDTO user = USER_DTO_SERVICE.create(USER_USER_LOGIN, USER_USER_PASSWORD);
        USER_ID = user.getId();
        USER_PROJECT_LIST.forEach(project -> project.setUserId(USER_ID));
    }

    @AfterAll
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_DTO_SERVICE.findByLogin(USER_USER_LOGIN);
        if (user != null) USER_DTO_SERVICE.remove(user);
    }

    @BeforeEach
    public void before() throws Exception {
        PROJECT_DTO_SERVICE.add(USER_ID, USER_PROJECT1);
        PROJECT_DTO_SERVICE.add(USER_ID, USER_PROJECT2);
    }

    @AfterEach
    public void after() throws Exception {
        PROJECT_DTO_SERVICE.removeAll(USER_ID);
    }

    @Test
    public void setupOk() {
    }

    @Test
    public void addByUserId() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.add(null, USER_PROJECT3));
        Assertions.assertNotNull(PROJECT_DTO_SERVICE.add(USER_ID, USER_PROJECT3));
        @Nullable final ProjectDTO project = PROJECT_DTO_SERVICE.findById(USER_ID, USER_PROJECT3.getId());
        Assertions.assertNotNull(project);
        Assertions.assertEquals(USER_PROJECT3.getId(), project.getId());
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.findAll(""));
        final List<ProjectDTO> projects = PROJECT_DTO_SERVICE.findAll(USER_ID);
        Assertions.assertNotNull(projects);
        Assertions.assertEquals(2, projects.size());
        projects.forEach(project -> Assertions.assertEquals(USER_ID, project.getUserId()));
    }

    @Test
    public void findAllSortByUserId() throws Exception {
        @Nullable SortOrder sort = null;
        Assertions.assertNotNull(PROJECT_DTO_SERVICE.findAll(USER_ID, sort));
        Assertions.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable SortOrder sortInner = null;
            PROJECT_DTO_SERVICE.findAll("", sortInner);
        });
        sort = SortOrder.BY_NAME;
        final List<ProjectDTO> projects = PROJECT_DTO_SERVICE.findAll(USER_ID, sort);
        Assertions.assertNotNull(projects);
        projects.forEach(project -> Assertions.assertEquals(USER_ID, project.getUserId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.existsById(null, NON_EXISTING_PROJECT_ID));
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.existsById("", NON_EXISTING_PROJECT_ID));
        Assertions.assertThrows(IdEmptyException.class, () -> PROJECT_DTO_SERVICE.existsById(USER_ID, ""));
        Assertions.assertFalse(PROJECT_DTO_SERVICE.existsById(USER_ID, NON_EXISTING_PROJECT_ID));
        Assertions.assertTrue(PROJECT_DTO_SERVICE.existsById(USER_ID, USER_PROJECT1.getId()));
    }

    @Test
    public void findByIdByUserId() throws Exception {
        Assertions.assertThrows(IdEmptyException.class, () -> PROJECT_DTO_SERVICE.findById(USER_ID, ""));
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.existsById("", USER_PROJECT1.getId()));
        Assertions.assertNull(PROJECT_DTO_SERVICE.findById(USER_ID, NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = PROJECT_DTO_SERVICE.findById(USER_ID, USER_PROJECT1.getId());
        Assertions.assertNotNull(project);
        Assertions.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void clearByUserId() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.removeAll(""));
        PROJECT_DTO_SERVICE.removeAll(USER_ID);
        Assertions.assertEquals(0, PROJECT_DTO_SERVICE.countByUserId(USER_ID));
    }

    @Test
    public void removeByUserId() throws Exception {
        PROJECT_DTO_SERVICE.removeById(USER_ID, USER_PROJECT2.getId());
        Assertions.assertNull(PROJECT_DTO_SERVICE.findById(USER_ID, USER_PROJECT2.getId()));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.removeById(null, null));
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.removeById("", null));
        Assertions.assertThrows(IdEmptyException.class, () -> PROJECT_DTO_SERVICE.removeById(USER_ID, null));
        Assertions.assertThrows(IdEmptyException.class, () -> PROJECT_DTO_SERVICE.removeById(USER_ID, ""));
        PROJECT_DTO_SERVICE.removeById(USER_ID, USER_PROJECT2.getId());
        Assertions.assertNull(PROJECT_DTO_SERVICE.findById(USER_ID, USER_PROJECT2.getId()));
    }

    @Test
    public void countByUserId() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.countByUserId(""));
        Assertions.assertEquals(2, PROJECT_DTO_SERVICE.countByUserId(USER_ID));
    }

    @Test
    public void create() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.create(null, USER_PROJECT3.getName()));
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.create("", USER_PROJECT3.getName()));
        Assertions.assertThrows(NameEmptyException.class, () -> PROJECT_DTO_SERVICE.create(USER_ID, null));
        Assertions.assertThrows(NameEmptyException.class, () -> PROJECT_DTO_SERVICE.create(USER_ID, ""));
        @Nullable final String userProject3Name = USER_PROJECT3.getName();
        if (userProject3Name == null) return;
        @NotNull final ProjectDTO project = PROJECT_DTO_SERVICE.create(USER_ID, USER_PROJECT3.getName());
        Assertions.assertNotNull(project);
        @Nullable final ProjectDTO findProject = PROJECT_DTO_SERVICE.findById(USER_ID, project.getId());
        Assertions.assertNotNull(findProject);
        Assertions.assertEquals(project.getId(), findProject.getId());
        Assertions.assertEquals(USER_PROJECT3.getName(), project.getName());
        Assertions.assertEquals(USER_ID, project.getUserId());
    }

    @Test
    public void createWithDescription() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.create(null, USER_PROJECT3.getName(), USER_PROJECT3.getDescription()));
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.create("", USER_PROJECT3.getName(), USER_PROJECT3.getDescription()));
        Assertions.assertThrows(NameEmptyException.class, () -> PROJECT_DTO_SERVICE.create(USER_ID, null, USER_PROJECT3.getDescription()));
        Assertions.assertThrows(NameEmptyException.class, () -> PROJECT_DTO_SERVICE.create(USER_ID, "", USER_PROJECT3.getDescription()));
        Assertions.assertThrows(DescriptionEmptyException.class, () -> PROJECT_DTO_SERVICE.create(USER_ID, USER_PROJECT3.getName(), null));
        @NotNull final ProjectDTO project = PROJECT_DTO_SERVICE.create(USER_ID, USER_PROJECT3.getName(), USER_PROJECT3.getDescription());
        Assertions.assertNotNull(project);
        @Nullable final ProjectDTO findProject = PROJECT_DTO_SERVICE.findById(USER_ID, project.getId());
        Assertions.assertNotNull(findProject);
        Assertions.assertEquals(project.getId(), findProject.getId());
        Assertions.assertEquals(USER_PROJECT3.getName(), project.getName());
        Assertions.assertEquals(USER_PROJECT3.getDescription(), project.getDescription());
        Assertions.assertEquals(USER_ID, project.getUserId());
    }

    @Test
    public void updateById() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.updateById(null, USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.updateById("", USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assertions.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_DTO_SERVICE.updateById(USER_ID, null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assertions.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_DTO_SERVICE.updateById(USER_ID, "", USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assertions.assertThrows(NameEmptyException.class, () -> PROJECT_DTO_SERVICE.updateById(USER_ID, USER_PROJECT1.getId(), null, USER_PROJECT1.getDescription()));
        Assertions.assertThrows(NameEmptyException.class, () -> PROJECT_DTO_SERVICE.updateById(USER_ID, USER_PROJECT1.getId(), "", USER_PROJECT1.getDescription()));
        Assertions.assertThrows(DescriptionEmptyException.class, () -> PROJECT_DTO_SERVICE.updateById(USER_ID, USER_PROJECT1.getId(), USER_PROJECT1.getName(), null));
        Assertions.assertThrows(ProjectNotFoundException.class, () -> PROJECT_DTO_SERVICE.updateById(USER_ID, NON_EXISTING_PROJECT_ID, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        @NotNull final String name = USER_PROJECT1.getName() + NON_EXISTING_PROJECT_ID;
        @NotNull final String description = USER_PROJECT1.getDescription() + NON_EXISTING_PROJECT_ID;
        PROJECT_DTO_SERVICE.updateById(USER_ID, USER_PROJECT1.getId(), name, description);
        @Nullable final ProjectDTO project = PROJECT_DTO_SERVICE.findById(USER_ID, USER_PROJECT1.getId());
        Assertions.assertNotNull(project);
        Assertions.assertEquals(name, project.getName());
        Assertions.assertEquals(description, project.getDescription());
    }

    @Test
    public void changeProjectStatusById() throws Exception {
        @NotNull final Status status = Status.COMPLETED;
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.changeProjectStatusById(null, USER_PROJECT1.getId(), status));
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.changeProjectStatusById("", USER_PROJECT1.getId(), status));
        Assertions.assertThrows(IdEmptyException.class, () -> PROJECT_DTO_SERVICE.changeProjectStatusById(USER_ID, null, status));
        Assertions.assertThrows(IdEmptyException.class, () -> PROJECT_DTO_SERVICE.changeProjectStatusById(USER_ID, "", status));
        Assertions.assertThrows(StatusNotFoundException.class, () -> PROJECT_DTO_SERVICE.changeProjectStatusById(USER_ID, USER_PROJECT1.getId(), null));
        Assertions.assertThrows(ProjectNotFoundException.class, () -> PROJECT_DTO_SERVICE.changeProjectStatusById(USER_ID, NON_EXISTING_PROJECT_ID, status));
        PROJECT_DTO_SERVICE.changeProjectStatusById(USER_ID, USER_PROJECT1.getId(), status);
        @Nullable final ProjectDTO project = PROJECT_DTO_SERVICE.findById(USER_ID, USER_PROJECT1.getId());
        Assertions.assertNotNull(project);
        Assertions.assertEquals(status, project.getStatus());
    }

}
