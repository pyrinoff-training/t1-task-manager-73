package ru.t1.pyrinov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;
import ru.t1.pyrinov.tm.api.service.dto.IProjectDTOService;
import ru.t1.pyrinov.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.pyrinov.tm.api.service.dto.ITaskDTOService;
import ru.t1.pyrinov.tm.api.service.dto.IUserDTOService;
import ru.t1.pyrinov.tm.common.AbstractSchemeInitialization;
import ru.t1.pyrinov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.pyrinov.tm.exception.entity.TaskNotFoundException;
import ru.t1.pyrinov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.pyrinov.tm.exception.field.TaskIdEmptyException;
import ru.t1.pyrinov.tm.exception.field.UserIdEmptyException;
import ru.t1.pyrinov.tm.marker.UnitTest;
import ru.t1.pyrinov.tm.model.dto.TaskDTO;
import ru.t1.pyrinov.tm.model.dto.UserDTO;

import static ru.t1.pyrinov.tm.constant.ProjectTestData.*;
import static ru.t1.pyrinov.tm.constant.TaskTestData.*;
import static ru.t1.pyrinov.tm.constant.UserTestData.USER_USER_LOGIN;
import static ru.t1.pyrinov.tm.constant.UserTestData.USER_USER_PASSWORD;

@TestMethodOrder(MethodOrderer.MethodName.class)
public final class ProjectTaskServiceTest extends AbstractSchemeInitialization implements UnitTest {

    private static @NotNull IProjectDTOService PROJECT_SERVICE;

    private static @NotNull ITaskDTOService TASK_SERVICE;

    private static @NotNull IUserDTOService USER_SERVICE;

    private static @NotNull IProjectTaskDTOService PROJECT_TASK_SERVICE;

    @NotNull
    private static String USER_ID = "";


    @BeforeAll
    public static void setUp() throws Exception {
        PROJECT_SERVICE = context.getBean(IProjectDTOService.class);
        TASK_SERVICE = context.getBean(ITaskDTOService.class);
        USER_SERVICE = context.getBean(IUserDTOService.class);
        PROJECT_TASK_SERVICE = context.getBean(IProjectTaskDTOService.class);

        @NotNull final UserDTO user = USER_SERVICE.create(USER_USER_LOGIN, USER_USER_PASSWORD);
        USER_ID = user.getId();
        USER_TASK_LIST.forEach(task -> task.setUserId(USER_ID));
        USER_PROJECT_LIST.forEach(task -> task.setUserId(USER_ID));
    }

    @AfterAll
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_USER_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
    }

    @BeforeEach
    public void before() throws Exception {
        PROJECT_SERVICE.add(USER_ID, USER_PROJECT1);
        PROJECT_SERVICE.add(USER_ID, USER_PROJECT2);
        TASK_SERVICE.add(USER_ID, USER_TASK1);
        TASK_SERVICE.add(USER_ID, USER_TASK2);
    }

    @AfterEach
    public void after() throws Exception {
        TASK_SERVICE.removeAll(USER_ID);
        PROJECT_SERVICE.removeAll(USER_ID);
    }

    @Test
    public void setupOk() {
    }

    @Test
    public void bindTaskToProject() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_TASK_SERVICE.bindTaskToProject(null, USER_PROJECT1.getId(), USER_TASK1.getId()));
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_TASK_SERVICE.bindTaskToProject("", USER_PROJECT1.getId(), USER_TASK1.getId()));
        Assertions.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_TASK_SERVICE.bindTaskToProject(USER_ID, null, USER_TASK1.getId()));
        Assertions.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_TASK_SERVICE.bindTaskToProject(USER_ID, "", USER_TASK1.getId()));
        Assertions.assertThrows(TaskIdEmptyException.class, () -> PROJECT_TASK_SERVICE.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), null));
        Assertions.assertThrows(TaskIdEmptyException.class, () -> PROJECT_TASK_SERVICE.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), ""));
        Assertions.assertThrows(ProjectNotFoundException.class, () -> PROJECT_TASK_SERVICE.bindTaskToProject(USER_ID, NON_EXISTING_PROJECT_ID, USER_TASK1.getId()));
        Assertions.assertThrows(TaskNotFoundException.class, () -> PROJECT_TASK_SERVICE.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), NON_EXISTING_TASK_ID));
        PROJECT_TASK_SERVICE.bindTaskToProject(USER_ID, USER_PROJECT2.getId(), USER_TASK1.getId());
        @Nullable final TaskDTO task = TASK_SERVICE.findById(USER_ID, USER_TASK1.getId());
        Assertions.assertNotNull(task);
        Assertions.assertEquals(USER_PROJECT2.getId(), task.getProjectId());
        PROJECT_TASK_SERVICE.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), USER_TASK1.getId());
    }

    @Test
    public void removeProjectById() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_TASK_SERVICE.removeProjectById(null, USER_PROJECT1.getId()));
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_TASK_SERVICE.removeProjectById("", USER_PROJECT1.getId()));
        Assertions.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_TASK_SERVICE.removeProjectById(USER_ID, null));
        Assertions.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_TASK_SERVICE.removeProjectById(USER_ID, ""));
        Assertions.assertThrows(ProjectNotFoundException.class, () -> PROJECT_TASK_SERVICE.removeProjectById(USER_ID, NON_EXISTING_PROJECT_ID));
        PROJECT_TASK_SERVICE.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), USER_TASK1.getId());
        PROJECT_TASK_SERVICE.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), USER_TASK2.getId());
        PROJECT_TASK_SERVICE.removeProjectById(USER_ID, USER_PROJECT1.getId());
        Assertions.assertNull(PROJECT_SERVICE.findById(USER_ID, USER_PROJECT1.getId()));
        Assertions.assertNull(TASK_SERVICE.findById(USER_ID, USER_TASK1.getId()));
        Assertions.assertNull(TASK_SERVICE.findById(USER_ID, USER_TASK2.getId()));
    }

    @Test
    public void unbindTaskFromProject() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_TASK_SERVICE.unbindTaskFromProject(null, USER_PROJECT1.getId(), USER_TASK1.getId()));
        Assertions.assertThrows(UserIdEmptyException.class, () -> PROJECT_TASK_SERVICE.unbindTaskFromProject("", USER_PROJECT1.getId(), USER_TASK1.getId()));
        Assertions.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_TASK_SERVICE.unbindTaskFromProject(USER_ID, null, USER_TASK1.getId()));
        Assertions.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_TASK_SERVICE.unbindTaskFromProject(USER_ID, "", USER_TASK1.getId()));
        Assertions.assertThrows(TaskIdEmptyException.class, () -> PROJECT_TASK_SERVICE.unbindTaskFromProject(USER_ID, USER_PROJECT1.getId(), null));
        Assertions.assertThrows(TaskIdEmptyException.class, () -> PROJECT_TASK_SERVICE.unbindTaskFromProject(USER_ID, USER_PROJECT1.getId(), ""));
        Assertions.assertThrows(ProjectNotFoundException.class, () -> PROJECT_TASK_SERVICE.unbindTaskFromProject(USER_ID, NON_EXISTING_PROJECT_ID, USER_TASK1.getId()));
        Assertions.assertThrows(TaskNotFoundException.class, () -> PROJECT_TASK_SERVICE.unbindTaskFromProject(USER_ID, USER_PROJECT1.getId(), NON_EXISTING_TASK_ID));
        PROJECT_TASK_SERVICE.unbindTaskFromProject(USER_ID, USER_PROJECT1.getId(), USER_TASK1.getId());
        @Nullable final TaskDTO task = TASK_SERVICE.findById(USER_ID, USER_TASK1.getId());
        Assertions.assertNotNull(task);
        Assertions.assertNull(task.getProjectId());
        PROJECT_TASK_SERVICE.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), USER_TASK1.getId());
    }

}
