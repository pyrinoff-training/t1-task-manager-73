package ru.t1.pyrinov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.*;
import ru.t1.pyrinov.tm.api.service.IPropertyService;
import ru.t1.pyrinov.tm.common.AbstractSpringTest;
import ru.t1.pyrinov.tm.marker.UnitTest;

@TestMethodOrder(MethodOrderer.MethodName.class)
public final class PropertyServiceTest extends AbstractSpringTest implements UnitTest {

    private static @NotNull IPropertyService PROPERTY_SERVICE;

    @BeforeAll
    public static void setUp() {
        PROPERTY_SERVICE = context.getBean(IPropertyService.class);
    }

    @Test
    public void setupOk() {
    }

    @Test
    @Disabled
    public void getApplicationVersion() {
        Assertions.assertNotNull(PROPERTY_SERVICE.getApplicationVersion());
    }

    @Test
    @Disabled
    public void getAuthorName() {
        Assertions.assertNotNull(PROPERTY_SERVICE.getAuthorName());
    }

    @Test
    @Disabled
    public void getAuthorEmail() {
        Assertions.assertNotNull(PROPERTY_SERVICE.getAuthorEmail());
    }

    @Test
    public void getServerPort() {
        Assertions.assertNotNull(PROPERTY_SERVICE.getServerPort());
    }

    @Test
    public void getServerHost() {
        Assertions.assertNotNull(PROPERTY_SERVICE.getServerHost());
    }

    @Test
    public void getSessionKey() {
        Assertions.assertNotNull(PROPERTY_SERVICE.getSessionKey());
    }

    @Test
    public void getSessionTimeout() {
        Assertions.assertNotNull(PROPERTY_SERVICE.getSessionTimeout());
    }

    @Test
    public void getDatabaseUsername() {
        Assertions.assertNotNull(PROPERTY_SERVICE.getDatabaseUsername());
    }

    @Test
    public void getDatabasePassword() {
        Assertions.assertNotNull(PROPERTY_SERVICE.getDatabasePassword());
    }

    @Test
    public void getDatabaseUrl() {
        Assertions.assertNotNull(PROPERTY_SERVICE.getDatabaseUrl());
    }

    @Test
    public void getDatabaseDriver() {
        Assertions.assertNotNull(PROPERTY_SERVICE.getDatabaseDriver());
    }

}
