package ru.t1.pyrinov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;
import ru.t1.pyrinov.tm.api.service.dto.ISessionDTOService;
import ru.t1.pyrinov.tm.api.service.dto.IUserDTOService;
import ru.t1.pyrinov.tm.common.AbstractSchemeInitialization;
import ru.t1.pyrinov.tm.exception.field.IdEmptyException;
import ru.t1.pyrinov.tm.exception.field.UserIdEmptyException;
import ru.t1.pyrinov.tm.marker.UnitTest;
import ru.t1.pyrinov.tm.model.dto.SessionDTO;
import ru.t1.pyrinov.tm.model.dto.UserDTO;

import java.util.List;

import static ru.t1.pyrinov.tm.constant.SessionTestData.*;
import static ru.t1.pyrinov.tm.constant.UserTestData.USER_USER_LOGIN;
import static ru.t1.pyrinov.tm.constant.UserTestData.USER_USER_PASSWORD;

@TestMethodOrder(MethodOrderer.MethodName.class)
public final class SessionServiceTest extends AbstractSchemeInitialization implements UnitTest {

    private static @NotNull IUserDTOService USER_SERVICE;

    private static @NotNull ISessionDTOService SESSION_SERVICE;

    @NotNull
    private static String USER_ID = "";

    @BeforeAll
    public static void setUp() throws Exception {
        USER_SERVICE = context.getBean(IUserDTOService.class);
        SESSION_SERVICE = context.getBean(ISessionDTOService.class);

        @NotNull final UserDTO user = USER_SERVICE.create(USER_USER_LOGIN, USER_USER_PASSWORD);
        USER_ID = user.getId();
        USER_SESSION_LIST.forEach(session -> session.setUserId(USER_ID));
        //ADMIN_SESSION_LIST.forEach(session -> session.setUserId(UserTestData.USER_ADMIN.getId()));
    }

    @AfterAll
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_USER_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
    }

    @BeforeEach
    public void before() throws Exception {
        SESSION_SERVICE.add(USER_ID, USER_SESSION1);
        SESSION_SERVICE.add(USER_ID, USER_SESSION2);
    }

    @AfterEach
    public void after() throws Exception {
        SESSION_SERVICE.removeByUserId(USER_ID);
    }

    @Test
    public void setupOk() {
    }

    @Test
    public void addByUserId() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> SESSION_SERVICE.add(null, USER_SESSION3));
        Assertions.assertNotNull(SESSION_SERVICE.add(USER_ID, USER_SESSION3));
        @Nullable final SessionDTO session = SESSION_SERVICE.findById(USER_ID, USER_SESSION3.getId());
        Assertions.assertNotNull(session);
        Assertions.assertEquals(USER_SESSION3.getId(), session.getId());
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> SESSION_SERVICE.findAll(""));
        final List<SessionDTO> sessions = SESSION_SERVICE.findAll(USER_ID);
        Assertions.assertNotNull(sessions);
        Assertions.assertEquals(2, sessions.size());
        sessions.forEach(session -> Assertions.assertEquals(USER_ID, session.getUserId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> SESSION_SERVICE.existsById("", NON_EXISTING_SESSION_ID));
        Assertions.assertFalse(SESSION_SERVICE.existsById(USER_ID, ""));
        Assertions.assertFalse(SESSION_SERVICE.existsById(USER_ID, NON_EXISTING_SESSION_ID));
        Assertions.assertTrue(SESSION_SERVICE.existsById(USER_ID, USER_SESSION1.getId()));
    }

    @Test
    public void findByIdByUserId() throws Exception {
        Assertions.assertThrows(IdEmptyException.class, () -> SESSION_SERVICE.findById(USER_ID, ""));
        Assertions.assertThrows(UserIdEmptyException.class, () -> SESSION_SERVICE.existsById("", USER_SESSION1.getId()));
        Assertions.assertNull(SESSION_SERVICE.findById(USER_ID, NON_EXISTING_SESSION_ID));
        @Nullable final SessionDTO session = SESSION_SERVICE.findById(USER_ID, USER_SESSION1.getId());
        Assertions.assertNotNull(session);
        Assertions.assertEquals(USER_SESSION1.getId(), session.getId());
    }

    @Test
    public void clearByUserId() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> SESSION_SERVICE.removeByUserId(""));
        SESSION_SERVICE.removeByUserId(USER_ID);
        Assertions.assertEquals(0, SESSION_SERVICE.countByUserId(USER_ID));
    }

    @Test
    public void removeByUserId() throws Exception {
        SESSION_SERVICE.removeById(USER_ID, USER_SESSION2.getId());
        Assertions.assertNull(SESSION_SERVICE.findById(USER_ID, USER_SESSION2.getId()));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> SESSION_SERVICE.removeById(null, null));
        Assertions.assertThrows(UserIdEmptyException.class, () -> SESSION_SERVICE.removeById("", null));
        Assertions.assertThrows(IdEmptyException.class, () -> SESSION_SERVICE.removeById(USER_ID, null));
        Assertions.assertThrows(IdEmptyException.class, () -> SESSION_SERVICE.removeById(USER_ID, ""));
        SESSION_SERVICE.removeById(USER_ID, USER_SESSION2.getId());
        Assertions.assertNull(SESSION_SERVICE.findById(USER_ID, USER_SESSION2.getId()));
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> SESSION_SERVICE.countByUserId(""));
        Assertions.assertEquals(2, SESSION_SERVICE.countByUserId(USER_ID));
    }

}
