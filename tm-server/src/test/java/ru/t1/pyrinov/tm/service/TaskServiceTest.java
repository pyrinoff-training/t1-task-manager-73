package ru.t1.pyrinov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;
import ru.t1.pyrinov.tm.api.service.dto.IProjectDTOService;
import ru.t1.pyrinov.tm.api.service.dto.ITaskDTOService;
import ru.t1.pyrinov.tm.api.service.dto.IUserDTOService;
import ru.t1.pyrinov.tm.common.AbstractSchemeInitialization;
import ru.t1.pyrinov.tm.enumerated.SortOrder;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.exception.entity.StatusNotFoundException;
import ru.t1.pyrinov.tm.exception.entity.TaskNotFoundException;
import ru.t1.pyrinov.tm.exception.field.*;
import ru.t1.pyrinov.tm.marker.UnitTest;
import ru.t1.pyrinov.tm.model.dto.TaskDTO;
import ru.t1.pyrinov.tm.model.dto.UserDTO;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static ru.t1.pyrinov.tm.constant.ProjectTestData.*;
import static ru.t1.pyrinov.tm.constant.TaskTestData.*;
import static ru.t1.pyrinov.tm.constant.UserTestData.*;

@TestMethodOrder(MethodOrderer.MethodName.class)
public final class TaskServiceTest extends AbstractSchemeInitialization implements UnitTest {

    private static @NotNull IProjectDTOService PROJECT_SERVICE;

    private static @NotNull ITaskDTOService TASK_SERVICE;

    private static @NotNull IUserDTOService USER_SERVICE;

    @NotNull
    private static String USER_ID = "";

    @BeforeAll
    public static void setUp() throws Exception {
        USER_SERVICE = context.getBean(IUserDTOService.class);
        TASK_SERVICE = context.getBean(ITaskDTOService.class);
        PROJECT_SERVICE = context.getBean(IProjectDTOService.class);

        @NotNull final UserDTO user = USER_SERVICE.create(USER_USER_LOGIN, USER_USER_PASSWORD);
        USER_ID = user.getId();
        USER_TASK_LIST.forEach(task -> task.setUserId(USER_ID));
        USER_PROJECT_LIST.forEach(task -> task.setUserId(USER_ID));
    }

    @AfterAll
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_USER_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
    }

    @BeforeEach
    public void before() throws Exception {
        PROJECT_SERVICE.add(USER_ID, USER_PROJECT1);
        PROJECT_SERVICE.add(USER_ID, USER_PROJECT2);
        TASK_SERVICE.add(USER_ID, USER_TASK1);
        TASK_SERVICE.add(USER_ID, USER_TASK2);
    }

    @AfterEach
    public void after() throws Exception {
        TASK_SERVICE.removeAll(USER_ID);
        PROJECT_SERVICE.removeAll(USER_ID);
    }

    @Test
    public void setupOk() {
    }

    @Test
    public void addByUserId() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.add("", USER_TASK3));
        Assertions.assertNotNull(TASK_SERVICE.add(USER_ID, USER_TASK3));
        @Nullable final TaskDTO task = TASK_SERVICE.findById(USER_ID, USER_TASK3.getId());
        Assertions.assertNotNull(task);
        Assertions.assertEquals(USER_TASK3.getId(), task.getId());
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.findAll(""));
        final List<TaskDTO> tasks = TASK_SERVICE.findAll(USER_ID);
        Assertions.assertNotNull(tasks);
        Assertions.assertEquals(2, tasks.size());
        tasks.forEach(task -> Assertions.assertEquals(USER_ID, task.getUserId()));
    }

    @Test
    public void findAllSortByUserId() throws Exception {
        @Nullable SortOrder sort = null;
        Assertions.assertNotNull(TASK_SERVICE.findAll(USER_ID, sort));
        Assertions.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable SortOrder sortInner = null;
            TASK_SERVICE.findAll("", sortInner);
        });
        sort = SortOrder.BY_NAME;
        final List<TaskDTO> tasks = TASK_SERVICE.findAll(USER_ID, sort);
        Assertions.assertNotNull(tasks);
        tasks.forEach(task -> Assertions.assertEquals(USER_ID, task.getUserId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.existsById("", NON_EXISTING_TASK_ID));
        Assertions.assertFalse(TASK_SERVICE.existsById(USER_ID, ""));
        Assertions.assertFalse(TASK_SERVICE.existsById(USER_ID, NON_EXISTING_TASK_ID));
        Assertions.assertTrue(TASK_SERVICE.existsById(USER_ID, USER_TASK1.getId()));
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assertions.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.findById(USER_ID, ""));
        Assertions.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.existsById("", USER_TASK1.getId()));
        Assertions.assertNull(TASK_SERVICE.findById(USER_ID, NON_EXISTING_TASK_ID));
        @Nullable final TaskDTO task = TASK_SERVICE.findById(USER_ID, USER_TASK1.getId());
        Assertions.assertNotNull(task);
        Assertions.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void clearByUserId() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.removeAll(""));
        TASK_SERVICE.removeAll(USER_ID);
        Assertions.assertEquals(0, TASK_SERVICE.countByUserId(USER_ID));
    }

    @Test
    public void removeByUserId() throws Exception {
        TASK_SERVICE.removeById(USER_ID, USER_TASK2.getId());
        Assertions.assertNull(TASK_SERVICE.findById(USER_ID, USER_TASK2.getId()));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.removeById(null, null));
        Assertions.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.removeById("", null));
        Assertions.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.removeById(USER_ID, null));
        Assertions.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.removeById(USER_ID, ""));
        TASK_SERVICE.removeById(USER_ID, USER_TASK2.getId());
        Assertions.assertNull(TASK_SERVICE.findById(USER_ID, USER_TASK2.getId()));
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.countByUserId(""));
        Assertions.assertEquals(2, TASK_SERVICE.countByUserId(USER_ID));
    }

    @Test
    public void create() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.create(null, USER_TASK3.getName()));
        Assertions.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.create("", USER_TASK3.getName()));
        Assertions.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.create(USER_ID, null));
        Assertions.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.create(USER_ID, ""));
        @NotNull final TaskDTO task = TASK_SERVICE.create(USER_ID, USER_TASK3.getName());
        Assertions.assertNotNull(task);
        @Nullable final TaskDTO findTask = TASK_SERVICE.findById(USER_ID, task.getId());
        Assertions.assertNotNull(findTask);
        Assertions.assertEquals(task.getId(), findTask.getId());
        Assertions.assertEquals(USER_TASK3.getName(), task.getName());
        Assertions.assertEquals(USER_ID, task.getUserId());
    }

    @Test
    public void createWithDescription() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.create(null, USER_TASK3.getName(), USER_TASK3.getDescription()));
        Assertions.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.create("", USER_TASK3.getName(), USER_TASK3.getDescription()));
        Assertions.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.create(USER_ID, null, USER_TASK3.getDescription()));
        Assertions.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.create(USER_ID, "", USER_TASK3.getDescription()));
        Assertions.assertThrows(DescriptionEmptyException.class, () -> TASK_SERVICE.create(USER_ID, USER_TASK3.getName(), null));
        @NotNull final TaskDTO task = TASK_SERVICE.create(USER_ID, USER_TASK3.getName(), USER_TASK3.getDescription());
        Assertions.assertNotNull(task);
        @Nullable final TaskDTO findTask = TASK_SERVICE.findById(USER_ID, task.getId());
        Assertions.assertNotNull(findTask);
        Assertions.assertEquals(task.getId(), findTask.getId());
        Assertions.assertEquals(USER_TASK3.getName(), task.getName());
        Assertions.assertEquals(USER_TASK3.getDescription(), task.getDescription());
        Assertions.assertEquals(USER_ID, task.getUserId());
    }

    @Test
    public void updateById() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.updateById(null, USER_TASK1.getId(), USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assertions.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.updateById("", USER_TASK1.getId(), USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assertions.assertThrows(TaskIdEmptyException.class, () -> TASK_SERVICE.updateById(USER_ID, null, USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assertions.assertThrows(TaskIdEmptyException.class, () -> TASK_SERVICE.updateById(USER_ID, "", USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assertions.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.updateById(USER_ID, USER_TASK1.getId(), null, USER_TASK1.getDescription()));
        Assertions.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.updateById(USER_ID, USER_TASK1.getId(), "", USER_TASK1.getDescription()));
        Assertions.assertThrows(DescriptionEmptyException.class, () -> TASK_SERVICE.updateById(USER_ID, USER_TASK1.getId(), USER_TASK1.getName(), null));
        Assertions.assertThrows(TaskNotFoundException.class, () -> TASK_SERVICE.updateById(USER_ID, NON_EXISTING_TASK_ID, USER_TASK1.getName(), USER_TASK1.getDescription()));
        @NotNull final String name = USER_TASK1.getName() + NON_EXISTING_TASK_ID;
        @NotNull final String description = USER_TASK1.getDescription() + NON_EXISTING_TASK_ID;
        TASK_SERVICE.updateById(USER_ID, USER_TASK1.getId(), name, description);
        @Nullable final TaskDTO task = TASK_SERVICE.findById(USER_ID, USER_TASK1.getId());
        Assertions.assertNotNull(task);
        Assertions.assertEquals(name, task.getName());
        Assertions.assertEquals(description, task.getDescription());
    }

    @Test
    public void changeTaskStatusById() throws Exception {
        @NotNull final Status status = Status.COMPLETED;
        Assertions.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(null, USER_TASK1.getId(), status));
        Assertions.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById("", USER_TASK1.getId(), status));
        Assertions.assertThrows(TaskIdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(USER_ID, null, status));
        Assertions.assertThrows(TaskIdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(USER_ID, "", status));
        Assertions.assertThrows(StatusNotFoundException.class, () -> TASK_SERVICE.changeTaskStatusById(USER_ID, USER_TASK1.getId(), null));
        Assertions.assertThrows(TaskNotFoundException.class, () -> TASK_SERVICE.changeTaskStatusById(USER_ID, NON_EXISTING_TASK_ID, status));
        TASK_SERVICE.changeTaskStatusById(USER_ID, USER_TASK1.getId(), status);
        @Nullable final TaskDTO task = TASK_SERVICE.findById(USER_ID, USER_TASK1.getId());
        Assertions.assertNotNull(task);
        Assertions.assertEquals(status, task.getStatus());
    }

    @Test
    public void findAllByProjectId() throws Exception {
        Assertions.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable final Collection<TaskDTO> testCollection = TASK_SERVICE.findAllByProjectId(null, USER_PROJECT1.getId());
        });
        Assertions.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable final Collection<TaskDTO> testCollection = TASK_SERVICE.findAllByProjectId("", USER_PROJECT1.getId());
        });
        @NotNull final Collection<TaskDTO> emptyCollection = Collections.emptyList();
        Assertions.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.findAllByProjectId(USER_USER.getId(), null));
        Assertions.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.findAllByProjectId(USER_USER.getId(), ""));
        final List<TaskDTO> tasks = TASK_SERVICE.findAllByProjectId(USER_ID, USER_PROJECT1.getId());
        Assertions.assertNotNull(tasks);
        Assertions.assertEquals(2, tasks.size());
        tasks.forEach(task -> Assertions.assertEquals(USER_ID, task.getUserId()));
        tasks.forEach(task -> Assertions.assertEquals(USER_PROJECT1.getId(), task.getProjectId()));
    }

}
