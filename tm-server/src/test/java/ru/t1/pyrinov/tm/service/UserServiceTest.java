package ru.t1.pyrinov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;
import ru.t1.pyrinov.tm.api.service.dto.IUserDTOService;
import ru.t1.pyrinov.tm.common.AbstractSchemeInitialization;
import ru.t1.pyrinov.tm.enumerated.Role;
import ru.t1.pyrinov.tm.exception.entity.UserNotFoundException;
import ru.t1.pyrinov.tm.exception.field.*;
import ru.t1.pyrinov.tm.marker.UnitTest;
import ru.t1.pyrinov.tm.model.dto.UserDTO;

import static ru.t1.pyrinov.tm.constant.UserTestData.*;

@TestMethodOrder(MethodOrderer.MethodName.class)
public final class UserServiceTest extends AbstractSchemeInitialization implements UnitTest {

    private static @NotNull IUserDTOService USER_SERVICE;

    @BeforeEach
    public void before() throws Exception {
        deleteUsers();
        USER_SERVICE.add(USER_USER);
    }

    @AfterEach
    public void after() {
        deleteUsers();
    }

    @BeforeAll
    public static void setUp() {
        USER_SERVICE = context.getBean(IUserDTOService.class);
    }

    @AfterAll
    public static void tearDown() {
    }

    @SneakyThrows
    public void deleteUsers() {
        @Nullable UserDTO user = USER_SERVICE.findById(USER_USER.getId());
        if (user != null) USER_SERVICE.remove(user);
        user = USER_SERVICE.findByLogin(USER_USER_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
        user = USER_SERVICE.findById(USER_ADMIN.getId());
        if (user != null) USER_SERVICE.remove(user);
        user = USER_SERVICE.findByLogin(USER_ADMIN_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
    }

    @Test
    public void setupOk() {
    }

    @Test
    public void add() throws Exception {
        Assertions.assertNotNull(USER_SERVICE.add(USER_ADMIN));
        @Nullable final UserDTO user = USER_SERVICE.findById(USER_ADMIN.getId());
        Assertions.assertNotNull(user);
        Assertions.assertEquals(USER_ADMIN.getId(), user.getId());
    }

    @Test
    public void existsById() throws Exception {
        Assertions.assertFalse(USER_SERVICE.existsById(""));
        Assertions.assertFalse(USER_SERVICE.existsById(null));
        Assertions.assertFalse(USER_SERVICE.existsById(NON_EXISTING_USER_ID));
        Assertions.assertTrue(USER_SERVICE.existsById(USER_USER.getId()));
    }

    @Test
    public void findById() throws Exception {
        Assertions.assertThrows(IdEmptyException.class, () -> USER_SERVICE.findById(null));
        Assertions.assertThrows(IdEmptyException.class, () -> USER_SERVICE.findById(""));
        Assertions.assertNull(USER_SERVICE.findById(NON_EXISTING_USER_ID));
        @Nullable final UserDTO user = USER_SERVICE.findById(USER_USER.getId());
        Assertions.assertNotNull(user);
        Assertions.assertEquals(USER_USER.getId(), user.getId());
    }

    @Test
    public void removeById() throws Exception {
        Assertions.assertThrows(IdEmptyException.class, () -> USER_SERVICE.removeById(null));
        Assertions.assertThrows(IdEmptyException.class, () -> USER_SERVICE.removeById(""));
        USER_SERVICE.add(USER_ADMIN);
        Assertions.assertNotNull(USER_SERVICE.findById(USER_ADMIN.getId()));
        USER_SERVICE.removeById(USER_ADMIN.getId());
        Assertions.assertNull(USER_SERVICE.findById(USER_ADMIN.getId()));
    }

    @Test
    public void create() throws Exception {
        Assertions.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create(null, USER_ADMIN_PASSWORD));
        Assertions.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create("", USER_ADMIN_PASSWORD));
        Assertions.assertThrows(LoginExistsException.class, () -> USER_SERVICE.create(USER_USER_LOGIN, USER_ADMIN_PASSWORD));
        Assertions.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create(USER_ADMIN_LOGIN, null));
        Assertions.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create(USER_ADMIN_LOGIN, ""));
        @NotNull final UserDTO user = USER_SERVICE.create(USER_ADMIN_LOGIN, USER_ADMIN_PASSWORD);
        Assertions.assertNotNull(user);
        @Nullable final UserDTO findUser = USER_SERVICE.findById(user.getId());
        Assertions.assertNotNull(findUser);
        Assertions.assertEquals(user.getId(), findUser.getId());
        Assertions.assertEquals(USER_ADMIN_LOGIN, user.getLogin());
        Assertions.assertEquals(USER_ADMIN.getPasswordHash(), user.getPasswordHash());
    }

    @Test
    public void createWithEmail() throws Exception {
        Assertions.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create(null, USER_ADMIN_PASSWORD, USER_ADMIN_EMAIL));
        Assertions.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create("", USER_ADMIN_PASSWORD, USER_ADMIN_EMAIL));
        Assertions.assertThrows(LoginExistsException.class, () -> USER_SERVICE.create(USER_USER_LOGIN, USER_ADMIN_PASSWORD, USER_ADMIN_EMAIL));
        Assertions.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create(USER_ADMIN_LOGIN, null, USER_ADMIN_EMAIL));
        Assertions.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create(USER_ADMIN_LOGIN, "", USER_ADMIN_EMAIL));
        @NotNull final UserDTO user = USER_SERVICE.create(USER_ADMIN_LOGIN, USER_ADMIN_PASSWORD, USER_ADMIN_EMAIL);
        Assertions.assertNotNull(user);
        @Nullable final UserDTO findUser = USER_SERVICE.findById(user.getId());
        Assertions.assertNotNull(findUser);
        Assertions.assertEquals(user.getId(), findUser.getId());
        Assertions.assertEquals(USER_ADMIN_LOGIN, user.getLogin());
        Assertions.assertEquals(USER_ADMIN.getPasswordHash(), user.getPasswordHash());
        Assertions.assertEquals(USER_ADMIN_EMAIL, user.getEmail());
        Assertions.assertThrows(EmailExistsException.class, () -> USER_SERVICE.create(USER_USER2_LOGIN, USER_ADMIN_PASSWORD, USER_ADMIN_EMAIL));
    }

    @Test
    public void createWithRole() throws Exception {
        @NotNull final Role role = Role.ADMIN;
        Assertions.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create(null, USER_ADMIN_PASSWORD, role));
        Assertions.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create("", USER_ADMIN_PASSWORD, role));
        Assertions.assertThrows(LoginExistsException.class, () -> USER_SERVICE.create(USER_USER_LOGIN, USER_ADMIN_PASSWORD, role));
        Assertions.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create(USER_ADMIN_LOGIN, null, role));
        Assertions.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create(USER_ADMIN_LOGIN, "", role));
        Assertions.assertThrows(RoleEmptyException.class, () -> {
            USER_SERVICE.create(USER_ADMIN_LOGIN, USER_ADMIN_PASSWORD, USER_ADMIN_EMAIL, null);
        });
        @NotNull final UserDTO user = USER_SERVICE.create(USER_ADMIN_LOGIN, USER_ADMIN_PASSWORD, Role.ADMIN);
        Assertions.assertNotNull(user);
        @Nullable final UserDTO findUser = USER_SERVICE.findById(user.getId());
        Assertions.assertNotNull(findUser);
        Assertions.assertEquals(user.getId(), findUser.getId());
        Assertions.assertEquals(USER_ADMIN_LOGIN, user.getLogin());
        Assertions.assertEquals(USER_ADMIN.getPasswordHash(), user.getPasswordHash());
        Assertions.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    public void findByLogin() throws Exception {
        Assertions.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.findByLogin(null));
        Assertions.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.findByLogin(""));
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_USER_LOGIN);
        Assertions.assertNotNull(user);
        Assertions.assertEquals(USER_USER.getId(), user.getId());
    }

    @Test
    public void remove() throws Exception {
        USER_SERVICE.add(USER_ADMIN);
        Assertions.assertNotNull(USER_SERVICE.findById(USER_ADMIN.getId()));
        USER_SERVICE.remove(USER_ADMIN);
        Assertions.assertNull(USER_SERVICE.findById(USER_ADMIN.getId()));
    }

    @Test
    public void removeByLogin() throws Exception {
        Assertions.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.removeByLogin(null));
        Assertions.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.removeByLogin(""));
        Assertions.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.removeByLogin(NON_EXISTING_USER_ID));
        USER_SERVICE.add(USER_ADMIN);
        USER_SERVICE.removeByLogin(USER_ADMIN_LOGIN);
        Assertions.assertNull(USER_SERVICE.findById(USER_ADMIN.getId()));
    }

    @Test
    public void setPassword() throws Exception {
        Assertions.assertThrows(IdEmptyException.class, () -> USER_SERVICE.setPassword(null, USER_ADMIN_PASSWORD));
        Assertions.assertThrows(IdEmptyException.class, () -> USER_SERVICE.setPassword("", USER_ADMIN_PASSWORD));
        Assertions.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.setPassword(USER_USER.getId(), null));
        Assertions.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.setPassword(USER_USER.getId(), ""));
        Assertions.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.setPassword(NON_EXISTING_USER_ID, USER_ADMIN_PASSWORD));
        USER_SERVICE.setPassword(USER_USER.getId(), USER_ADMIN_PASSWORD);
        @Nullable final UserDTO user = USER_SERVICE.findById(USER_USER.getId());
        Assertions.assertNotNull(user);
        Assertions.assertEquals(USER_ADMIN.getPasswordHash(), user.getPasswordHash());
        USER_SERVICE.setPassword(USER_USER.getId(), USER_USER_PASSWORD);
    }

    @Test
    public void updateUser() throws Exception {
        @NotNull final String firstName = "User_first_name";
        @NotNull final String lastName = "User_last_name";
        @NotNull final String middleName = "User_middle_name";
        Assertions.assertThrows(IdEmptyException.class, () -> USER_SERVICE.updateUser(null, firstName, lastName, middleName));
        Assertions.assertThrows(IdEmptyException.class, () -> USER_SERVICE.updateUser("", firstName, lastName, middleName));
        USER_SERVICE.updateUser(USER_USER.getId(), firstName, lastName, middleName);
        @Nullable final UserDTO user = USER_SERVICE.findById(USER_USER.getId());
        Assertions.assertNotNull(user);
        Assertions.assertEquals(firstName, user.getFirstName());
        Assertions.assertEquals(lastName, user.getLastName());
        Assertions.assertEquals(middleName, user.getMiddleName());
    }

    @Test
    public void isLoginExists() throws Exception {
        Assertions.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.isLoginExist(null));
        Assertions.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.isLoginExist(""));
        Assertions.assertTrue(USER_SERVICE.isLoginExist(USER_USER_LOGIN));
    }

    @Test
    public void isEmailExists() throws Exception {
        Assertions.assertThrows(EmailEmptyException.class, () -> USER_SERVICE.isEmailExist(null));
        Assertions.assertThrows(EmailEmptyException.class, () -> USER_SERVICE.isEmailExist(""));
        Assertions.assertTrue(USER_SERVICE.isEmailExist(USER_USER_EMAIL));
    }

    @Test
    public void lockUserByLogin() throws Exception {
        Assertions.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.lockUserByLogin(null));
        Assertions.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.lockUserByLogin(""));
        Assertions.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.lockUserByLogin(NON_EXISTING_USER_ID));
        USER_SERVICE.lockUserByLogin(USER_USER_LOGIN);
        @Nullable final UserDTO user = USER_SERVICE.findById(USER_USER.getId());
        Assertions.assertNotNull(user);
        Assertions.assertTrue(user.getLocked());
    }

    @Test
    public void unlockUserByLogin() throws Exception {
        Assertions.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.unlockUserByLogin(null));
        Assertions.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.unlockUserByLogin(""));
        Assertions.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.unlockUserByLogin(NON_EXISTING_USER_ID));
        USER_SERVICE.lockUserByLogin(USER_USER_LOGIN);
        USER_SERVICE.unlockUserByLogin(USER_USER_LOGIN);
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_USER_LOGIN);
        Assertions.assertNotNull(user);
        Assertions.assertFalse(user.getLocked());
    }

}
