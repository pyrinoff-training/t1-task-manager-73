package ru.t1.pyrinov.tm.util;

import org.junit.jupiter.api.Assertions;

import java.util.List;

public interface CustomAssertionUtil {

    static void listEqualsWithoutOrder(List listOne, List listTwo) {
        Assertions.assertTrue(listOne.size() == listTwo.size() && listOne.containsAll(listTwo) && listTwo.containsAll(listOne));
    }

}
