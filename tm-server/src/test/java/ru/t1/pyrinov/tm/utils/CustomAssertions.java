package ru.t1.pyrinov.tm.utils;

import org.junit.jupiter.api.Assertions;

import java.util.List;

public interface CustomAssertions {

    static void listEqualsWithoutOrder(List listOne, List listTwo) {
        Assertions.assertTrue(listOne.size() == listTwo.size() && listOne.containsAll(listTwo) && listTwo.containsAll(listOne));
    }

}
