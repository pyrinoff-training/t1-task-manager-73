package ru.t1.pyrinov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.t1.pyrinov.tm.model.Result;
import ru.t1.pyrinov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/api/auth")
public interface IAuthEndpoint {

    @WebMethod
    @PostMapping(
            value = "/login",
            produces = "application/json"
    )
    @NotNull Result login(@WebParam(name = "username") @RequestParam("username") @NotNull String username,
                          @WebParam(name = "password") @RequestParam("password") @NotNull String password);

    @WebMethod
    @GetMapping(
            value = "/profile",
            produces = "application/json"
    )
    @NotNull User profile();

    @WebMethod
    @PostMapping("/logout")
    @Nullable Result logout();

}
