package ru.t1.pyrinov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.t1.pyrinov.tm.model.CustomSecurityUser;
import ru.t1.pyrinov.tm.model.Project;

import java.util.List;

public interface IProjectRestEndpoint {


    @GetMapping(value = "/projects", produces = "application/json")
    @Nullable ResponseEntity<List<Project>> getProjects(@AuthenticationPrincipal final CustomSecurityUser user);

    @GetMapping(value = "/project/{id}", produces = "application/json")
    @Nullable ResponseEntity<Project> getProject(
            @AuthenticationPrincipal final CustomSecurityUser user, @PathVariable("id") String id);

    @PostMapping(value = "/project", produces = "application/json")
    @Nullable ResponseEntity<Project> addProject(
            @AuthenticationPrincipal final CustomSecurityUser user, @RequestBody @Nullable Project project);

    @PutMapping(value = "/project/{id}", produces = "application/json")
    ResponseEntity<Project> editProject(
            @AuthenticationPrincipal final CustomSecurityUser user,
            @PathVariable("id") String id, @RequestBody Project project);

    @DeleteMapping("/project/{id}")
    @NotNull ResponseEntity<String> deleteProject(
            @AuthenticationPrincipal final CustomSecurityUser user, @PathVariable("id") String id);


}
