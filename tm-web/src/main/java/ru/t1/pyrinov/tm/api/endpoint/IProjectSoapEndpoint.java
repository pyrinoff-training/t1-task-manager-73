package ru.t1.pyrinov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.pyrinov.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectSoapEndpoint {

    @WebMethod
    @NotNull List<Project> getProjects();

    @WebMethod
    @Nullable Project getProject(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @Nullable Project addProject(
            @WebParam(name = "project", partName = "project")
            @RequestBody @Nullable Project project
    );

    @WebMethod
    Project editProject(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id,
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project
    );

    @WebMethod
    void deleteProject(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

}
