package ru.t1.pyrinov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.t1.pyrinov.tm.model.CustomSecurityUser;
import ru.t1.pyrinov.tm.model.Task;

import java.util.List;

public interface ITaskRestEndpoint {

    @GetMapping(value = "/projects", produces = "application/json")
    @Nullable ResponseEntity<List<Task>> getTasks(@AuthenticationPrincipal final CustomSecurityUser user);

    @GetMapping(value = "/project/{id}", produces = "application/json")
    @Nullable ResponseEntity<Task> getTask(
            @AuthenticationPrincipal final CustomSecurityUser user, @PathVariable("id") String id);

    @PostMapping(value = "/project", produces = "application/json")
    @Nullable ResponseEntity<Task> addTask(
            @AuthenticationPrincipal final CustomSecurityUser user, @RequestBody @Nullable Task project);

    @PutMapping(value = "/project/{id}", produces = "application/json")
    ResponseEntity<Task> editTask(
            @AuthenticationPrincipal final CustomSecurityUser user,
            @PathVariable("id") String id, @RequestBody Task project);

    @DeleteMapping("/project/{id}")
    @NotNull ResponseEntity<String> deleteTask(
            @AuthenticationPrincipal final CustomSecurityUser user, @PathVariable("id") String id);


}
