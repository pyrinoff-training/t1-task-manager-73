package ru.t1.pyrinov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.pyrinov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskSoapEndpoint {

    @WebMethod
    @NotNull List<Task> getTasks();

    @WebMethod
    @Nullable Task getTask(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @Nullable Task addTask(
            @WebParam(name = "project", partName = "project")
            @RequestBody @Nullable Task task
    );

    @WebMethod
    Task editTask(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id,
            @WebParam(name = "project", partName = "project")
            @RequestBody Task task
    );

    @WebMethod
    void deleteTask(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

}
