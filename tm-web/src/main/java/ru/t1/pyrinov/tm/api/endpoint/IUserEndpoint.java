package ru.t1.pyrinov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.t1.pyrinov.tm.model.User;

import java.util.List;

public interface IUserEndpoint {

    @GetMapping(value = "/users", produces = "application/json")
    @NotNull List<User> getUsers();

    @GetMapping(value = "/user/{id}", produces = "application/json")
    @Nullable User getUser(@PathVariable("id") String id);

    @PostMapping(value = "/user", produces = "application/json")
    @Nullable ResponseEntity<User> addUser(@RequestBody @Nullable User user);

    @PutMapping(value = "/user/{id}", produces = "application/json")
    @NotNull User editUser(@PathVariable("id") String id, @RequestBody User user);

    @DeleteMapping("/user/{id}")
    void deleteUser(@PathVariable("id") String id);

}
