package ru.t1.pyrinov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @NotNull
    Project create(String userId, String name, String description);

    @Nullable
    Project create(String userId, String name);

    @Nullable
    Project updateById(String userId, String id, String name, String description);

    @Nullable
    Project changeProjectStatusById(String userId, String id, Status status);

    @NotNull List<Project> findAll();

    @Nullable Project save(@NotNull final Project project);

    @Nullable Project findById(@Nullable String id);

    long count(@Nullable String userId);

    long count();

    @NotNull List<Project> findByUserId(@Nullable String userId);

    @NotNull Project addByUserId(@Nullable String userId, @Nullable Project project);

    @NotNull Collection<Project> addAllByUserId(@Nullable String userId, @Nullable Collection<Project> project);

    @NotNull Project saveByUserId(@Nullable String userId, @Nullable Project project);

    long countByUserId(@NotNull String userId);

    boolean existsByUserId(@Nullable String userId, @Nullable String id);

    void deleteAllByUserId(@Nullable String userId);

    @Nullable Project findById(@Nullable String userId, @NotNull String id);

    @Nullable String getNameById(@Nullable String userId, @NotNull String id);

    @Nullable List<Project> getProjects(@Nullable final String userId);

    @Nullable Project getProject(@Nullable final String userId, @Nullable final String id);

    @Nullable Project editProject(
            @Nullable final String userId, @Nullable final String id, @Nullable final Project project);

    void deleteProject(@Nullable final String userId, @Nullable final String id);

}
