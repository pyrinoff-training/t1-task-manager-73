package ru.t1.pyrinov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends IDatabaseDataProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

    @NotNull
    String getAdminLogin();

    @NotNull
    String getAdminPassword();

    @NotNull
    String getJmsUrl();

}
