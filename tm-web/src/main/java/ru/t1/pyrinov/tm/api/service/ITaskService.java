package ru.t1.pyrinov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    @NotNull
    Task create(String userId, String name, String description);

    @Nullable
    Task create(String userId, String name);

    @Nullable
    Task updateById(String userId, String id, String name, String description);

    @Nullable
    Task changeTaskStatusById(String userId, String id, Status status);

    @NotNull List<Task> findAll();

    @Nullable Task save(@NotNull final Task project);

    void deleteById(@NotNull final String id);

    @Nullable Task findById(@NotNull String id);

    long count();

    @NotNull List<Task> findByUserId(@Nullable String userId);

    @NotNull Task addByUserId(@Nullable String userId, @Nullable Task task);

    @NotNull Collection<Task> addAllByUserId(@Nullable String userId, @Nullable Collection<Task> task);

    @NotNull Task saveByUserId(@Nullable String userId, @Nullable Task task);

    long countByUserId(@NotNull String userId);

    boolean existsByUserId(@Nullable String userId, @Nullable String id);

    void deleteByUserId(@Nullable String userId, @Nullable String id);

    @Nullable Task findById(@Nullable String userId, @NotNull String id);

    void deleteAllByUserId(@Nullable String userId);

    @Nullable List<Task> findAllByProjectId(String userId, String taskId);

    @Nullable List<Task> getTasks(@Nullable final String userId);

    @Nullable Task getTask(@Nullable final String userId, @Nullable final String id);

    @Nullable Task editTask(@Nullable final String userId, @Nullable final String id, @Nullable final Task project);

    void deleteTask(@Nullable final String userId, @Nullable final String id);

}
