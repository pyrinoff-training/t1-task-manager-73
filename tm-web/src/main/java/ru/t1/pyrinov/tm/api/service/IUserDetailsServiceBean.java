package ru.t1.pyrinov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.pyrinov.tm.enumerated.RoleType;
import ru.t1.pyrinov.tm.model.User;

public interface IUserDetailsServiceBean extends UserDetailsService {

    @Nullable User initUser(
            @NotNull String login,
            @NotNull String password,
            @NotNull RoleType role);

    @Override
    @Transactional
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;

}
