package ru.t1.pyrinov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.model.User;

import java.util.List;

public interface IUserService {

    @NotNull List<User> findAll();

    @Nullable User save(@NotNull final User user);

    void delete(@NotNull final String id);

    @Nullable User find(@Nullable String id);

    @Nullable User findByLogin(@Nullable String login);

    long count();

}
