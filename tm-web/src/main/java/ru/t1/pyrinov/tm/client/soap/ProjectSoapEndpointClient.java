package ru.t1.pyrinov.tm.client.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.api.endpoint.IProjectSoapEndpoint;

import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

@WebService(name = "ProjectSoapEndpoint")
public interface ProjectSoapEndpointClient {

    static IProjectSoapEndpoint getInstance(@NotNull final String baseURL, @Nullable Map<String, List<String>> HEADERS)
            throws MalformedURLException {
        @NotNull final String wsdl = baseURL + "/ws/ProjectEndpoint?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "ProjectSoapEndpointService";
        @NotNull final String ns = "http://endpoint.tm.pyrinov.t1.ru/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final IProjectSoapEndpoint result = Service.create(url, name).getPort(IProjectSoapEndpoint.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        if (HEADERS != null) bindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, HEADERS);
        return result;
    }

}
