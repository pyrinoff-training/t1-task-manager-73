package ru.t1.pyrinov.tm.configuration;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.t1.pyrinov.tm.api.endpoint.IProjectSoapEndpoint;
import ru.t1.pyrinov.tm.endpoint.AuthEndpoint;
import ru.t1.pyrinov.tm.endpoint.TaskSoapEndpoint;

//Contract last
@Configuration
public class SoapApplicationConfiguration {

    @Bean
    public EndpointImpl projectEndpointRegistry(final IProjectSoapEndpoint projectSoapEndpoint, Bus bus) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(bus, projectSoapEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public EndpointImpl taskEndpointRegistry(final TaskSoapEndpoint taskEndpoint, Bus bus) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(bus, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    public EndpointImpl authEndpointRegistry(final AuthEndpoint authSoapEndpoint, Bus bus) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(bus, authSoapEndpoint);
        endpoint.publish("/AuthEndpoint");
        return endpoint;
    }

}
