package ru.t1.pyrinov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.pyrinov.tm.api.service.IProjectService;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.model.CustomSecurityUser;
import ru.t1.pyrinov.tm.model.Project;

@Controller
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @RequestMapping(value = "/projects")
    public ModelAndView list(@AuthenticationPrincipal final CustomSecurityUser user) {
        return new ModelAndView("project-list", "projects", projectService.findByUserId(user.getUserId()));
    }

    @RequestMapping("/project/create")
    public String create(@AuthenticationPrincipal final CustomSecurityUser user) {
        projectService.addByUserId(user.getUserId(), new Project("GeneratedProject", Status.NOT_STARTED, "Some description"));
        return "redirect:/projects";
    }

    @RequestMapping("/project/delete/{id}")
    public String delete(
            @AuthenticationPrincipal final CustomSecurityUser user,
            @PathVariable("id") String id) {
        projectService.deleteProject(user.getUserId(), id);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView editGet(
            @AuthenticationPrincipal final CustomSecurityUser user,
            @PathVariable("id") String id) {
        @Nullable String userId = user.getUserId();
        if (userId == null) {
            @NotNull final ModelAndView modelAndView = new ModelAndView();
            modelAndView.setStatus(HttpStatus.NOT_FOUND);
            return modelAndView;
        }
        @Nullable final Project project = projectService.findById(user.getUserId(), id);
        if (project == null) {
            @NotNull final ModelAndView modelAndView = new ModelAndView();
            modelAndView.setStatus(HttpStatus.NOT_FOUND);
            return modelAndView;
        }
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

    @PostMapping("/project/edit/{id}")
    public String editPost(
            @AuthenticationPrincipal final CustomSecurityUser user,
            @ModelAttribute("project") Project project) {
        projectService.saveByUserId(user.getUserId(), project);
        return "redirect:/projects";
    }

}
