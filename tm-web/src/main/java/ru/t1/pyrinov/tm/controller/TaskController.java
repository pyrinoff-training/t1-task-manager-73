package ru.t1.pyrinov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.pyrinov.tm.api.service.IProjectService;
import ru.t1.pyrinov.tm.api.service.ITaskService;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.model.CustomSecurityUser;
import ru.t1.pyrinov.tm.model.Project;
import ru.t1.pyrinov.tm.model.Task;

import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @RequestMapping(value = "/tasks")
    public ModelAndView list(@AuthenticationPrincipal final CustomSecurityUser user) {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", taskService.findByUserId(user.getUserId()));
        modelAndView.addObject("projectService", projectService);
        return modelAndView;
    }

    @RequestMapping("/task/create")
    public String create(@AuthenticationPrincipal final CustomSecurityUser user) {
        taskService.addByUserId(user.getUserId(), new Task("GeneratedTask", Status.NOT_STARTED, "Some description"));
        return "redirect:/tasks";
    }

    @RequestMapping("/task/delete/{id}")
    public String delete(
            @AuthenticationPrincipal final CustomSecurityUser user,
            @PathVariable("id") String id) {
        taskService.deleteByUserId(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView editGet(
            @AuthenticationPrincipal final CustomSecurityUser user,
            @PathVariable("id") String id) {
        @Nullable final Task task = taskService.findById(user.getUserId(), id);
        if (task == null) {
            @NotNull final ModelAndView modelAndView = new ModelAndView();
            modelAndView.setStatus(HttpStatus.NOT_FOUND);
            return modelAndView;
        }
        @NotNull final Collection<Project> projectList = projectService.findByUserId(user.getUserId());
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("statuses", Status.values());
        modelAndView.addObject("projects", projectList);
        return modelAndView;
    }

    @PostMapping("/task/edit/{id}")
    public String editPost(
            @AuthenticationPrincipal final CustomSecurityUser user,
            @ModelAttribute("task") Task task) {
        taskService.saveByUserId(user.getUserId(), task);
        return "redirect:/tasks";
    }

}
