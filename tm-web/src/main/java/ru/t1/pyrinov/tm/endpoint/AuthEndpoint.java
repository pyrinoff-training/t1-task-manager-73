package ru.t1.pyrinov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.t1.pyrinov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.pyrinov.tm.api.service.IUserService;
import ru.t1.pyrinov.tm.model.Result;
import ru.t1.pyrinov.tm.model.User;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/auth")
@WebService(endpointInterface = "ru.t1.pyrinov.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpoint implements IAuthEndpoint {

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUserService userService;

    @Override
    @WebMethod
    @PostMapping(value = "/login", produces = "application/json")
    public @NotNull Result login(
            @WebParam(name = "username") @RequestParam("username") @NotNull String username,
            @WebParam(name = "password") @RequestParam("password") @NotNull String password) {
        try {
            @NotNull
            final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            @NotNull final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (@NotNull final Exception e) {
            return new Result(e);
        }
    }

    @Override
    @WebMethod
    @GetMapping(value = "/profile", produces = "application/json")
    public @NotNull User profile() {
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        @NotNull final Authentication authentication = securityContext.getAuthentication();
        @NotNull final String username = authentication.getName();
        return userService.findByLogin(username);
    }

    @Override
    @WebMethod
    @PostMapping("/logout")
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}
