package ru.t1.pyrinov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.t1.pyrinov.tm.api.endpoint.IProjectRestEndpoint;
import ru.t1.pyrinov.tm.api.service.IProjectService;
import ru.t1.pyrinov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.pyrinov.tm.exception.field.IdEmptyException;
import ru.t1.pyrinov.tm.exception.field.UserIdEmptyException;
import ru.t1.pyrinov.tm.exception.user.AccessDeniedException;
import ru.t1.pyrinov.tm.model.CustomSecurityUser;
import ru.t1.pyrinov.tm.model.Project;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @GetMapping(
            value = "/projects",
            produces = "application/json"
    )
    public @Nullable ResponseEntity<List<Project>> getProjects(@AuthenticationPrincipal final CustomSecurityUser user) {
        @Nullable String userId = user.getUserId();
        if (userId == null) return new ResponseEntity<>(null, UNAUTHORIZED);
        return new ResponseEntity<>(projectService.getProjects(user.getUserId()), OK);
    }

    @Override
    @GetMapping(
            value = "/project/{id}",
            produces = "application/json"
    )
    public @Nullable ResponseEntity<Project> getProject(
            @AuthenticationPrincipal final CustomSecurityUser user,
            @PathVariable("id") String id
    ) {
        @Nullable String userId = user.getUserId();
        if (userId == null) return new ResponseEntity<>(null, UNAUTHORIZED);
        try {
            return new ResponseEntity<>(projectService.getProject(user.getUserId(), id), OK);
        } catch (@NotNull final ProjectNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, "Unable to find project with this id!");
        }
    }

    @Override
    @PostMapping(
            value = "/project",
            produces = "application/json"
    )
    public @Nullable ResponseEntity<Project> addProject(
            @AuthenticationPrincipal final CustomSecurityUser user,
            @RequestBody @Nullable Project project
    ) {
        @Nullable String userId = user.getUserId();
        if (userId == null) return new ResponseEntity<>(null, UNAUTHORIZED);
        if (project == null) return new ResponseEntity<>(null, BAD_REQUEST);
        return new ResponseEntity<>(projectService.save(project), CREATED);
    }

    @Override
    @PutMapping(
            value = "/project/{id}",
            produces = "application/json"
    )
    public ResponseEntity<Project> editProject(
            @AuthenticationPrincipal final CustomSecurityUser user,
            @PathVariable("id") String id,
            @RequestBody final Project project
    ) {
        @Nullable String userId = user.getUserId();
        if (userId == null) return new ResponseEntity<>(null, UNAUTHORIZED);
        if (project == null) return new ResponseEntity<>(null, BAD_REQUEST);
        try {
            return new ResponseEntity<>(projectService.editProject(user.getUserId(), id, project), OK);
        } catch (@NotNull final ProjectNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, "Unable to find project with this id!");
        } catch (@NotNull final IllegalArgumentException e) {
            throw new ResponseStatusException(BAD_REQUEST);
        } catch (@NotNull final AccessDeniedException e) {
            throw new ResponseStatusException(FORBIDDEN);
        }
    }

    @Override
    @DeleteMapping("/project/{id}")
    public @NotNull ResponseEntity<String> deleteProject(
            @AuthenticationPrincipal final CustomSecurityUser user,
            @PathVariable("id") String id
    ) {
        @Nullable String userId = user.getUserId();
        if (userId == null) return new ResponseEntity<>(null, UNAUTHORIZED);
        try {
            projectService.deleteProject(userId, id);
        } catch (@NotNull final ProjectNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, "Unable to find project with this id!");
        } catch (@NotNull final IllegalArgumentException | IdEmptyException e) {
            throw new ResponseStatusException(BAD_REQUEST);
        } catch (@NotNull final AccessDeniedException | UserIdEmptyException e) {
            throw new ResponseStatusException(FORBIDDEN);
        } catch (@NotNull final Exception e) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "Error");
        }
        throw new ResponseStatusException(OK);
    }

}
