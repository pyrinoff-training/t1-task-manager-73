package ru.t1.pyrinov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;
import ru.t1.pyrinov.tm.api.endpoint.IProjectSoapEndpoint;
import ru.t1.pyrinov.tm.api.service.IProjectService;
import ru.t1.pyrinov.tm.model.Project;
import ru.t1.pyrinov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Component
@WebService(endpointInterface = "ru.t1.pyrinov.tm.api.endpoint.IProjectSoapEndpoint")
public class ProjectSoapEndpoint implements IProjectSoapEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @WebMethod
    public @NotNull List<Project> getProjects() {
        return projectService.findAll();
    }

    @Override
    @WebMethod
    public @Nullable Project getProject(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        Project project = projectService.findById(id);
        if (project == null) throw new ResponseStatusException(NOT_FOUND, "Unable to find project with this id!");
        return project;
    }

    @Override
    @WebMethod
    public @Nullable Project addProject(
            @WebParam(name = "project", partName = "project")
            @RequestBody @Nullable Project project
    ) {
        if (project == null) return null;
        return projectService.save(project);
    }

    @Override
    @WebMethod
    public Project editProject(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id,
            @WebParam(name = "project", partName = "project")
            @RequestBody final Project project
    ) {
        @Nullable final Project projectById = projectService.findById(id);
        if (projectById == null) throw new ResponseStatusException(NOT_FOUND, "Unable to find project with this id!");
        if (!projectById.getId().equals(id)) throw new IllegalArgumentException();
        return projectService.save(project);
    }

    @Override
    @WebMethod
    public void deleteProject(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        @Nullable final Project projectById = projectService.findById(id);
        if (projectById == null) throw new ResponseStatusException(NOT_FOUND, "Unable to find project with this id!");
        try {
            projectService.deleteProject(UserUtil.getUserId(), id);
        } catch (@NotNull final Exception e) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "Error");
        }
    }

}
