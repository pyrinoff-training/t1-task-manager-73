package ru.t1.pyrinov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.t1.pyrinov.tm.api.endpoint.ITaskRestEndpoint;
import ru.t1.pyrinov.tm.api.service.ITaskService;
import ru.t1.pyrinov.tm.exception.entity.TaskNotFoundException;
import ru.t1.pyrinov.tm.exception.field.IdEmptyException;
import ru.t1.pyrinov.tm.exception.field.UserIdEmptyException;
import ru.t1.pyrinov.tm.exception.user.AccessDeniedException;
import ru.t1.pyrinov.tm.model.CustomSecurityUser;
import ru.t1.pyrinov.tm.model.Task;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @GetMapping(
            value = "/tasks",
            produces = "application/json"
    )
    public @Nullable ResponseEntity<List<Task>> getTasks(@AuthenticationPrincipal final CustomSecurityUser user) {
        @Nullable String userId = user.getUserId();
        if (userId == null) return new ResponseEntity<>(null, UNAUTHORIZED);
        return new ResponseEntity<>(taskService.getTasks(user.getUserId()), OK);
    }

    @Override
    @GetMapping(
            value = "/task/{id}",
            produces = "application/json"
    )
    public @Nullable ResponseEntity<Task> getTask(
            @AuthenticationPrincipal final CustomSecurityUser user,
            @PathVariable("id") String id
    ) {
        @Nullable String userId = user.getUserId();
        if (userId == null) return new ResponseEntity<>(null, UNAUTHORIZED);
        try {
            return new ResponseEntity<>(taskService.getTask(user.getUserId(), id), OK);
        } catch (@NotNull final TaskNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, "Unable to find task with this id!");
        }
    }

    @Override
    @PostMapping(
            value = "/task",
            produces = "application/json"
    )
    public @Nullable ResponseEntity<Task> addTask(
            @AuthenticationPrincipal final CustomSecurityUser user,
            @RequestBody @Nullable Task task
    ) {
        @Nullable String userId = user.getUserId();
        if (userId == null) return new ResponseEntity<>(null, UNAUTHORIZED);
        if (task == null) return new ResponseEntity<>(null, BAD_REQUEST);
        return new ResponseEntity<>(taskService.save(task), CREATED);
    }

    @Override
    @PutMapping(
            value = "/task/{id}",
            produces = "application/json"
    )
    public ResponseEntity<Task> editTask(
            @AuthenticationPrincipal final CustomSecurityUser user,
            @PathVariable("id") String id,
            @RequestBody final Task task
    ) {
        @Nullable String userId = user.getUserId();
        if (userId == null) return new ResponseEntity<>(null, UNAUTHORIZED);
        if (task == null) return new ResponseEntity<>(null, BAD_REQUEST);
        try {
            return new ResponseEntity<>(taskService.editTask(user.getUserId(), id, task), OK);
        } catch (@NotNull final TaskNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, "Unable to find task with this id!");
        } catch (@NotNull final IllegalArgumentException e) {
            throw new ResponseStatusException(BAD_REQUEST);
        } catch (@NotNull final AccessDeniedException e) {
            throw new ResponseStatusException(FORBIDDEN);
        }
    }

    @Override
    @DeleteMapping("/task/{id}")
    public @NotNull ResponseEntity<String> deleteTask(
            @AuthenticationPrincipal final CustomSecurityUser user,
            @PathVariable("id") String id
    ) {
        @Nullable String userId = user.getUserId();
        if (userId == null) return new ResponseEntity<>(null, UNAUTHORIZED);
        try {
            taskService.deleteTask(userId, id);
        } catch (@NotNull final TaskNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, "Unable to find task with this id!");
        } catch (@NotNull final IllegalArgumentException | IdEmptyException e) {
            throw new ResponseStatusException(BAD_REQUEST);
        } catch (@NotNull final AccessDeniedException | UserIdEmptyException e) {
            throw new ResponseStatusException(FORBIDDEN);
        } catch (@NotNull final Exception e) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "Error: " + e.getMessage());
        }
        throw new ResponseStatusException(OK);
    }

}
