package ru.t1.pyrinov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;
import ru.t1.pyrinov.tm.api.endpoint.ITaskSoapEndpoint;
import ru.t1.pyrinov.tm.api.service.ITaskService;
import ru.t1.pyrinov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Component
@WebService(endpointInterface = "ru.t1.pyrinov.tm.api.endpoint.ITaskSoapEndpoint")
public class TaskSoapEndpoint implements ITaskSoapEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @WebMethod
    public @NotNull List<Task> getTasks() {
        return taskService.findAll();
    }

    @Override
    @WebMethod
    public @Nullable Task getTask(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        Task task = taskService.findById(id);
        if (task == null) throw new ResponseStatusException(NOT_FOUND, "Unable to find task with this id!");
        return task;
    }

    @Override
    @WebMethod
    public @Nullable Task addTask(
            @WebParam(name = "project", partName = "project")
            @RequestBody @Nullable Task task
    ) {
        if (task == null) return null;
        return taskService.save(task);
    }

    @Override
    @WebMethod
    public Task editTask(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id,
            @WebParam(name = "project", partName = "project")
            @RequestBody final Task task
    ) {
        @Nullable final Task taskById = taskService.findById(id);
        if (taskById == null) throw new ResponseStatusException(NOT_FOUND, "Unable to find task with this id!");
        if (!taskById.getId().equals(id)) throw new IllegalArgumentException();
        return taskService.save(task);
    }

    @Override
    @WebMethod
    public void deleteTask(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        @Nullable final Task taskById = taskService.findById(id);
        if (taskById == null) throw new ResponseStatusException(NOT_FOUND, "Unable to find task with this id!");
        try {
            taskService.deleteById(id);
        } catch (@NotNull final Exception e) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "Error");
        }
    }

}
