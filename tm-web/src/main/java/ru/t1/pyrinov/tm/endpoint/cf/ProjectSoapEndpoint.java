package ru.t1.pyrinov.tm.endpoint.cf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.pyrinov.tm.api.service.IProjectService;
import ru.t1.pyrinov.tm.model.soap.*;
import ru.t1.pyrinov.tm.util.UserUtil;

//@Endpoint  //Contract First
public class ProjectSoapEndpoint {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    public final static String NAMESPACE = "http://pyrinov.t1.ru/tm/model/soap";

    @Autowired
    private IProjectService projectService;

    @ResponsePayload
    @PayloadRoot(
            localPart = "projectsGetRequest",
            namespace = NAMESPACE
    )
    public ProjectsGetResponse findAll(
            @RequestPayload final ProjectsGetRequest request) throws Exception {
        return new ProjectsGetResponse(projectService.findByUserId(UserUtil.getUserId()));
    }

    @ResponsePayload
    @PayloadRoot(
            localPart = "projectAddRequest",
            namespace = NAMESPACE
    )
    public ProjectAddResponse add(
            @RequestPayload final ProjectAddRequest request) throws Exception {
        return new ProjectAddResponse(projectService.addByUserId(UserUtil.getUserId(), request.getProject()));
    }

    @ResponsePayload
    @PayloadRoot(
            localPart = "projectSaveRequest",
            namespace = NAMESPACE
    )
    public ProjectSaveResponse save(
            @RequestPayload final ProjectSaveRequest request) throws Exception {
        return new ProjectSaveResponse(projectService.saveByUserId(UserUtil.getUserId(), request.getProject()));
    }

    @ResponsePayload
    @PayloadRoot(
            localPart = "projectGetRequest",
            namespace = NAMESPACE
    )
    public ProjectGetResponse findById(
            @RequestPayload final ProjectGetRequest request) throws Exception {
        return new ProjectGetResponse(projectService.findById(UserUtil.getUserId(), request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(
            localPart = "projectExistsByIdRequest",
            namespace = NAMESPACE
    )
    public ProjectExistsByIdResponse existsById(
            @RequestPayload final ProjectExistsByIdRequest request) {
        return new ProjectExistsByIdResponse(projectService.existsByUserId(UserUtil.getUserId(), request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(
            localPart = "projectCountRequest",
            namespace = NAMESPACE
    )
    public ProjectCountResponse count(
            @RequestPayload final ProjectCountRequest request) throws Exception {
        return new ProjectCountResponse(projectService.countByUserId(UserUtil.getUserId()));
    }

    @ResponsePayload
    @PayloadRoot(
            localPart = "projectDeleteByIdRequest",
            namespace = NAMESPACE
    )
    public ProjectDeleteByIdResponse deleteById(
            @RequestPayload final ProjectDeleteByIdRequest request) throws Exception {
        projectService.deleteProject(UserUtil.getUserId(), request.getId());
        return new ProjectDeleteByIdResponse();
    }

}
