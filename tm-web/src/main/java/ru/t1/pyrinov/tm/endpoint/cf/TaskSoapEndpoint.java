package ru.t1.pyrinov.tm.endpoint.cf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.pyrinov.tm.api.service.ITaskService;
import ru.t1.pyrinov.tm.model.soap.*;
import ru.t1.pyrinov.tm.util.UserUtil;

//@Endpoint   //Contract First
public class TaskSoapEndpoint {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    public final static String NAMESPACE = "http://pyrinov.t1.ru/tm/model/soap";

    @Autowired
    private ITaskService taskService;

    @ResponsePayload
    @PayloadRoot(
            localPart = "tasksGetRequest",
            namespace = NAMESPACE
    )
    public TasksGetResponse findAll(
            @RequestPayload final TasksGetRequest request) throws Exception {
        return new TasksGetResponse(taskService.findByUserId(UserUtil.getUserId()));
    }

    @ResponsePayload
    @PayloadRoot(
            localPart = "taskAddRequest",
            namespace = NAMESPACE
    )
    public TaskAddResponse add(
            @RequestPayload final TaskAddRequest request) throws Exception {
        return new TaskAddResponse(taskService.addByUserId(UserUtil.getUserId(), request.getTask()));
    }

    @ResponsePayload
    @PayloadRoot(
            localPart = "taskSaveRequest",
            namespace = NAMESPACE
    )
    public TaskSaveResponse save(
            @RequestPayload final TaskSaveRequest request) throws Exception {
        return new TaskSaveResponse(taskService.saveByUserId(UserUtil.getUserId(), request.getTask()));
    }

    @ResponsePayload
    @PayloadRoot(
            localPart = "taskGetRequest",
            namespace = NAMESPACE
    )
    public TaskGetResponse findById(
            @RequestPayload final TaskGetRequest request) throws Exception {
        return new TaskGetResponse(taskService.findById(UserUtil.getUserId(), request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(
            localPart = "tasksFindByProjectIdRequest",
            namespace = NAMESPACE
    )
    public TasksFindByProjectIdResponse findAllByProjectId(
            @RequestPayload final TasksFindByProjectIdRequest request) throws Exception {
        return new TasksFindByProjectIdResponse(taskService.findAllByProjectId(UserUtil.getUserId(), request.getProjectId()));
    }

    @ResponsePayload
    @PayloadRoot(
            localPart = "taskExistsByIdRequest",
            namespace = NAMESPACE
    )
    public TaskExistsByIdResponse existsById(
            @RequestPayload final TaskExistsByIdRequest request) {
        return new TaskExistsByIdResponse(taskService.existsByUserId(UserUtil.getUserId(), request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(
            localPart = "taskCountRequest",
            namespace = NAMESPACE
    )
    public TaskCountResponse count(
            @RequestPayload final TaskCountRequest request) throws Exception {
        return new TaskCountResponse(taskService.countByUserId(UserUtil.getUserId()));
    }

    @ResponsePayload
    @PayloadRoot(
            localPart = "taskDeleteByIdRequest",
            namespace = NAMESPACE
    )
    public TaskDeleteByIdResponse deleteById(
            @RequestPayload final TaskDeleteByIdRequest request) throws Exception {
        taskService.deleteByUserId(UserUtil.getUserId(), request.getId());
        return new TaskDeleteByIdResponse();
    }

}
