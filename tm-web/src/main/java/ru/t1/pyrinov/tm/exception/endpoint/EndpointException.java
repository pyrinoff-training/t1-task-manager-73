package ru.t1.pyrinov.tm.exception.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.t1.pyrinov.tm.exception.AbstractException;

public class EndpointException extends AbstractException {

    public EndpointException() {
        super("Endpoint exception!");
    }

    public EndpointException(
            @Nullable final String message) {
        super("Endpoint exception: " + message);
    }

}
