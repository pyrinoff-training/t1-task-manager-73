package ru.t1.pyrinov.tm.exception.entity;

import org.jetbrains.annotations.Nullable;

public class EntityNotFoundException extends AbstractEntityNotFoundException {

    public EntityNotFoundException(
            @Nullable final String model) {
        super("Error! Entity " + model + " not found!");
    }

    public EntityNotFoundException() {
        super("Error! Entity not found!");
    }

}
