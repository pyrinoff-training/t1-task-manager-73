package ru.t1.pyrinov.tm.exception.entity;

import org.jetbrains.annotations.Nullable;

public class EntityNullException extends AbstractEntityNotFoundException {

    public EntityNullException(
            @Nullable final String model) {
        super("Error! Entity " + model + " is null!");
    }

    public EntityNullException() {
        super("Error! Entity is null!");
    }

}
