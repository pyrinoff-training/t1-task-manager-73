package ru.t1.pyrinov.tm.exception.entity;

public class StatusNotFoundException extends EntityNotFoundException {

    public StatusNotFoundException() {
        super("Error! Status not found!");
    }

}
