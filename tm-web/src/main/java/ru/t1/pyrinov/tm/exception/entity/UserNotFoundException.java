package ru.t1.pyrinov.tm.exception.entity;

public class UserNotFoundException extends EntityNotFoundException {

    public UserNotFoundException() {
        super("Error! User not found!");
    }

}
