package ru.t1.pyrinov.tm.exception.field;

public class LoginExistsException extends AbstractFieldException {

    public LoginExistsException() {
        super("Error! Login already exists!");
    }

}
