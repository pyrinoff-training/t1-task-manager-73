package ru.t1.pyrinov.tm.exception.field;

public class TaskIdEmptyException extends AbstractFieldException {

    public TaskIdEmptyException() {
        super("Error! Task id is empty!");
    }

}
