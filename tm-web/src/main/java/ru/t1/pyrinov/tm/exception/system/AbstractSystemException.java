package ru.t1.pyrinov.tm.exception.system;

import ru.t1.pyrinov.tm.exception.AbstractException;

public abstract class AbstractSystemException extends AbstractException {

    public AbstractSystemException() {
        super();
    }

    public AbstractSystemException(String message) {
        super(message);
    }

    public AbstractSystemException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractSystemException(Throwable cause) {
        super(cause);
    }

    protected AbstractSystemException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
