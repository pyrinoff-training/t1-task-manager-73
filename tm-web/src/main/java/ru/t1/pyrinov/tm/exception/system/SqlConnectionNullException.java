package ru.t1.pyrinov.tm.exception.system;

public class SqlConnectionNullException extends AbstractSystemException {

    public SqlConnectionNullException() {
        super("Error! SQL connection is null! ");
    }

}
