package ru.t1.pyrinov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;


public class CustomSecurityUser extends User {

    @Getter
    @Setter
    private @Nullable String userId = null;

    @Getter
    @Setter
    private @Nullable String email = null;

    public CustomSecurityUser(@NotNull final UserDetails user) {
        super(user.getUsername(), user.getPassword(), user.isEnabled(), user.isAccountNonExpired(), user.isCredentialsNonExpired(), user.isAccountNonLocked(), user.getAuthorities());
    }

    public CustomSecurityUser(@NotNull final String username,
                              @NotNull final String password,
                              @NotNull final Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public CustomSecurityUser(@NotNull final String username,
                              @NotNull final String password,
                              final boolean enabled,
                              final boolean accountNonExpired,
                              final boolean credentialsNonExpired,
                              final boolean accountNonLocked,
                              @NotNull final Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }

    public @NotNull CustomSecurityUser withUserId(@NotNull final String userId) {
        this.userId = userId;
        return this;
    }

}
