package ru.t1.pyrinov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class Message {

    @NotNull String message;

    public Message(@NotNull String message) {
        this.message = message;
    }

}
