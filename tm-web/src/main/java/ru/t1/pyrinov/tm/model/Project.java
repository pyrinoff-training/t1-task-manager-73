package ru.t1.pyrinov.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.pyrinov.tm.enumerated.Status;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "projects", schema = "public")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Project {

    @Id
    private @NotNull String id = UUID.randomUUID().toString();

    private static final long serialVersionUID = 1;

    @Column
    private @NotNull String name = "";

    @Column
    private @NotNull String description = "";

    @Column
    @Enumerated(EnumType.STRING)
    private @Nullable Status status = Status.NOT_STARTED;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private @NotNull Date created = new Date();

    @Column(name = "user_id")
    private @Nullable String userId;

    public Project(final @NotNull String name, final @NotNull Status status) {
        this.name = name;
        this.status = status;
    }

    public Project(final @NotNull String name, final @NotNull Status status,
                   @NotNull final String userId) {
        this.name = name;
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return name.equals(project.name) && description.equals(project.description) && status == project.status && created.equals(project.created);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, status, created);
    }

    @NotNull
    @Override
    public String toString() {
        return name + " | " + description + " | " + status.getDisplayName() + " (ID: " + getId() + ", created: "
                + created
                + ")";
    }

}
