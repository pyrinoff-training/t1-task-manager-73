package ru.t1.pyrinov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class Result {

    private @NotNull Boolean success = true;

    private @Nullable String message = "";

    public Result(@NotNull final Boolean success) {
        this.success = success;
    }

    public Result(@NotNull final Exception e) {
        success = false;
        message = e.getMessage();
    }

}

