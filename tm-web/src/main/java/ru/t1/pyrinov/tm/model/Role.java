package ru.t1.pyrinov.tm.model;

import lombok.Getter;
import lombok.Setter;
import ru.t1.pyrinov.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "role", schema = "public")
public class Role {

    @Id
    private String id = UUID.randomUUID().toString();

    @ManyToOne
    private User user;

    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.ADMINISTRATOR;


}
