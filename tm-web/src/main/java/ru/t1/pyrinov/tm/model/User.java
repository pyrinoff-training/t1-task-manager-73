package ru.t1.pyrinov.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Getter
@Setter
@Entity
@Table(name = "user", schema = "public")
public class User {

    @Id
    @Column
    private @NotNull String id = UUID.randomUUID().toString();

    @Column
    private @NotNull String login;

    @Column
    private @NotNull String passwordHash;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Role> roles = new ArrayList<>();

    @JsonIgnore     //StackOverflowError if not @JsonIgnore
    @XmlTransient   //javax.xml.ws.soap.SOAPFaultException: Marshalling Error
    public List<Role> getRoles() {
        return roles;
    }

}
