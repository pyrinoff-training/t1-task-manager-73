package ru.t1.pyrinov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.pyrinov.tm.model.Project;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

    @Query("select count(*)>0 FROM Project WHERE userId=:userId and id=:id")
    boolean existsByUserIdAndId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    long countByUserId(@Param("userId") @NotNull final String userId);

    long deleteByUserId(@Param("userId") @NotNull final String userId);

    long deleteByUserIdAndId(@Param("userId") @NotNull final String userId, @Param("id") @NotNull final String id);

    @NotNull List<Project> findByUserId(@Param("userId") @NotNull final String userId);

    @NotNull List<Project> findByUserId(@Param("userId") @NotNull final String userId, @NotNull final Sort sort);

    @NotNull Optional<Project> findByUserIdAndId(
            @Param("userId") @NotNull final String userId, @Param("id") @NotNull final String id);

    @NotNull List<Project> findAllByUserId(@Nullable String userId);

    void deleteAllByUserId(@Nullable String userId);

}
