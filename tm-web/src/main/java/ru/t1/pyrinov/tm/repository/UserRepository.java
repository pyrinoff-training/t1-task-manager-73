package ru.t1.pyrinov.tm.repository;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.pyrinov.tm.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    @Nullable User findByLogin(final String login);

}
