package ru.t1.pyrinov.tm.service;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.pyrinov.tm.api.service.IProjectService;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.exception.entity.EntityNullException;
import ru.t1.pyrinov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.pyrinov.tm.exception.entity.StatusNotFoundException;
import ru.t1.pyrinov.tm.exception.field.*;
import ru.t1.pyrinov.tm.exception.user.AccessDeniedException;
import ru.t1.pyrinov.tm.model.Project;
import ru.t1.pyrinov.tm.repository.ProjectRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectService implements IProjectService {

    @Getter
    @PersistenceContext
    protected @NotNull EntityManager entityManager;

    @Getter
    @Autowired
    protected @NotNull ApplicationContext context;

    @Getter
    @Autowired
    private @NotNull ProjectRepository repository;

    @SneakyThrows
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return repository.save(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return repository.save(project);
    }

    @Transactional
    public @NotNull Project save(
            @Nullable final String userId,
            @Nullable final Project model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNullException();
        if (!userId.equals(model.getUserId())) throw new AccessDeniedException();
        return repository.save(model);
    }

    @Transactional
    public @NotNull Project addByUserId(
            @Nullable final String userId,
            @Nullable final Project model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNullException();
        model.setUserId(userId);
        return save(userId, model);
    }

    @Transactional
    public @NotNull Collection<Project> addAllByUserId(
            @Nullable final String userId,
            @Nullable final Collection<Project> models) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (models == null) throw new EntityNullException();
        for (@NotNull final Project oneProject : models) oneProject.setUserId(userId);
        return repository.saveAll(models);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @Nullable final Project project = repository.findById(id).orElse(null);
        if (project == null) throw new ProjectNotFoundException();
        if (!userId.equals(project.getUserId())) throw new AccessDeniedException();
        project.setName(name);
        project.setDescription(description == null ? "" : description);
        return repository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusNotFoundException();
        @Nullable final Project project = repository.findById(id).orElse(null);
        if (project == null) throw new ProjectNotFoundException();
        if (!userId.equals(project.getUserId())) throw new AccessDeniedException();
        project.setStatus(status);
        return repository.save(project);
    }

    public boolean existsByUserId(
            @Nullable final String userId,
            @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsByUserIdAndId(userId, id);
    }

    @SneakyThrows
    @Transactional
    public void removeByUserId(
            @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @SneakyThrows
    public @NotNull long countByUserId(
            @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.countByUserId(userId);
    }

    @SneakyThrows
    @Transactional
    public void remove(
            @Nullable final String userId,
            @Nullable final Project model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNullException();
        repository.deleteByUserIdAndId(userId, model.getId());
    }

    @SneakyThrows
    @Transactional
    public void removeAll(
            @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @Override
    public @NotNull List<Project> findAll() {
        return repository.findAll();
    }

    @Override
    @Transactional
    public Project save(@NotNull Project project) {
        return repository.save(project);
    }

    @Override
    public @Nullable Project findById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findByUserIdAndId(userId, id).orElse(null);
    }

    @Override
    public @Nullable Project findById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).orElse(null);
    }

    @Override
    public @Nullable List<Project> findByUserId(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findByUserId(userId);
    }

    @Override
    public long count(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.countByUserId(userId);
    }

    @Override
    public long count() {
        return repository.count();
    }

    @Override
    public @NotNull Project saveByUserId(@Nullable String userId, @Nullable Project project) {
        return save(userId, project);
    }

    @Override
    @Transactional
    public void deleteAllByUserId(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteAllByUserId(userId);
    }

    @Override
    @Nullable
    public String getNameById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return "";
        Optional<Project> byUserIdAndId = repository.findByUserIdAndId(userId, id);
        if (!byUserIdAndId.isPresent()) return "";
        return byUserIdAndId.get().getName();
    }

    public @Nullable List<Project> getProjects(@Nullable final String userId) {
        if (userId == null) throw new UserIdEmptyException();
        return findByUserId(userId);
    }

    public @Nullable Project getProject(@Nullable final String userId, @Nullable final String id) {
        if (userId == null) throw new UserIdEmptyException();
        Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    public @Nullable Project editProject(
            @Nullable final String userId, @Nullable final String id, @Nullable final Project project) {
        if (project == null) throw new IllegalArgumentException();
        @Nullable final Project projectById = findById(userId, id);
        if (projectById == null) throw new ProjectNotFoundException();
        if (!projectById.getId().equals(id)) throw new AccessDeniedException();
        return save(project);
    }

    @Transactional
    public void deleteProject(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project projectById = findById(userId, id);
        if (projectById == null) throw new ProjectNotFoundException();
        if (!projectById.getId().equals(id)) throw new AccessDeniedException();
        repository.deleteByUserIdAndId(userId, id);
    }

}
