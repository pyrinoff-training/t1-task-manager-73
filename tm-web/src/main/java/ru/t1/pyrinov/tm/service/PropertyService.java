package ru.t1.pyrinov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.pyrinov.tm.api.service.IPropertyService;

import java.util.regex.Pattern;

@Service
@Getter
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    @Value("#{environment['server.host']}")
    private String serverHost;

    @NotNull
    @Value("#{environment['server.port']}")
    private Integer serverPort;

    @NotNull
    @Value("#{environment['admin.login']}")
    private String adminLogin;

    @NotNull
    @Value("#{environment['admin.password']}")
    private String adminPassword;

    @NotNull
    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @NotNull
    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @NotNull
    @Value("#{environment['session.key']}")
    private String sessionKey;

    @NotNull
    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeout;

    @NotNull
    @Value("#{environment['database.url']}")
    private String databaseUrl;

    @NotNull
    @Value("#{environment['database.username']}")
    private String databaseUsername;

    @NotNull
    @Value("#{environment['database.password']}")
    private String databasePassword;

    @NotNull
    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @NotNull
    @Value("#{environment['database.dialect']}")
    private String databaseDialect;

    @NotNull
    @Value("#{environment['database.hbm2ddl_auto']}")
    private String databaseHbm2ddlAuto;

    @NotNull
    @Value("#{environment['database.show_sql']}")
    private String databaseShowSql;

    @NotNull
    @Value("#{environment['database.format_sql']}")
    private String databaseFormatSql;

    @NotNull
    @Value("#{environment['database.cache.use_second_level_cache']}")
    private String cacheUseSecondLevelCache;

    @NotNull
    @Value("#{environment['database.cache.use_query_cache']}")
    private String cacheUseQueryCache;

    @NotNull
    @Value("#{environment['database.cache.use_minimal_puts']}")
    private String cacheUseMinimalPuts;

    @NotNull
    @Value("#{environment['database.cache.region_prefix']}")
    private String cacheRegionPrefix;

    @NotNull
    @Value("#{environment['database.cache.provider_configuration_file_resource_path']}")
    private String cacheProviderConfigurationFile;

    @NotNull
    @Value("#{environment['database.cache.region.factory_class']}")
    private String cacheRegionFactoryClass;

    @NotNull
    @Value("#{environment['database.cache.admin_protection_token']}")
    private String databaseAdminProtectionToken;

    @NotNull
    @Value("#{environment['jms.url']}")
    private String jmsUrl;

    @NotNull
    @Value("#{environment['liquibase.outputChangeLogFile']}")
    private String liquibaseChangelogFile;

    public static final @NotNull String APPLICATION_VERSION_KEY = "version";

    public static final @NotNull String AUTHOR_NAME_KEY = "author_name";

    public static final @NotNull String AUTHOR_EMAIL_KEY = "author_name";

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    public @NotNull String getLiquibaseChangelogFile(boolean relativePath) {
        if (!relativePath) return liquibaseChangelogFile;
        Pattern pattern = Pattern.compile(".*?src/main/resources/", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
        return pattern.matcher(liquibaseChangelogFile).replaceAll("");
    }

}
