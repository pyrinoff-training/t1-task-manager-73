package ru.t1.pyrinov.tm.service;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.pyrinov.tm.api.service.ITaskService;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.exception.entity.EntityNullException;
import ru.t1.pyrinov.tm.exception.entity.StatusNotFoundException;
import ru.t1.pyrinov.tm.exception.entity.TaskNotFoundException;
import ru.t1.pyrinov.tm.exception.field.*;
import ru.t1.pyrinov.tm.exception.user.AccessDeniedException;
import ru.t1.pyrinov.tm.model.Task;
import ru.t1.pyrinov.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class TaskService implements ITaskService {

    @Getter
    @PersistenceContext
    protected @NotNull EntityManager entityManager;

    @Getter
    @Autowired
    protected @NotNull ApplicationContext context;

    @Getter
    @Autowired
    private @NotNull TaskRepository repository;

    @SneakyThrows
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return repository.save(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return repository.save(task);
    }

    @Transactional
    public @NotNull Task save(
            @Nullable final String userId,
            @Nullable final Task model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNullException();
        if (!userId.equals(model.getUserId())) throw new AccessDeniedException();
        return repository.save(model);
    }

    @Transactional
    public @NotNull Task addByUserId(
            @Nullable final String userId,
            @Nullable final Task model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNullException();
        model.setUserId(userId);
        return save(userId, model);
    }

    @Transactional
    public @NotNull Collection<Task> addAllByUserId(
            @Nullable final String userId,
            @Nullable final Collection<Task> models) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (models == null) throw new EntityNullException();
        for (@NotNull final Task oneTask : models) oneTask.setUserId(userId);
        return repository.saveAll(models);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @Nullable final Task task = repository.findById(id).orElse(null);
        if (task == null) throw new TaskNotFoundException();
        if (!userId.equals(task.getUserId())) throw new AccessDeniedException();
        task.setName(name);
        task.setDescription(description == null ? "" : description);
        return repository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusNotFoundException();
        @Nullable final Task task = repository.findById(id).orElse(null);
        if (task == null) throw new TaskNotFoundException();
        if (!userId.equals(task.getUserId())) throw new AccessDeniedException();
        task.setStatus(status);
        return repository.save(task);
    }

    public boolean existsByUserId(
            @Nullable final String userId,
            @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsByUserIdAndId(userId, id);
    }

    @SneakyThrows
    @Transactional
    public void removeByUserId(
            @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @SneakyThrows
    public long countByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.countByUserId(userId);
    }

    @SneakyThrows
    @Transactional
    public void remove(
            @Nullable final String userId,
            @Nullable final Task model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNullException();
        repository.deleteByUserIdAndId(userId, model.getId());
    }

    @SneakyThrows
    @Transactional
    public void removeAll(
            @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @Override
    public @NotNull List<Task> findAll() {
        return repository.findAll();
    }

    @Override
    public Task save(@NotNull Task task) {
        return repository.save(task);
    }

    @Override
    public void deleteById(@NotNull String id) {
        repository.deleteById(id);
    }

    @Override
    public @Nullable Task findById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findByUserIdAndId(userId, id).orElse(null);
    }

    @Override
    public @Nullable Task findById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).orElse(null);
    }

    @Override
    public @Nullable List<Task> findByUserId(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findByUserId(userId);
    }

    @Override
    public long count() {
        return repository.count();
    }

    @Override
    public @NotNull Task saveByUserId(@Nullable String userId, @Nullable Task task) {
        return save(userId, task);
    }

    @Override
    @Transactional
    public void deleteByUserId(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void deleteAllByUserId(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteAllByUserId(userId);
    }

    @Nullable
    public String getNameById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return "";
        Optional<Task> byUserIdAndId = repository.findByUserIdAndId(userId, id);
        if (!byUserIdAndId.isPresent()) return "";
        return byUserIdAndId.get().getName();
    }

    @Override
    public @Nullable List<Task> findAllByProjectId(String userId, String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findByProjectId(taskId);
    }

    public @Nullable List<Task> getTasks(@Nullable final String userId) {
        if (userId == null) throw new UserIdEmptyException();
        return findByUserId(userId);
    }

    public @Nullable Task getTask(@Nullable final String userId, @Nullable final String id) {
        if (userId == null) throw new UserIdEmptyException();
        Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    public @Nullable Task editTask(
            @Nullable final String userId, @Nullable final String id, @Nullable final Task task) {
        if (task == null) throw new IllegalArgumentException();
        @Nullable final Task taskById = findById(userId, id);
        if (taskById == null) throw new TaskNotFoundException();
        if (!taskById.getId().equals(id)) throw new AccessDeniedException();
        return save(task);
    }

    @Transactional
    public void deleteTask(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task taskById = findById(userId, id);
        if (taskById == null) throw new TaskNotFoundException();
        if (!taskById.getId().equals(id)) throw new AccessDeniedException();
        repository.deleteByUserIdAndId(userId, id);
    }

}
