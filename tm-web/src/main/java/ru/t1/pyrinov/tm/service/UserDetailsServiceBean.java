package ru.t1.pyrinov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.pyrinov.tm.api.service.IUserDetailsServiceBean;
import ru.t1.pyrinov.tm.enumerated.RoleType;
import ru.t1.pyrinov.tm.model.CustomSecurityUser;
import ru.t1.pyrinov.tm.model.Role;
import ru.t1.pyrinov.tm.model.User;
import ru.t1.pyrinov.tm.repository.UserRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service("userDetailsService")
@DependsOn({"passwordEncoder"})
public class UserDetailsServiceBean implements IUserDetailsServiceBean {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    private void postConstruct() {
        initUser("admin", "admin", RoleType.ADMINISTRATOR);
        initUser("test", "test", RoleType.USER);
    }

    public @Nullable User initUser(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final RoleType role) {
        @Nullable final User user = userRepository.findByLogin(login);
        if (user != null) return user;
        return createUser(login, password, role);
    }

    @Nullable
    private User createUser(
            @Nullable String login,
            @Nullable String password,
            @NotNull RoleType roleType) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @NotNull final String passwordHash = passwordEncoder.encode(password);
        @NotNull final User user = new User();
        @NotNull final Role role = new Role();
        role.setRoleType(roleType);
        role.setUser(user);
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
        return user;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        @NotNull final User user = findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);

        UserBuilder builder = org.springframework.security.core.userdetails.User.withUsername(username);
        builder.password(user.getPasswordHash());
        @NotNull final List<Role> userRoles = user.getRoles();
        @NotNull final List<String> roles = new ArrayList<>();
        for (@NotNull final Role role : userRoles) roles.add(role.toString());
        builder.roles(roles.toArray(new String[]{}));

        @NotNull final UserDetails userDetails = builder.build();
        return new CustomSecurityUser(
                org.springframework.security.core.userdetails.User
                        .withUsername(user.getLogin())
                        .password(user.getPasswordHash())
                        .roles(roles.toArray(new String[]{}))
                        .build()).withUserId(user.getId());
    }

    private User findByLogin(@Nullable String username) {
        if (username == null || username.isEmpty()) return null;
        return userRepository.findByLogin(username);
    }

}