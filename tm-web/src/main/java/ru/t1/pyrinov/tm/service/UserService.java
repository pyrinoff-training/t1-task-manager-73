package ru.t1.pyrinov.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.pyrinov.tm.api.service.IUserService;
import ru.t1.pyrinov.tm.exception.field.IdEmptyException;
import ru.t1.pyrinov.tm.exception.field.LoginEmptyException;
import ru.t1.pyrinov.tm.model.User;
import ru.t1.pyrinov.tm.repository.UserRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class UserService implements IUserService {

    @Getter
    @PersistenceContext
    protected @NotNull EntityManager entityManager;

    @Getter
    @Autowired
    protected @NotNull ApplicationContext context;

    @Getter
    @Autowired
    private @NotNull UserRepository repository;

    @Override
    public @NotNull List<User> findAll() {
        return repository.findAll();
    }

    @Override
    public User save(@NotNull User user) {
        return repository.save(user);
    }

    @Override
    public void delete(@NotNull String id) {
        repository.deleteById(id);
    }

    @Override
    public @Nullable User find(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).orElse(null);
    }

    @Override
    public @Nullable User findByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Override
    public long count() {
        return repository.count();
    }

}
