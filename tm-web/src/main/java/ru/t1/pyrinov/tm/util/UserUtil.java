package ru.t1.pyrinov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.t1.pyrinov.tm.model.CustomSecurityUser;

public final class UserUtil {

    private UserUtil() {
    }

    public static String getUserId() {
        @NotNull final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        @Nullable final Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessDeniedException("");
        if (!(principal instanceof CustomSecurityUser)) throw new AccessDeniedException("");
        @NotNull final CustomSecurityUser customUser = (CustomSecurityUser) principal;
        return customUser.getUserId();
    }

}
