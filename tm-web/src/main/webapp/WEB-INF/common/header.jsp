<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="${pageContext.request.contextPath}/includes/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/includes/custom.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/includes/custom.js"></script>
    <script src="${pageContext.request.contextPath}/includes/bootstrap.bundle.min.js"></script>
</head>
<body>
<div class="content">

    <sec:authorize access="isAuthenticated()"><p>Current login: <sec:authentication property="name"/></p>
    </sec:authorize>
    <a href="${pageContext.request.contextPath}/">BACK</a>
    <sec:authorize access="isAuthenticated()"><a
            href="${pageContext.request.contextPath}/logout">LOGOUT</a></sec:authorize>
    <sec:authorize access="!isAuthenticated()"><a
            href="${pageContext.request.contextPath}/logout">LOGIN</a></sec:authorize>
    </p>
</div>
