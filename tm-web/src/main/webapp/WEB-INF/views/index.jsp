<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<jsp:include page="../common/header.jsp"/>

<sec:authorize access="isAuthenticated()">
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="m-b-md col-md-12"><h1>TASK MANAGER</h1></div>
            <div><a href="${pageContext.request.contextPath}/projects/">Projects</a></div>
            <div><a href="${pageContext.request.contextPath}/tasks/">Tasks</a></div>
        </div>
    </div>
</sec:authorize>

<jsp:include page="../common/footer.jsp"/>