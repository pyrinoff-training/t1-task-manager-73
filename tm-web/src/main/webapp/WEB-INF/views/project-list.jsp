<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../common/header.jsp"/>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="m-b-md col-md-12">
            <h3>PROJECT LIST</h3>
        </div>
        <table class="table">
            <tr>
                <th class="text-center">ID</th>
                <th class="text-center">Name</th>
                <th class="text-center">Description</th>
                <th class="text-center">Status</th>
                <th class="text-center">Created</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
            <c:forEach var="project" items="${projects}">
                <tr>
                    <td><c:out value="${project.id}"/></td>
                    <td><c:out value="${project.name}"/></td>
                    <td><c:out value="${project.description}"/></td>
                    <td><c:out value="${project.status.displayName}"/></td>
                    <td><c:out value="${project.created}"/></td>
                    <td><a href="${pageContext.request.contextPath}/project/edit/${project.id}/">EDIT</a></td>
                    <td><a href="${pageContext.request.contextPath}/project/delete/${project.id}/">DELETE</a></td>
                </tr>
            </c:forEach>
        </table>
        <form action="${pageContext.request.contextPath}/project/create">
            <button>CREATE PROJECT</button>
        </form>
    </div>
</div>
<jsp:include page="../common/footer.jsp"/>