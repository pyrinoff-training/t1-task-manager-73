<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../common/header.jsp"/>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="m-b-md col-md-12">
            <h3>TASK EDIT</h3>
        </div>
        <form:form action="${pageContext.request.contextPath}/task/edit/${task.id}/" method="POST"
                   modelAttribute="task">
            <form:input type="hidden" path="id"/>
            <form:input type="hidden" path="userId"/>
            <div>Name:</div>
            <div><form:input type="text" path="name"/></div>
            <div>Description:</div>
            <div><form:input type="text" path="description"/></div>
            <div>Status:</div>
            <div>
                <form:select path="status">
                    <form:option value="${null}" label="-----------"/>
                    <form:options items="${statuses}" itemLabel="displayName"/>
                </form:select>
            </div>
            <div>CREATED:</div>
            <div><form:input type="date" path="created"/></div>
            <div>Project:</div>
            <div>
                <form:select path="projectId">
                    <form:option value="${null}" label="-----------"/>
                    <form:options items="${projects}" itemLabel="name" itemValue="id"/>
                </form:select>
            </div>
            <button type="submit" class="btn btn-primary">SAVE TASK</button>
        </form:form>
    </div>
</div>
<jsp:include page="../common/footer.jsp"/>