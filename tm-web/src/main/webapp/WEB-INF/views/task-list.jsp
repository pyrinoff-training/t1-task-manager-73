<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../common/header.jsp"/>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="m-b-md col-md-12">
            <h3>TASK LIST</h3>
        </div>
        <table class="table">
            <tr>
                <th class="text-center">ID</th>
                <th class="text-center">Name</th>
                <th class="text-center">Description</th>
                <th class="text-center">Status</th>
                <th class="text-center">Created</th>
                <th class="text-center">Project ID</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
            <c:forEach var="task" items="${tasks}">
                <tr>
                    <td><c:out value="${task.id}"/></td>
                    <td><c:out value="${task.name}"/></td>
                    <td><c:out value="${task.description}"/></td>
                    <td><c:out value="${task.status.displayName}"/></td>
                    <td><c:out value="${task.created}"/></td>
                    <td><c:out value="${projectService.getNameById(task.userId, task.projectId)}"/></td>
                    <td><a href="${pageContext.request.contextPath}/task/edit/${task.id}">EDIT</a></td>
                    <td><a href="${pageContext.request.contextPath}/task/delete/${task.id}">DELETE</a></td>
                </tr>
            </c:forEach>
        </table>
        <form action="${pageContext.request.contextPath}/task/create">
            <button>CREATE TASK</button>
        </form>
    </div>
</div>
<jsp:include page="../common/footer.jsp"/>