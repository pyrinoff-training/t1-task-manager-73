function onReady() {
    const queryString = window.location.search;
    if (queryString.includes('error=true')) {
        const errorElement = document.createElement('p');
        errorElement.textContent = 'Login failed! Check your credentials!';
        document.getElementById("infoBlock").appendChild(errorElement);
    }
}

function docReady(fn) {
    // see if DOM is already available
    if (document.readyState === "complete" || document.readyState === "interactive") {
        // call on next available tick
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

docReady(onReady);