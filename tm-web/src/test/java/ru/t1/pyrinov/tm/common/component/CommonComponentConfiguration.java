package ru.t1.pyrinov.tm.common.component;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.t1.pyrinov.tm.common.component")
public class CommonComponentConfiguration {

}
