package ru.t1.pyrinov.tm.common.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.api.service.IProjectService;
import ru.t1.pyrinov.tm.model.Project;

@Component
public class ProjectDataGenerator {

    @Autowired
    private IProjectService projectService;

    public @NotNull Project addProjectByDb(@Nullable final String userId) {
        @Nullable final Project project = projectService.save(getStandardProject(userId));
        Assertions.assertNotNull(project);
        Assertions.assertNotNull(userId);
        return project;
    }

    private static int projectNumber = 1;

    public @NotNull Project getStandardProject(@Nullable final String userId) {
        @NotNull final Project project = new Project();
        project.setName("PROJECT #" + projectNumber++);
        project.setDescription("PROJECT DESCRIPTION");
        project.setUserId(userId);
        return project;
    }

    public void testDataGen(@Nullable final String userId) {
        @NotNull final Project project = addProjectByDb(userId);
        Assertions.assertNotNull(projectService.findById(project.getId()));
    }

}
