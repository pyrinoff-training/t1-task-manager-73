package ru.t1.pyrinov.tm.common.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.pyrinov.tm.api.service.ITaskService;
import ru.t1.pyrinov.tm.model.Project;
import ru.t1.pyrinov.tm.model.Task;

@Component
public class TaskDataGenerator {

    @Autowired
    private ITaskService taskService;

    @NotNull
    public Task addTaskByDb(@Nullable final String userId) {
        @Nullable final Task task = taskService.save(getStandardTask(userId));
        Assertions.assertNotNull(task);
        Assertions.assertNotNull(userId);
        return task;
    }

    private static int taskNumber = 1;

    @NotNull
    public Task getStandardTask(@Nullable final String userId) {
        @NotNull final Task task = new Task();
        task.setName("TASK #" + taskNumber++);
        task.setDescription("TASK DESCRIPTION");
        task.setUserId(userId);
        return task;
    }

    public void testDataGen(@Nullable final String userId) {
        @NotNull final Task task = addTaskByDb(userId);
        Assertions.assertNotNull(taskService.findById(task.getId()));
    }

}
