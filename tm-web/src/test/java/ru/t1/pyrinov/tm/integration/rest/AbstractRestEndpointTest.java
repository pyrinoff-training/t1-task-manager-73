package ru.t1.pyrinov.tm.integration.rest;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.t1.pyrinov.tm.common.marker.WebIntegrationTag;
import ru.t1.pyrinov.tm.model.Result;
import ru.t1.pyrinov.tm.model.User;

import java.net.HttpCookie;
import java.net.URI;
import java.util.List;
import java.util.Objects;

@TestMethodOrder(MethodOrderer.MethodName.class)
@ExtendWith(SpringExtension.class)
@WebAppConfiguration
//@ContextConfiguration(classes = {ApplicationConfiguration.class, WebMvcConfiguration.class, CommonComponentConfiguration.class})
public class AbstractRestEndpointTest implements WebIntegrationTag {

    @Getter
    protected static final @NotNull String API_BASE_URL = "http://localhost:8082/tm/api";

    protected static final @NotNull String USERNAME = "test";

    protected static final @NotNull String PASSWORD = "test";

    @Getter
    protected static final @NotNull HttpHeaders HEADER = new HttpHeaders();

    private static @Nullable String SESSION_ID;

    @Getter
    protected static @Nullable String USER_ID;

    @BeforeAll
    protected static void authorize() {
        authorizeRequest();
        profileRequest();
    }

    public static void authorizeRequest() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();

        HEADER.setContentType(MediaType.APPLICATION_JSON);

        @NotNull final URI uri = UriComponentsBuilder
                .fromUriString(API_BASE_URL + "/auth/login")
                .queryParam("username", USERNAME)
                .queryParam("password", PASSWORD)
                .build().toUri();

        @NotNull
        final ResponseEntity<Result> response = restTemplate.postForEntity(uri, new HttpEntity<>("", HEADER), Result.class);

        Assertions.assertEquals(200, response.getStatusCodeValue());
        Assertions.assertNotNull(response.getBody());
        Assertions.assertTrue(response.getBody().getSuccess());

        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        @Nullable String firstSetCookieHeader = headersResponse.getFirst(HttpHeaders.SET_COOKIE);
        if (firstSetCookieHeader == null) return;
        @NotNull final List<HttpCookie> cookies = HttpCookie.parse(firstSetCookieHeader);
        SESSION_ID = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        if (SESSION_ID == null) return;
        Assertions.assertNotNull(SESSION_ID);
        HEADER.put(HttpHeaders.COOKIE, List.of("JSESSIONID=" + SESSION_ID));
    }

    protected static void profileRequest() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final ResponseEntity<User> responseProfile = restTemplate.exchange(
                API_BASE_URL + "/auth/profile",
                HttpMethod.GET,
                new HttpEntity<>(HEADER),
                User.class);
        USER_ID = Objects.requireNonNull(responseProfile.getBody()).getId();
    }

}
