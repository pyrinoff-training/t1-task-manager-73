package ru.t1.pyrinov.tm.integration.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.t1.pyrinov.tm.api.service.IProjectService;
import ru.t1.pyrinov.tm.common.component.ProjectDataGenerator;
import ru.t1.pyrinov.tm.model.Project;

import java.net.URI;
import java.util.HashMap;
import java.util.UUID;

public class ProjectRestEndpointTest extends AbstractRestEndpointTest {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private ProjectDataGenerator projectDataGenerator;

    @Test
    public void setupOk() {
    }

    @Test
    public void testOfAddProjectByDb() {
        projectDataGenerator.testDataGen(USER_ID);
    }

    @Test
    public void add() {
        @NotNull final Project project = projectDataGenerator.getStandardProject(USER_ID);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final URI uri = UriComponentsBuilder.fromUriString(API_BASE_URL + "/project").build().toUri();
        @NotNull
        final ResponseEntity<Project> response = restTemplate.postForEntity(uri, new HttpEntity<>(project, HEADER), Project.class);
        Assertions.assertNotNull(response);
        Assertions.assertEquals(HttpStatus.CREATED, response.getStatusCode());
        @Nullable final Project createdProject = response.getBody();
        Assertions.assertNotNull(createdProject);
        Assertions.assertEquals(project.getName(), createdProject.getName());
        Assertions.assertEquals(project.getDescription(), createdProject.getDescription());
    }

    public @NotNull
    final ResponseEntity<Project> getProject(@NotNull final String id) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final URI uri = UriComponentsBuilder.fromUriString(API_BASE_URL + "/project/{id}")
                .buildAndExpand(new HashMap<>() {{
                    put("id", id);
                }})
                .toUri();
        return restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(HEADER), Project.class);
    }

    @Test
    public void get() {
        @NotNull final Project project = projectDataGenerator.addProjectByDb(USER_ID);
        @NotNull final ResponseEntity<Project> response = getProject(project.getId());
        Assertions.assertNotNull(response);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        @Nullable final Project getProject = response.getBody();
        Assertions.assertNotNull(getProject);
        Assertions.assertEquals(project, getProject);
    }

    @Test
    public void get404() {
        @NotNull final Project project = projectDataGenerator.addProjectByDb(USER_ID);
        try {
            getProject(UUID.randomUUID().toString());
        } catch (@NotNull final HttpClientErrorException exception) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, exception.getStatusCode());
            return;
        }
        Assertions.fail();
    }

    public @NotNull
    final ResponseEntity<Project> editProject(@NotNull final String id, @NotNull final Project project) {
        //Build and send request
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final URI uri = UriComponentsBuilder.fromUriString(API_BASE_URL + "/project/{id}")
                .buildAndExpand(new HashMap<>() {{
                    put("id", id);
                }})
                .toUri();
        return restTemplate.exchange(uri, HttpMethod.PUT, new HttpEntity<>(project, HEADER), Project.class);
    }

    @Test
    public void edit() {
        @NotNull final Project project = projectDataGenerator.addProjectByDb(USER_ID);
        @NotNull final String newDesc = "EDITED DESCRIPTION";
        project.setDescription(newDesc);
        @NotNull final ResponseEntity<Project> response = editProject(project.getId(), project);
        @Nullable final Project editedProject = response.getBody();
        Assertions.assertNotNull(response);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertNotNull(editedProject);
        Assertions.assertEquals(newDesc, editedProject.getDescription());
    }

    @Test
    public void edit404() {
        @NotNull final Project project = projectDataGenerator.getStandardProject(USER_ID);
        try {
            @NotNull
            final ResponseEntity<Project> response = editProject(UUID.randomUUID().toString(), projectDataGenerator.getStandardProject(USER_ID));
        } catch (@NotNull final HttpClientErrorException exception) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, exception.getStatusCode());
            return;
        }
        Assertions.fail();
    }

    @Test
    public void delete() {
        @NotNull final Project project = projectDataGenerator.addProjectByDb(USER_ID);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final URI uri = UriComponentsBuilder.fromUriString(API_BASE_URL + "/project/{id}")
                .buildAndExpand(new HashMap<>() {{
                    put("id", project.getId());
                }})
                .toUri();
        ResponseEntity<String> exchange = restTemplate.exchange(uri, HttpMethod.DELETE, new HttpEntity<>(null, HEADER), String.class);
        Assertions.assertEquals(HttpStatus.OK, exchange.getStatusCode());
        Assertions.assertNull(projectService.findById(project.getId()));
    }

    @Test
    public void getAll() {
        long countDb = projectService.count();
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final URI uri = UriComponentsBuilder.fromUriString(API_BASE_URL + "/projects").build().toUri();
        @NotNull
        final ResponseEntity<Project[]> exchange = restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(HEADER), Project[].class);
        Assertions.assertEquals(HttpStatus.OK, exchange.getStatusCode());
        @NotNull final Project[] projects = exchange.getBody();
        Assertions.assertNotNull(projects);
        Assertions.assertEquals(countDb, projects.length);
    }

}
