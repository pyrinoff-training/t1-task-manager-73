package ru.t1.pyrinov.tm.integration.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.t1.pyrinov.tm.api.service.ITaskService;
import ru.t1.pyrinov.tm.common.component.TaskDataGenerator;
import ru.t1.pyrinov.tm.model.Task;

import java.net.URI;
import java.util.HashMap;
import java.util.UUID;

public class TaskRestEndpointTest extends AbstractRestEndpointTest {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private TaskDataGenerator taskDataGenerator;

    @Test
    public void setupOk() {
    }

    @Test
    public void testOfAddTaskByDb() {
        taskDataGenerator.testDataGen(USER_ID);
    }

    @Test
    public void add() {
        @NotNull final Task task = taskDataGenerator.getStandardTask(USER_ID);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final URI uri = UriComponentsBuilder.fromUriString(API_BASE_URL + "/task").build().toUri();
        @NotNull
        final ResponseEntity<Task> response = restTemplate.postForEntity(uri, new HttpEntity<>(task, HEADER), Task.class);
        Assertions.assertNotNull(response);
        Assertions.assertEquals(HttpStatus.CREATED, response.getStatusCode());
        @Nullable final Task createdTask = response.getBody();
        Assertions.assertNotNull(createdTask);
        Assertions.assertEquals(task.getName(), createdTask.getName());
        Assertions.assertEquals(task.getDescription(), createdTask.getDescription());
    }

    public @NotNull
    final ResponseEntity<Task> getTask(@NotNull final String id) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final URI uri = UriComponentsBuilder.fromUriString(API_BASE_URL + "/task/{id}")
                .buildAndExpand(new HashMap<>() {{
                    put("id", id);
                }})
                .toUri();
        return restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(HEADER), Task.class);
    }

    @Test
    public void get() {
        @NotNull final Task task = taskDataGenerator.addTaskByDb(USER_ID);
        @NotNull final ResponseEntity<Task> response = getTask(task.getId());
        Assertions.assertNotNull(response);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        @Nullable final Task getTask = response.getBody();
        Assertions.assertNotNull(getTask);
        Assertions.assertEquals(task, getTask);
    }

    @Test
    public void get404() {
        @NotNull final Task task = taskDataGenerator.addTaskByDb(USER_ID);
        try {
            getTask(UUID.randomUUID().toString());
        } catch (@NotNull final HttpClientErrorException exception) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, exception.getStatusCode());
            return;
        }
        Assertions.fail();
    }

    public @NotNull
    final ResponseEntity<Task> editTask(@NotNull final String id, @NotNull final Task task) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final URI uri = UriComponentsBuilder.fromUriString(API_BASE_URL + "/task/{id}")
                .buildAndExpand(new HashMap<>() {{
                    put("id", id);
                }})
                .toUri();
        return restTemplate.exchange(uri, HttpMethod.PUT, new HttpEntity<>(task, HEADER), Task.class);
    }

    @Test
    public void edit() {
        @NotNull final Task task = taskDataGenerator.addTaskByDb(USER_ID);
        @NotNull final String newDesc = "EDITED DESCRIPTION";
        task.setDescription(newDesc);
        @NotNull final ResponseEntity<Task> response = editTask(task.getId(), task);
        @Nullable final Task editedTask = response.getBody();
        Assertions.assertNotNull(response);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertNotNull(editedTask);
        Assertions.assertEquals(newDesc, editedTask.getDescription());
    }

    @Test
    public void edit404() {
        @NotNull final Task task = taskDataGenerator.getStandardTask(USER_ID);
        try {
            @NotNull
            final ResponseEntity<Task> response = editTask(UUID.randomUUID().toString(), taskDataGenerator.getStandardTask(USER_ID));
        } catch (@NotNull final HttpClientErrorException exception) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, exception.getStatusCode());
            return;
        }
        Assertions.fail();
    }

    @Test
    public void delete() {
        @NotNull final Task task = taskDataGenerator.addTaskByDb(USER_ID);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final URI uri = UriComponentsBuilder.fromUriString(API_BASE_URL + "/task/{id}")
                .buildAndExpand(new HashMap<>() {{
                    put("id", task.getId());
                }})
                .toUri();
        ResponseEntity<String> exchange = restTemplate.exchange(uri, HttpMethod.DELETE, new HttpEntity<>(null, HEADER), String.class);
        Assertions.assertEquals(HttpStatus.OK, exchange.getStatusCode());
        Assertions.assertNull(taskService.findById(task.getId()));
    }

    @Test
    public void getAll() {
        long countDb = taskService.count();
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final URI uri = UriComponentsBuilder.fromUriString(API_BASE_URL + "/tasks").build().toUri();
        @NotNull
        final ResponseEntity<Task[]> exchange = restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(HEADER), Task[].class);
        Assertions.assertEquals(HttpStatus.OK, exchange.getStatusCode());
        @NotNull final Task[] tasks = exchange.getBody();
        Assertions.assertNotNull(tasks);
        Assertions.assertEquals(countDb, tasks.length);
    }

}
