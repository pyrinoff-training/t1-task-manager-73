package ru.t1.pyrinov.tm.integration.soap;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.data.util.CastUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.pyrinov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.pyrinov.tm.client.soap.AuthSoapEndpointClient;
import ru.t1.pyrinov.tm.common.marker.WebIntegrationTag;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@TestMethodOrder(MethodOrderer.MethodName.class)
@ExtendWith(SpringExtension.class)
@WebAppConfiguration
//@ContextConfiguration(classes = {ApplicationConfiguration.class, WebMvcConfiguration.class, CommonComponentConfiguration.class})
public class AbstractSoapEndpointTest implements WebIntegrationTag {

    @Getter
    protected static final @NotNull String API_BASE_URL = "http://localhost:8082/tm";

    protected static final @NotNull String USERNAME = "test";

    protected static final @NotNull String PASSWORD = "test";

    protected static @Nullable Map<String, List<String>> HEADER;

    protected static @Nullable String USER_ID;

    private static IAuthEndpoint authEndpoint;

    @BeforeAll
    public static void initAuthEndpoint() throws MalformedURLException {
        authEndpoint = AuthSoapEndpointClient.getInstance(API_BASE_URL);
        Assertions.assertTrue(authEndpoint.login(USERNAME, PASSWORD).getSuccess());
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        HEADER = getCookieHeadersFromContext(authBindingProvider.getResponseContext());
        authBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, HEADER);
        USER_ID = authEndpoint.profile().getId();
    }

    protected static @NotNull Map<String, List<String>> getCookieHeadersFromContext(
            @NotNull final Map<String, Object> responseContext) {
        @NotNull Map<String, List<String>> resultHeaders = new HashMap<>();
        @Nullable Map<String, List<String>> headersFromRequest = CastUtils.cast(responseContext.get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headersFromRequest == null) return resultHeaders;
        @NotNull final Object cookieValue = headersFromRequest.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        resultHeaders.put("Cookie", Collections.singletonList(cookies.get(0)));
        return resultHeaders;
    }

}
