//package ru.t1.pyrinov.tm.integration.soap;
//
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import ru.t1.pyrinov.tm.api.endpoint.IProjectSoapEndpoint;
//import ru.t1.pyrinov.tm.api.service.IProjectService;
//import ru.t1.pyrinov.tm.client.soap.ProjectSoapEndpointClient;
//import ru.t1.pyrinov.tm.common.component.ProjectDataGenerator;
//import ru.t1.pyrinov.tm.model.Project;
//
//import javax.xml.ws.soap.SOAPFaultException;
//import java.net.MalformedURLException;
//import java.util.List;
//import java.util.UUID;
//
//
//public class ProjectSoapEndpointTest extends AbstractSoapEndpointTest {
//
//    private static IProjectSoapEndpoint projectEndpoint;
//
//    @Autowired
//    private ProjectDataGenerator projectDataGenerator;
//
//    @Autowired
//    private IProjectService projectService;
//
//    @BeforeAll
//    public static void initProjectEndpoint() throws MalformedURLException {
//        projectEndpoint = ProjectSoapEndpointClient.getInstance(API_BASE_URL, HEADER);
//    }
//
//    @Test
//    public void setupOk() {
//    }
//
//    @Test
//    public void add() {
//        //Get project
//        @NotNull final Project project = projectDataGenerator.getStandardProject(USER_ID);
//        //Build and send request
//        @Nullable final Project createdProject = projectEndpoint.addProject(project);
//        //Assert project content
//        Assertions.assertNotNull(createdProject);
//        Assertions.assertNotNull(createdProject);
//        Assertions.assertEquals(project.getName(), createdProject.getName());
//        Assertions.assertEquals(project.getDescription(), createdProject.getDescription());
//    }
//
//    @Test
//    public void getProject() {
//        //Add project to DB (using projectService, not controller)
//        @NotNull final Project project = projectDataGenerator.addProjectByDb(USER_ID);
//        //Build and send request
//        @Nullable final Project getProject = projectEndpoint.getProject(project.getId());
//        //Assert project content
//        Assertions.assertNotNull(getProject);
//        Assertions.assertEquals(project, getProject);
//    }
//
//    @Test
//    public void get404Project() {
//        @NotNull final String id = UUID.randomUUID().toString();
//        //Build and send request
//        try {
//            @Nullable final Project getProject = projectEndpoint.getProject(id);
//        } catch (@NotNull final SOAPFaultException exception) {
//            //Assertions.assertTrue(exception.getMessage().contains("404 NOT_FOUND"));
//            Assertions.assertTrue(exception.getCause().getMessage().contains("404 NOT_FOUND"));
//            return;
//        }
//        Assertions.fail();
//    }
//
//
//    @Test
//    public void editProject() {
//        //Add project to DB (using projectService, not controller)
//        @NotNull final Project project = projectDataGenerator.addProjectByDb(USER_ID);
//        @NotNull final String newDesc = "EDITED DESCRIPTION";
//        project.setDescription(newDesc);
//        //Build and send request
//        @Nullable final Project editedProject = projectEndpoint.editProject(project.getId(), project);
//        //Assert project content
//        Assertions.assertNotNull(editedProject);
//        Assertions.assertEquals(newDesc, editedProject.getDescription());
//    }
//
//    @Test
//    public void edit404Project() {
//        @NotNull final Project nonExistProject = projectDataGenerator.getStandardProject(USER_ID);
//        @NotNull final String id = UUID.randomUUID().toString();
//        //Build and send request
//        try {
//            @Nullable final Project getProject = projectEndpoint.editProject(id, nonExistProject);
//        } catch (@NotNull final SOAPFaultException exception) {
//            //Assertions.assertTrue(exception.getMessage().contains("404 NOT_FOUND"));
//            Assertions.assertTrue(exception.getCause().getMessage().contains("404 NOT_FOUND"));
//            return;
//        }
//        Assertions.fail();
//    }
//
//    @Test
//    public void delete() {
//        @NotNull final Project project = projectDataGenerator.addProjectByDb(USER_ID);
//        //Build and send request
//        projectEndpoint.deleteProject(project.getId());
//        //Assert business login
//        Assertions.assertNull(projectService.findById(project.getId()));
//    }
//
//
//    @Test
//    public void getAll() {
//        long countDb = projectService.count();
//        //Build and send request
//        @NotNull final List<Project> projectList = projectEndpoint.getProjects();
//        //Assert business login
//        Assertions.assertNotNull(projectList);
//        Assertions.assertEquals(countDb, projectList.size());
//    }
//
//}
