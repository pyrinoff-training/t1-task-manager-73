//package ru.t1.pyrinov.tm.integration.soap;
//
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import ru.t1.pyrinov.tm.api.endpoint.ITaskSoapEndpoint;
//import ru.t1.pyrinov.tm.api.service.ITaskService;
//import ru.t1.pyrinov.tm.client.soap.TaskSoapEndpointClient;
//import ru.t1.pyrinov.tm.common.component.TaskDataGenerator;
//import ru.t1.pyrinov.tm.model.Task;
//
//import javax.xml.ws.soap.SOAPFaultException;
//import java.net.MalformedURLException;
//import java.util.List;
//import java.util.UUID;
//
//
//public class TaskSoapEndpointTest extends AbstractSoapEndpointTest {
//
//    private static ITaskSoapEndpoint taskEndpoint;
//
//    @Autowired
//    private TaskDataGenerator taskDataGenerator;
//
//    @Autowired
//    private ITaskService taskService;
//
//    @BeforeAll
//    public static void initTaskEndpoint() throws MalformedURLException {
//        taskEndpoint = TaskSoapEndpointClient.getInstance(API_BASE_URL, HEADER);
//    }
//
//    @Test
//    public void setupOk() {
//    }
//
//    @Test
//    public void add() {
//        //Get task
//        @NotNull final Task task = taskDataGenerator.getStandardTask(USER_ID);
//        //Build and send request
//        @Nullable final Task createdTask = taskEndpoint.addTask(task);
//        //Assert task content
//        Assertions.assertNotNull(createdTask);
//        Assertions.assertNotNull(createdTask);
//        Assertions.assertEquals(task.getName(), createdTask.getName());
//        Assertions.assertEquals(task.getDescription(), createdTask.getDescription());
//    }
//
//    @Test
//    public void getTask() {
//        //Add task to DB (using taskService, not controller)
//        @NotNull final Task task = taskDataGenerator.addTaskByDb(USER_ID);
//        //Build and send request
//        @Nullable final Task getTask = taskEndpoint.getTask(task.getId());
//        //Assert task content
//        Assertions.assertNotNull(getTask);
//        Assertions.assertEquals(task, getTask);
//    }
//
//    @Test
//    public void get404Task() {
//        @NotNull final String id = UUID.randomUUID().toString();
//        //Build and send request
//        try {
//            @Nullable final Task getTask = taskEndpoint.getTask(id);
//        } catch (@NotNull final SOAPFaultException exception) {
//            //Assertions.assertTrue(exception.getMessage().contains("404 NOT_FOUND"));
//            Assertions.assertTrue(exception.getCause().getMessage().contains("404 NOT_FOUND"));
//            return;
//        }
//        Assertions.fail();
//    }
//
//
//    @Test
//    public void editTask() {
//        //Add task to DB (using taskService, not controller)
//        @NotNull final Task task = taskDataGenerator.addTaskByDb(USER_ID);
//        @NotNull final String newDesc = "EDITED DESCRIPTION";
//        task.setDescription(newDesc);
//        //Build and send request
//        @Nullable final Task editedTask = taskEndpoint.editTask(task.getId(), task);
//        //Assert task content
//        Assertions.assertNotNull(editedTask);
//        Assertions.assertEquals(newDesc, editedTask.getDescription());
//    }
//
//    @Test
//    public void edit404Task() {
//        @NotNull final Task nonExistTask = taskDataGenerator.getStandardTask(USER_ID);
//        @NotNull final String id = UUID.randomUUID().toString();
//        //Build and send request
//        try {
//            @Nullable final Task getTask = taskEndpoint.editTask(id, nonExistTask);
//        } catch (@NotNull final SOAPFaultException exception) {
//            //Assertions.assertTrue(exception.getMessage().contains("404 NOT_FOUND"));
//            Assertions.assertTrue(exception.getCause().getMessage().contains("404 NOT_FOUND"));
//            return;
//        }
//        Assertions.fail();
//    }
//
//    @Test
//    public void delete() {
//        @NotNull final Task task = taskDataGenerator.addTaskByDb(USER_ID);
//        //Build and send request
//        taskEndpoint.deleteTask(task.getId());
//        //Assert business login
//        Assertions.assertNull(taskService.findById(task.getId()));
//    }
//
//
//    @Test
//    public void getAll() {
//        long countDb = taskService.count();
//        //Build and send request
//        @NotNull final List<Task> taskList = taskEndpoint.getTasks();
//        //Assert business login
//        Assertions.assertNotNull(taskList);
//        Assertions.assertEquals(countDb, taskList.size());
//    }
//
//}
