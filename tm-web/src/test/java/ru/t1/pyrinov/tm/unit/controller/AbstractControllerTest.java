package ru.t1.pyrinov.tm.unit.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.pyrinov.tm.common.marker.WebUnitTag;
import ru.t1.pyrinov.tm.util.UserUtil;

import javax.annotation.PostConstruct;

@TestMethodOrder(MethodOrderer.MethodName.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AbstractControllerTest implements WebUnitTag {

    @Autowired
    private AuthenticationManager authenticationManager;

    protected MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    protected static @Nullable String USER_ID;

    protected static final @NotNull String USERNAME = "test";

    protected static final @NotNull String PASSWORD = "test";

    @PostConstruct
    protected void authorize() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
    }

}
