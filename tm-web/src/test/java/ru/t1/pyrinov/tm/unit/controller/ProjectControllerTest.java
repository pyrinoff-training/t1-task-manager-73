package ru.t1.pyrinov.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.t1.pyrinov.tm.api.service.IProjectService;
import ru.t1.pyrinov.tm.model.Project;
import ru.t1.pyrinov.tm.repository.ProjectRepository;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ProjectControllerTest extends AbstractControllerTest {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private ProjectRepository projectRepository;

    //TEST DATA GENERATION START
    private @NotNull Project addProjectByDb() {
        @Nullable final Project project = projectService.save(getStandardProject());
        Assertions.assertNotNull(project);
        Assertions.assertNotNull(USER_ID);
        return project;
    }

    private static int projectNumber = 1;

    private @NotNull Project getStandardProject() {
        @NotNull final Project project = new Project();
        project.setName("PROJECT #" + projectNumber++);
        project.setDescription("PROJECT DESCRIPTION");
        project.setUserId(USER_ID);
        return project;
    }
    //TEST DATA GENERATION END

    @Test
    @SneakyThrows
    public void findAllTest() {
        @NotNull final String url = "/projects";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void createTest() {
        long count = projectRepository.count();
        @NotNull final String url = "/project/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @Nullable final List<Project> projects = projectService.findAll();
        Assertions.assertNotNull(projects);
        Assertions.assertEquals(count + 1, projects.size());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        @NotNull final Project project = addProjectByDb();
        @NotNull final String url = "/project/delete/" + project.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @Nullable final Project projectFind = projectService.findById(project.getId());
        Assertions.assertNull(projectFind);
    }

    @Test
    @SneakyThrows
    public void editTest() {
        @NotNull final Project project = addProjectByDb();
        @NotNull final String url = "/project/edit/" + project.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

}
