package ru.t1.pyrinov.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.t1.pyrinov.tm.api.service.ITaskService;
import ru.t1.pyrinov.tm.model.Task;
import ru.t1.pyrinov.tm.repository.TaskRepository;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TaskControllerTest extends AbstractControllerTest {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private TaskRepository taskRepository;

    //TEST DATA GENERATION START
    private @NotNull Task addTaskByDb() {
        @Nullable final Task task = taskService.save(getStandardTask());
        Assertions.assertNotNull(task);
        Assertions.assertNotNull(USER_ID);
        return task;
    }

    private static int taskNumber = 1;

    private @NotNull Task getStandardTask() {
        @NotNull final Task task = new Task();
        task.setName("TASK #" + taskNumber++);
        task.setDescription("TASK DESCRIPTION");
        task.setUserId(USER_ID);
        return task;
    }
    //TEST DATA GENERATION END

    @Test
    @SneakyThrows
    public void findAllTest() {
        @NotNull final String url = "/tasks";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void createTest() {
        long count = taskRepository.count();
        @NotNull final String url = "/task/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @Nullable final List<Task> tasks = taskService.findAll();
        Assertions.assertNotNull(tasks);
        Assertions.assertEquals(count + 1, tasks.size());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        @NotNull final Task task = addTaskByDb();
        @NotNull final String url = "/task/delete/" + task.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @Nullable final Task taskFind = taskService.findById(task.getId());
        Assertions.assertNull(taskFind);
    }

    @Test
    @SneakyThrows
    public void editTest() {
        @NotNull final Task task = addTaskByDb();
        @NotNull final String url = "/task/edit/" + task.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

}
