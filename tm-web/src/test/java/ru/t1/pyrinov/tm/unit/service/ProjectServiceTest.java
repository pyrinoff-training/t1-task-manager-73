package ru.t1.pyrinov.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import ru.t1.pyrinov.tm.api.service.IProjectService;
import ru.t1.pyrinov.tm.api.service.IUserService;
import ru.t1.pyrinov.tm.common.AbstractSpringTest;
import ru.t1.pyrinov.tm.enumerated.RoleType;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.pyrinov.tm.exception.entity.StatusNotFoundException;
import ru.t1.pyrinov.tm.exception.field.*;
import ru.t1.pyrinov.tm.model.Project;
import ru.t1.pyrinov.tm.model.User;
import ru.t1.pyrinov.tm.repository.ProjectRepository;
import ru.t1.pyrinov.tm.service.UserDetailsServiceBean;

import java.util.List;
import java.util.UUID;


public class ProjectServiceTest extends AbstractSpringTest {

    @Autowired
    private @NotNull IProjectService projectService;

    @Autowired
    private @NotNull ProjectRepository projectRepository;

    @Autowired
    private @NotNull IUserService userService;

    @Autowired
    private @NotNull UserDetailsServiceBean userDetailsServiceBean;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    protected static final @NotNull String USERNAME = "test";

    protected static final @NotNull String PASSWORD = "test";

    protected static @Nullable String USER_ID;

    //TEST DATA GENERATION START
    private @NotNull Project addProjectByDb() {
        @Nullable final Project project = projectRepository.save(getStandardProject());
        Assertions.assertNotNull(project);
        Assertions.assertNotNull(USER_ID);
        return project;
    }

    private static int projectNumber = 1;

    private @NotNull Project getStandardProject() {
        @NotNull final Project project = new Project();
        project.setName("PROJECT #" + projectNumber++);
        project.setDescription("PROJECT DESCRIPTION");
        project.setUserId(USER_ID);
        return project;
    }
    //TEST DATA GENERATION END

    @BeforeEach
    public void setUp() throws Exception {
        //Run only once
        if (USER_ID == null) {
            @Nullable final User user = userDetailsServiceBean.initUser(USERNAME, PASSWORD, RoleType.USER);
            Assertions.assertNotNull(user);
            USER_ID = user.getId();
            System.out.println("USER ID: " + USER_ID);
        }
    }


    @AfterAll
    public static void tearDown() throws Exception {
    }

    @BeforeEach
    public void before() throws Exception {
    }

    @AfterEach
    public void after() throws Exception {
    }

    @Test
    public void setupOk() {
    }

    @Test
    public void addByUserId() {
        @NotNull final Project project = getStandardProject();
        Assertions.assertThrows(UserIdEmptyException.class, () -> projectService.addByUserId(null, project));
        Assertions.assertNotNull(projectService.addByUserId(USER_ID, project));
        @Nullable final Project projectFind = projectService.findById(USER_ID, project.getId());
        Assertions.assertNotNull(projectFind);
        Assertions.assertEquals(projectFind.getId(), project.getId());
    }


    @Test
    public void findAllByUserId() {
        long count = projectRepository.countByUserId(USER_ID);
        Assertions.assertThrows(UserIdEmptyException.class, () -> projectService.findByUserId(""));
        final List<Project> projects = projectService.findByUserId(USER_ID);
        Assertions.assertNotNull(projects);
        Assertions.assertEquals(count, projects.size());
        projects.forEach(project -> Assertions.assertEquals(USER_ID, project.getUserId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assertions.assertThrows(UserIdEmptyException.class, () -> projectService.existsByUserId(null, UUID.randomUUID().toString()));
        Assertions.assertThrows(UserIdEmptyException.class, () -> projectService.existsByUserId("", UUID.randomUUID().toString()));
        Assertions.assertThrows(IdEmptyException.class, () -> projectService.existsByUserId(USER_ID, ""));
        Assertions.assertFalse(projectService.existsByUserId(USER_ID, UUID.randomUUID().toString()));
        @NotNull final Project project = addProjectByDb();
        Assertions.assertTrue(projectService.existsByUserId(USER_ID, project.getId()));
    }

    @Test
    public void findByIdByUserId() {
        @NotNull final Project project = addProjectByDb();
        Assertions.assertThrows(IdEmptyException.class, () -> projectService.findById(USER_ID, ""));
        Assertions.assertThrows(UserIdEmptyException.class, () -> projectService.existsByUserId("", project.getId()));
        Assertions.assertNull(projectService.findById(USER_ID, UUID.randomUUID().toString()));
        @Nullable final Project projectFind = projectService.findById(USER_ID, project.getId());
        Assertions.assertNotNull(projectFind);
        Assertions.assertEquals(project.getId(), projectFind.getId());
    }


    @Test
    public void clearByUserId() {
        Assertions.assertThrows(UserIdEmptyException.class, () -> projectService.deleteAllByUserId(""));
        projectService.deleteAllByUserId(USER_ID);
        Assertions.assertEquals(0, projectService.countByUserId(USER_ID));
    }


    @Test
    public void removeByUserId() {
        Assertions.assertThrows(UserIdEmptyException.class, () -> projectService.deleteProject(null, null));
        Assertions.assertThrows(UserIdEmptyException.class, () -> projectService.deleteProject("", null));
        Assertions.assertThrows(IdEmptyException.class, () -> projectService.deleteProject(USER_ID, null));
        Assertions.assertThrows(IdEmptyException.class, () -> projectService.deleteProject(USER_ID, ""));
        @NotNull final Project project = addProjectByDb();
        projectService.deleteProject(USER_ID, project.getId());
        Assertions.assertNull(projectService.findById(USER_ID, project.getId()));
    }


    @Test
    public void countByUserId() {
        Assertions.assertThrows(UserIdEmptyException.class, () -> projectService.countByUserId(""));

        projectRepository.deleteByUserId(USER_ID);
        addProjectByDb();
        addProjectByDb();
        addProjectByDb();
        Assertions.assertEquals(3, projectService.countByUserId(USER_ID));
    }

    @Test
    public void create() {
        @NotNull final String name = "SOME NAME";
        Assertions.assertThrows(UserIdEmptyException.class, () -> projectService.create(null, name));
        Assertions.assertThrows(UserIdEmptyException.class, () -> projectService.create("", name));
        Assertions.assertThrows(NameEmptyException.class, () -> projectService.create(USER_ID, null));
        Assertions.assertThrows(NameEmptyException.class, () -> projectService.create(USER_ID, ""));
        @Nullable final Project project = projectService.create(USER_ID, name);
        Assertions.assertNotNull(project);
        @Nullable final Project findProject = projectService.findById(USER_ID, project.getId());
        Assertions.assertNotNull(findProject);
        Assertions.assertEquals(project.getId(), findProject.getId());
        Assertions.assertEquals(name, findProject.getName());
        Assertions.assertEquals(USER_ID, findProject.getUserId());
    }

    @Test
    public void createWithDescription() {
        @NotNull final String name = "SOME NAME";
        @NotNull final String description = "SOME DESCRIPTION";
        Assertions.assertThrows(UserIdEmptyException.class, () -> projectService.create(null, name, description));
        Assertions.assertThrows(UserIdEmptyException.class, () -> projectService.create("", name, description));
        Assertions.assertThrows(NameEmptyException.class, () -> projectService.create(USER_ID, null, description));
        Assertions.assertThrows(NameEmptyException.class, () -> projectService.create(USER_ID, "", description));
        Assertions.assertThrows(DescriptionEmptyException.class, () -> projectService.create(USER_ID, name, null));
        @NotNull final Project project = projectService.create(USER_ID, name, description);
        Assertions.assertNotNull(project);
        @Nullable final Project findProject = projectService.findById(USER_ID, project.getId());
        Assertions.assertNotNull(findProject);
        Assertions.assertEquals(project.getId(), findProject.getId());
        Assertions.assertEquals(name, project.getName());
        Assertions.assertEquals(description, project.getDescription());
        Assertions.assertEquals(USER_ID, project.getUserId());
    }

    @Test
    public void updateById() {
        @NotNull final Project project = addProjectByDb();
        Assertions.assertThrows(UserIdEmptyException.class, () -> projectService.updateById(null, project.getId(), project.getName(), project.getDescription()));
        Assertions.assertThrows(UserIdEmptyException.class, () -> projectService.updateById("", project.getId(), project.getName(), project.getDescription()));
        Assertions.assertThrows(ProjectIdEmptyException.class, () -> projectService.updateById(USER_ID, null, project.getName(), project.getDescription()));
        Assertions.assertThrows(ProjectIdEmptyException.class, () -> projectService.updateById(USER_ID, "", project.getName(), project.getDescription()));
        Assertions.assertThrows(NameEmptyException.class, () -> projectService.updateById(USER_ID, project.getId(), null, project.getDescription()));
        Assertions.assertThrows(NameEmptyException.class, () -> projectService.updateById(USER_ID, project.getId(), "", project.getDescription()));
        Assertions.assertThrows(DescriptionEmptyException.class, () -> projectService.updateById(USER_ID, project.getId(), project.getName(), null));
        Assertions.assertThrows(ProjectNotFoundException.class, () -> projectService.updateById(USER_ID, UUID.randomUUID().toString(), project.getName(), project.getDescription()));
        @NotNull final String name = project.getName() + UUID.randomUUID().toString();
        @NotNull final String description = project.getDescription() + UUID.randomUUID().toString();
        projectService.updateById(USER_ID, project.getId(), name, description);
        @Nullable final Project projectFind = projectService.findById(USER_ID, project.getId());
        Assertions.assertNotNull(projectFind);
        Assertions.assertEquals(name, projectFind.getName());
        Assertions.assertEquals(description, projectFind.getDescription());
    }

    @Test
    public void changeProjectStatusById() {
        @NotNull final Project project = addProjectByDb();
        @NotNull final Status status = Status.COMPLETED;
        Assertions.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById(null, project.getId(), status));
        Assertions.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById("", project.getId(), status));
        Assertions.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(USER_ID, null, status));
        Assertions.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(USER_ID, "", status));
        Assertions.assertThrows(StatusNotFoundException.class, () -> projectService.changeProjectStatusById(USER_ID, project.getId(), null));
        Assertions.assertThrows(ProjectNotFoundException.class, () -> projectService.changeProjectStatusById(USER_ID, UUID.randomUUID().toString(), status));
        projectService.changeProjectStatusById(USER_ID, project.getId(), status);
        @Nullable final Project projectFind = projectService.findById(USER_ID, project.getId());
        Assertions.assertNotNull(projectFind);
        Assertions.assertEquals(status, projectFind.getStatus());
    }

}
