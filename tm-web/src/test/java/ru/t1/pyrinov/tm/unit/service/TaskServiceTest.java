package ru.t1.pyrinov.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import ru.t1.pyrinov.tm.api.service.ITaskService;
import ru.t1.pyrinov.tm.api.service.IUserService;
import ru.t1.pyrinov.tm.common.AbstractSpringTest;
import ru.t1.pyrinov.tm.enumerated.RoleType;
import ru.t1.pyrinov.tm.enumerated.Status;
import ru.t1.pyrinov.tm.exception.entity.StatusNotFoundException;
import ru.t1.pyrinov.tm.exception.entity.TaskNotFoundException;
import ru.t1.pyrinov.tm.exception.field.*;
import ru.t1.pyrinov.tm.model.Task;
import ru.t1.pyrinov.tm.model.User;
import ru.t1.pyrinov.tm.repository.TaskRepository;
import ru.t1.pyrinov.tm.service.UserDetailsServiceBean;

import java.util.List;
import java.util.UUID;


public class TaskServiceTest extends AbstractSpringTest {

    @Autowired
    private @NotNull ITaskService taskService;

    @Autowired
    private @NotNull TaskRepository taskRepository;

    @Autowired
    private @NotNull IUserService userService;

    @Autowired
    private @NotNull UserDetailsServiceBean userDetailsServiceBean;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    protected static final @NotNull String USERNAME = "test";

    protected static final @NotNull String PASSWORD = "test";

    protected static @Nullable String USER_ID;

    //TEST DATA GENERATION START
    private @NotNull Task addTaskByDb() {
        @Nullable final Task task = taskRepository.save(getStandardTask());
        Assertions.assertNotNull(task);
        Assertions.assertNotNull(USER_ID);
        return task;
    }

    private static int taskNumber = 1;

    private @NotNull Task getStandardTask() {
        @NotNull final Task task = new Task();
        task.setName("TASK #" + taskNumber++);
        task.setDescription("TASK DESCRIPTION");
        task.setUserId(USER_ID);
        return task;
    }
    //TEST DATA GENERATION END

    @BeforeEach
    public void setUp() throws Exception {
        //Run only once
        if (USER_ID == null) {
            @Nullable final User user = userDetailsServiceBean.initUser(USERNAME, PASSWORD, RoleType.USER);
            Assertions.assertNotNull(user);
            USER_ID = user.getId();
            System.out.println("USER ID: " + USER_ID);
        }
    }


    @AfterAll
    public static void tearDown() throws Exception {
    }

    @BeforeEach
    public void before() throws Exception {
    }

    @AfterEach
    public void after() throws Exception {
    }

    @Test
    public void setupOk() {
    }

    @Test
    public void addByUserId() {
        @NotNull final Task task = getStandardTask();
        Assertions.assertThrows(UserIdEmptyException.class, () -> taskService.addByUserId(null, task));
        Assertions.assertNotNull(taskService.addByUserId(USER_ID, task));
        @Nullable final Task taskFind = taskService.findById(USER_ID, task.getId());
        Assertions.assertNotNull(taskFind);
        Assertions.assertEquals(taskFind.getId(), task.getId());
    }


    @Test
    public void findAllByUserId() {
        long count = taskRepository.countByUserId(USER_ID);
        Assertions.assertThrows(UserIdEmptyException.class, () -> taskService.findByUserId(""));
        final List<Task> tasks = taskService.findByUserId(USER_ID);
        Assertions.assertNotNull(tasks);
        Assertions.assertEquals(count, tasks.size());
        tasks.forEach(task -> Assertions.assertEquals(USER_ID, task.getUserId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assertions.assertThrows(UserIdEmptyException.class, () -> taskService.existsByUserId(null, UUID.randomUUID().toString()));
        Assertions.assertThrows(UserIdEmptyException.class, () -> taskService.existsByUserId("", UUID.randomUUID().toString()));
        Assertions.assertThrows(IdEmptyException.class, () -> taskService.existsByUserId(USER_ID, ""));
        Assertions.assertFalse(taskService.existsByUserId(USER_ID, UUID.randomUUID().toString()));
        @NotNull final Task task = addTaskByDb();
        Assertions.assertTrue(taskService.existsByUserId(USER_ID, task.getId()));
    }

    @Test
    public void findByIdByUserId() {
        @NotNull final Task task = addTaskByDb();
        Assertions.assertThrows(IdEmptyException.class, () -> taskService.findById(USER_ID, ""));
        Assertions.assertThrows(UserIdEmptyException.class, () -> taskService.existsByUserId("", task.getId()));
        Assertions.assertNull(taskService.findById(USER_ID, UUID.randomUUID().toString()));
        @Nullable final Task taskFind = taskService.findById(USER_ID, task.getId());
        Assertions.assertNotNull(taskFind);
        Assertions.assertEquals(task.getId(), taskFind.getId());
    }


    @Test
    public void clearByUserId() {
        Assertions.assertThrows(UserIdEmptyException.class, () -> taskService.deleteAllByUserId(""));
        taskService.deleteAllByUserId(USER_ID);
        Assertions.assertEquals(0, taskService.countByUserId(USER_ID));
    }


    @Test
    public void removeByUserId() {
        Assertions.assertThrows(UserIdEmptyException.class, () -> taskService.deleteByUserId(null, null));
        Assertions.assertThrows(UserIdEmptyException.class, () -> taskService.deleteByUserId("", null));
        Assertions.assertThrows(IdEmptyException.class, () -> taskService.deleteByUserId(USER_ID, null));
        Assertions.assertThrows(IdEmptyException.class, () -> taskService.deleteByUserId(USER_ID, ""));
        @NotNull final Task task = addTaskByDb();
        taskService.deleteByUserId(USER_ID, task.getId());
        Assertions.assertNull(taskService.findById(USER_ID, task.getId()));
    }


    @Test
    public void countByUserId() {
        Assertions.assertThrows(UserIdEmptyException.class, () -> taskService.countByUserId(""));

        taskRepository.deleteByUserId(USER_ID);
        addTaskByDb();
        addTaskByDb();
        addTaskByDb();
        Assertions.assertEquals(3, taskService.countByUserId(USER_ID));
    }

    @Test
    public void create() {
        @NotNull final String name = "SOME NAME";
        Assertions.assertThrows(UserIdEmptyException.class, () -> taskService.create(null, name));
        Assertions.assertThrows(UserIdEmptyException.class, () -> taskService.create("", name));
        Assertions.assertThrows(NameEmptyException.class, () -> taskService.create(USER_ID, null));
        Assertions.assertThrows(NameEmptyException.class, () -> taskService.create(USER_ID, ""));
        @Nullable final Task task = taskService.create(USER_ID, name);
        Assertions.assertNotNull(task);
        @Nullable final Task findTask = taskService.findById(USER_ID, task.getId());
        Assertions.assertNotNull(findTask);
        Assertions.assertEquals(task.getId(), findTask.getId());
        Assertions.assertEquals(name, findTask.getName());
        Assertions.assertEquals(USER_ID, findTask.getUserId());
    }

    @Test
    public void createWithDescription() {
        @NotNull final String name = "SOME NAME";
        @NotNull final String description = "SOME DESCRIPTION";
        Assertions.assertThrows(UserIdEmptyException.class, () -> taskService.create(null, name, description));
        Assertions.assertThrows(UserIdEmptyException.class, () -> taskService.create("", name, description));
        Assertions.assertThrows(NameEmptyException.class, () -> taskService.create(USER_ID, null, description));
        Assertions.assertThrows(NameEmptyException.class, () -> taskService.create(USER_ID, "", description));
        Assertions.assertThrows(DescriptionEmptyException.class, () -> taskService.create(USER_ID, name, null));
        @NotNull final Task task = taskService.create(USER_ID, name, description);
        Assertions.assertNotNull(task);
        @Nullable final Task findTask = taskService.findById(USER_ID, task.getId());
        Assertions.assertNotNull(findTask);
        Assertions.assertEquals(task.getId(), findTask.getId());
        Assertions.assertEquals(name, task.getName());
        Assertions.assertEquals(description, task.getDescription());
        Assertions.assertEquals(USER_ID, task.getUserId());
    }

    @Test
    public void updateById() {
        @NotNull final Task task = addTaskByDb();
        Assertions.assertThrows(UserIdEmptyException.class, () -> taskService.updateById(null, task.getId(), task.getName(), task.getDescription()));
        Assertions.assertThrows(UserIdEmptyException.class, () -> taskService.updateById("", task.getId(), task.getName(), task.getDescription()));
        Assertions.assertThrows(TaskIdEmptyException.class, () -> taskService.updateById(USER_ID, null, task.getName(), task.getDescription()));
        Assertions.assertThrows(TaskIdEmptyException.class, () -> taskService.updateById(USER_ID, "", task.getName(), task.getDescription()));
        Assertions.assertThrows(NameEmptyException.class, () -> taskService.updateById(USER_ID, task.getId(), null, task.getDescription()));
        Assertions.assertThrows(NameEmptyException.class, () -> taskService.updateById(USER_ID, task.getId(), "", task.getDescription()));
        Assertions.assertThrows(DescriptionEmptyException.class, () -> taskService.updateById(USER_ID, task.getId(), task.getName(), null));
        Assertions.assertThrows(TaskNotFoundException.class, () -> taskService.updateById(USER_ID, UUID.randomUUID().toString(), task.getName(), task.getDescription()));
        @NotNull final String name = task.getName() + UUID.randomUUID().toString();
        @NotNull final String description = task.getDescription() + UUID.randomUUID().toString();
        taskService.updateById(USER_ID, task.getId(), name, description);
        @Nullable final Task taskFind = taskService.findById(USER_ID, task.getId());
        Assertions.assertNotNull(taskFind);
        Assertions.assertEquals(name, taskFind.getName());
        Assertions.assertEquals(description, taskFind.getDescription());
    }

    @Test
    public void changeTaskStatusById() {
        @NotNull final Task task = addTaskByDb();
        @NotNull final Status status = Status.COMPLETED;
        Assertions.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById(null, task.getId(), status));
        Assertions.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById("", task.getId(), status));
        Assertions.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(USER_ID, null, status));
        Assertions.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(USER_ID, "", status));
        Assertions.assertThrows(StatusNotFoundException.class, () -> taskService.changeTaskStatusById(USER_ID, task.getId(), null));
        Assertions.assertThrows(TaskNotFoundException.class, () -> taskService.changeTaskStatusById(USER_ID, UUID.randomUUID().toString(), status));
        taskService.changeTaskStatusById(USER_ID, task.getId(), status);
        @Nullable final Task taskFind = taskService.findById(USER_ID, task.getId());
        Assertions.assertNotNull(taskFind);
        Assertions.assertEquals(status, taskFind.getStatus());
    }

}
